import React from 'react';
import {StyleSheet} from 'react-native';
import {Router, Scene} from 'react-native-router-flux';
import exploreHome from './app/components/ExploreHome';
import exploreRegion from './app/components/ExploreRegion';

import individualCity from './app/components/IndividualCity';
import moodBroad from './app/components/MoodBroad';
import templates from './app/components/Templates';
import EditHome from './app/components/EditHome';
import Signin from './app/components/Signin';
import EmailSignin from './app/components/EmailSignin';
import ForgotPassword from './app/components/ForgotPassword';
import Home from './app/components/Home';
import Pricing from './app/components/Pricing';
import AddStickers from './app/components/AddStickers';
import FreeItinerary from './app/components/FreeItinerary';
import purchasedItenary from './app/components/PurchasedItinerary';
import AddTripDetails from './app/components/AddTripDetails';
import Profile from './app/components/Profile';
import MoodBroadBlank from './app/components/MoodBroadBlank';
import EnableLocation from './app/components/EnableLocation';
import LoadingPopup from './app/components/LoadingPopup';
import Register from './app/components/Register';
import NearbyLocations from './app/components/NearbyLocations';
import SplashScreen from './app/components/splashScreen';
import CameraPreview from './app/components/cameraPreview';

const Routes = () => (
  <Router navigationBarStyle={styles.navBar} titleStyle={styles.navTitle}>
    <Scene key="root">
      <Scene
        key="SplashScreen"
        component={SplashScreen}
        hideNavBar
        title="SplashScreen"
        initial={true}
      />
      <Scene key="Signin" component={Signin} hideNavBar title="Signin" />
      <Scene
        key="emailsignin"
        component={EmailSignin}
        hideNavBar
        title="emailSignin"
      />
      <Scene
        key="forgotpassword"
        component={ForgotPassword}
        hideNavBar
        title="forgotpassword"
      />
      <Scene key="register" component={Register} hideNavBar title="Register" />
      <Scene
        key="enablelocation"
        component={EnableLocation}
        hideNavBar
        title="EnableLocation"
      />
      <Scene key="home" hideNavBar component={Home} title="Home" />
      <Scene key="EditHome" component={EditHome} hideNavBar title="EditHome" />
      <Scene
        key="templates"
        component={templates}
        hideNavBar
        title="Templates"
      />
      <Scene
        key="moodBroad"
        component={moodBroad}
        hideNavBar
        title="MoodBroad"
      />
      <Scene
        key="MoodBroadBlank"
        component={MoodBroadBlank}
        hideNavBar
        title="MoodBroadBlank"
      />
      <Scene
        key="AddStickers"
        component={AddStickers}
        hideNavBar
        title="AddStickers"
      />
      <Scene
        key="exploreHome"
        hideNavBar
        component={exploreHome}
        title="Login"
      />
      <Scene key="exploreRegion" hideNavBar component={exploreRegion} />
      <Scene
        key="individualCity"
        hideNavBar
        component={individualCity}
        title="Individual City"
      />
      <Scene key="Pricing" component={Pricing} hideNavBar title="Pricing" />
      <Scene
        key="purchasedItenary"
        back={true}
        component={purchasedItenary}
        hideNavBar
        title="Purchased Itenary"
      />
      <Scene
        key="FreeItinerary"
        component={FreeItinerary}
        hideNavBar
        title="FreeItinerary"
      />
      <Scene
        key="AddTripDetails"
        component={AddTripDetails}
        hideNavBar
        title="AddTripDetails"
      />
      <Scene key="Profile" component={Profile} hideNavBar title="Profile" />
      <Scene
        key="LoadingPopup"
        component={LoadingPopup}
        hideNavBar
        title="LoadingPopup"
      />
      <Scene
        key="moodbroadblank"
        component={MoodBroadBlank}
        hideNavBar
        title="moodbroadblank"
      />
      <Scene
        key="NearbyLocations"
        component={NearbyLocations}
        hideNavBar
        title="NearbyLocations"
      />
      <Scene key="cameraPreview" component={CameraPreview} hideNavBar />
    </Scene>
  </Router>
);

const styles = StyleSheet.create({
  navBar: {
    backgroundColor: '#fff',
    elevation: 1,
  },
  navTitle: {
    color: '#111',
    textAlign: 'center',
    flex: 1,
    fontSize: 25,
  },
});
export default Routes;
