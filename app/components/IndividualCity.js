/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {
  Platform,
  Modal,
  AppRegistry,
  StyleSheet,
  Text,
  View,
  TextInput,
  Image,
  Button,
  TouchableHighlight,
  TouchableOpacity,
  CheckBox,
  ScrollView,
  AsyncStorage,
  FlatList,
  Alert,
  ImageBackground,
} from 'react-native';
import {Icon} from 'react-native-elements';
import {Actions} from 'react-native-router-flux';
import {REACT_APP_WEATHER_API_KEY} from 'react-native-dotenv';
import moment from 'moment';
import StarRating from 'react-native-star-rating';
import {Col, Row, Grid} from 'react-native-easy-grid';
import ContentLoader from 'react-native-content-loader';
import {Circle, Rect} from 'react-native-svg';
import axios from 'axios';
import * as geolib from 'geolib';
import MapView, {AnimatedRegion, Animated} from 'react-native-maps';
import {UrlTile} from 'react-native-maps';

export default class individualCity extends React.Component {
  constructor(props) {
    super(props);
    console.log('CHECK PROPS');
    console.log(this.props);
    this.state = {
      lat: undefined,
      lon: undefined,
      errorMessage: undefined,
      temperatureC: undefined,
      temperatureF: undefined,
      city: undefined,
      country: undefined,
      humidity: undefined,
      description: undefined,
      icon: undefined,
      sunrise: undefined,
      sunset: undefined,
      currency: '$',
      api_key: REACT_APP_WEATHER_API_KEY,
      showLocationPopup: false,
      showPhotoitenaryPopup: false,
      user_reviews: [],
      users_ratings: null,
      location_image: '',
      loader: true,
      pricingmodalVisible: false,
      userid: null,
      cityLocations: this.props.cityLocations,
      heartCounter: 0,
      props_cityLocations: this.props.cityLocations,
      modalCityHearted: '',
      modalCityLocationId: '',
      modalCityLat: '',
      modalCityLng: '',
      counter: 0,
      arr: [],
      selectedPhotoLocations: [],
      _showReviews: false,
    };
  }

  async componentDidMount() {
    
    await this._getCurrency();
    await this._getWeather();
    await this._getHeartedPhotoLocations();
    
  }

  /************SELECTED LOCATIONS***************** */

  async _checkLocations(Locations) {
    if (this.state.counter != Locations.length - 1) {
      for (var j = 0; j < Locations.length; j++) {
        if (j != Locations.length - 1) {
          if (Locations[this.state.counter] != Locations[j]) {
            const api_call = await fetch(
              'https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=' +
                Locations[this.state.counter].lat +
                ',' +
                Locations[this.state.counter].long +
                '+&radius=3000&type=point_of_interest&keyword=' +
                Locations[j].address +
                '&key=AIzaSyAUG9ZACOjwqlMYmEqlkXPYMz1wZ14H8zs',
            );
            const locationres = await api_call.json();
            console.log(locationres);
            if (locationres.status == 'OK') {
              const dist = geolib.getDistance(
                {
                  latitude: Locations[this.state.counter].lat,
                  longitude: Locations[this.state.counter].long,
                },
                {latitude: Locations[j].lat, longitude: Locations[j].long},
              );
              Locations[j].distance = dist;
              Locations[j].hide = false;
              this.state.arr.push(Locations[j]);
              console.log(this.state.arr);
            }
          }
        } else if (j == Locations.length - 1) {
          if (Locations[this.state.counter] != Locations[j]) {
            const api_call = await fetch(
              'https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=' +
                Locations[this.state.counter].lat +
                ',' +
                Locations[this.state.counter].long +
                '+&radius=3000&type=point_of_interest&keyword=' +
                Locations[j].address +
                '&key=AIzaSyAUG9ZACOjwqlMYmEqlkXPYMz1wZ14H8zs',
            );
            const locationres = await api_call.json();
            console.log(locationres);
            if (locationres.status == 'OK') {
              const dist = geolib.getDistance(
                {
                  latitude: Locations[this.state.counter].lat,
                  longitude: Locations[this.state.counter].long,
                },
                {latitude: Locations[j].lat, longitude: Locations[j].long},
              );
              Locations[j].distance = dist;
              Locations[j].hide = false;
              this.state.arr.push(Locations[j]);
            } else {
            }
            this.state.counter += 1;
            this._checkLocations(Locations);
            console.log(this.state.arr);
            //
          }
        }
      }
    } else if (this.state.counter == Locations.length - 1) {
      console.log('last');
      // Declare a new array
      let newArray = [];

      // Declare an empty object
      let uniqueObject = {};

      // Loop for the array elements
      console.log('unique object');
      console.log(this.state.arr);
      for (let i in this.state.arr) {
        // Extract the title
        var id = this.state.arr[i]['id'];
        // Use the title as the index
        uniqueObject[id] = this.state.arr[i];
      }

      // Loop to push unique object into array
      for (var i in uniqueObject) {
        newArray.push(uniqueObject[i]);
      }

      // Display the unique objects
      console.log('CHECK ARRAY');
      console.log(newArray);
      console.log('enter');

      this.setState({showPhotoitenaryPopup: false});
      Actions.purchasedItenary({
        cityId: this.props.cityId,
        cityLocations: this.props.cityLocations,
        paymentStatus: this.props.paymentStatus,
        cityIcon: this.state.icon,
        cityName: this.props.cityTitle,
        cityWeather: this.state.icon,
        temperature: this.state.temperatureC,
        citylat: this.props.citylat,
        citylng: this.props.citylng,
        cityNewArr: newArray,
      });
    }
  }

  _checkSelectedLocations() {
    var Locations = this.state.selectedPhotoLocations;
    console.log(Locations);

    if (Locations.length > 0) {
      this._checkLocations(Locations);
    } else if (Locations.length === 0) {
      this.setState({showPhotoitenaryPopup: false});
      Actions.purchasedItenary({
        cityId: this.props.cityId,
        cityLocations: this.props.cityLocations,
        cityIcon: this.state.icon,
        cityName: this.props.cityTitle,
        cityWeather: this.state.icon,
        temperature: this.state.temperatureC,
        citylat: this.props.citylat,
        citylng: this.props.citylng,
        cityNewArr: [],
      });
    }
  }

  /**********************GET CURRENCY************************** */

  _getCurrency(){

    fetch('https://maps.googleapis.com/maps/api/geocode/json?latlng='+this.props.citylat+','+this.props.citylng+'&sensor=true&key=AIzaSyAUG9ZACOjwqlMYmEqlkXPYMz1wZ14H8zs')
      .then(response => response.json())
      .then(json => {
        console.log(json.results[json.results.length-1].formatted_address);

        fetch('https://restcountries.eu/rest/v2/name/'+json.results[json.results.length-1].formatted_address).then(response => response.json()).then(json => {
          console.log(json[0].currencies[0].code);
          console.log(json[0].currencies[0].symbol);
          this.setState({
            currency: json[0].currencies[0].symbol,
          })
        })


      })
    
  }
  /************GET HEARTED PHOTO LOCATIONS*********** */

  async _getHeartedPhotoLocations() {
    await AsyncStorage.getItem('@userLoggedInId').then(value => {
      console.log(value);
      this.setState({
        userid: value,
      });
    });

    await axios
      .post(
        'http://3.135.63.132:3000/api/v1/tplp/cities/getHeartCityByUserId',
        {
          userid: this.state.userid,
          cityid: this.props.cityId,
        },
      )
      .then(responseJson => {
        console.log('hearted');
        console.log(responseJson);
        this.setState({
          loader: false,
        });

        if (responseJson.data.data.length === 0) {
          this.setState({
            heartCounter: 0,
            // cityLocations: this.state.props_cityLocations,
          });
          this.state.cityLocations.forEach(element => {
            if (element.hearted === 'true') {
              element.hearted = '';
            }
          });
          this.setState({
            cityLocations: this.state.cityLocations,
          });
        } else {
          const locationData = responseJson.data.data;
          this.setState({
            heartCounter: responseJson.data.data.length,
          });
          for (var i = 0; i < this.state.cityLocations.length; i++) {
            for (var j = 0; j < locationData.length; j++) {
              if (
                this.state.cityLocations[i].id.toString() ===
                locationData[j].photo_location_id.toString()
              ) {
                this.state.cityLocations[i].hearted = 'true';
                this.setState({
                  cityLocations: this.state.cityLocations,
                });
              }
            }
          }

          var locationsArr = [];
          this.state.cityLocations.filter(city => {
            if (city.hearted === 'true') {
              locationsArr.push(city);
            }
          });

          this.setState({
            selectedPhotoLocations: locationsArr,
          });
          console.log('selected');
          console.log(this.state.selectedPhotoLocations);
        }
      });
  }

  /********CLEAR ASYNCSTORAGE(LOGOUT FN)****** */

  _onSubmit = () => {
    AsyncStorage.clear();
    Actions.Signin();
  };

  /******SHOW MODAL CUSTOMISED PHOTO ITENARY****** */

  _showPhotoItenary() {
    // if (this.props.paymentStatus === 'false') {
    //   this.setState({showPhotoitenaryPopup: true});

    //   setTimeout(() => {
    //     Actions.FreeItinerary({
    //       cityId: this.props.cityId,
    //       cityLocations: this.props.cityLocations,
    //       cityIcon: this.state.icon,
    //       cityName: this.props.cityTitle,
    //       cityWeather: this.state.icon,
    //       temperature: this.state.temperatureC,
    //       citylat: this.props.citylat,
    //       citylng: this.props.citylng,
    //     });
    //     this.setState({showPhotoitenaryPopup: false});
    //   }, 4000);
    // } else {
    this.setState({showPhotoitenaryPopup: true});
    this._checkSelectedLocations();
    
    // }
  }

  /***********HEART PHOTO LOCATIONS ************** */

  async _heartLocations(photoLoc_id, hearted, lat, lng) {
    let heart = 'false';
    console.log(this.state.modalCityHearted);
    console.log(hearted);
    this.setState({
      modalCityHearted: 'false',
    });
    if (
      hearted === '' ||
      hearted === null ||
      hearted === undefined ||
      hearted === 'false'
    ) {
      heart = 'true';
      this.setState({
        modalCityHearted: 'true',
      });
    }
    try {
      await axios
        .post(
          'http://3.135.63.132:3000/api/v1/tplp/cities/HeartCityByUserId',
          {
            userid: this.state.userid,
            cityid: this.props.cityId,
            photolocationid: photoLoc_id.toString(),
            hearted: heart,
            lat: lat,
            lng: lng,
          },
        )
        .then(responseJson => {
          for (var i = 0; i < this.state.cityLocations.length; i++) {
            if (this.state.cityLocations[i].id === photoLoc_id.toString()) {
              this.state.cityLocations[i].hearted = '';
              this.setState({
                cityLocations: this.state.cityLocations,
              });
            }
          }
        });

      await this._getHeartedPhotoLocations();
    } catch {
      Alert.alert('Server Error');
    }
  }

  /***********RENDER LOADER COLS********** */
  _renderCol() {
    return (
      <Col>
        <Row>
          <ContentLoader height={100} duration={1000}>
            <Rect x="0" y="5" rx="0" ry="0" width="100" height="100" />
          </ContentLoader>
        </Row>
        <Row>
          <ContentLoader height={100} duration={1000}>
            <Rect x="0" y="5" rx="0" ry="0" width="100" height="100" />
          </ContentLoader>
        </Row>
        <Row>
          <ContentLoader height={100} duration={1000}>
            <Rect x="0" y="5" rx="0" ry="0" width="100" height="100" />
          </ContentLoader>
        </Row>
        <Row>
          <ContentLoader height={100} duration={1000}>
            <Rect x="0" y="5" rx="0" ry="0" width="100" height="100" />
          </ContentLoader>
        </Row>
      </Col>
    );
  }

  _renderReviews() {
    return this.state.user_reviews.map(comment => {
      return (
        <View style={styles.comntList}>
          <View style={styles.comntCols}>
            <Text style={styles.UserName}>{comment.author_name}</Text>
            <Text style={styles.ComntTxt}>{comment.text}</Text>
          </View>
          <View style={styles.comntCols2}>
            <View style={styles.ratingUser}>
              <StarRating
                disabled={false}
                maxStars={5}
                starSize={16}
                fullStarColor="#cf8cac"
                rating={comment.rating}
              />
            </View>
          </View>
        </View>
      );
    });
  }

  /********GET WEATHER BASED ON CITY LAT LONG********** */

  _getWeather = async () => {
    const lat = this.props.citylat;
    const long = this.props.citylng;
    const api_call = await fetch(
      'https://api.openweathermap.org/data/2.5/weather?lat=' +
        lat +
        '&lon=' +
        long +
        '&appid=' +
        this.state.api_key +
        '&units=metric',
    );
    const data = await api_call.json();
    console.log(data);
    this.setState({
      lat: lat,
      lon: long,
      city: data.name,
      temperatureC: Math.round(data.main.temp),
      temperatureF: Math.round(data.main.temp * 1.8 + 32),
      humidity: data.main.humidity,
      description: data.weather[0].description,
      icon: 'https://openweathermap.org/img/w/' + data.weather[0].icon + '.png',
      // sunrise: this.getTimeFromUnixTimeStamp(data.sys.sunrise),
      sunrise: moment.unix(data.sys.sunrise).format('hh:mm a'),
      sunset: moment.unix(data.sys.sunset).format('hh:mm a'),
      // sunset: this.getTimeFromUnixTimeStamp(data.sys.sunset),
    });
    console.log(this.state);
  };

  /******************RENDER VIEW HERE*************** */

  render() {
    const {cityTitle, cityId, cityLocations, citylat, citylng} = this.props;
    return (
      <View style={styles.container}>
        <View style={styles.header}>
          <Image
            style={styles.hdr_img}
            source={require('../../assets/LOGO_Blk_.png')}
          />
          <View style={styles.logout}>
            <Icon name="ios-heart" size={25} type="ionicon" color="#cf8cac" />
            <Text style={styles.favNoti}>{this.state.heartCounter}</Text>
          </View>
        </View>

        <ScrollView style={{width: '100%', backgroundColor: '#fff'}}>
          <View
            style={{
              justifyContent: 'center',
              position: 'relative',
              flexDirection: 'row',
              backgroundColor: '#cf8cac',
              paddingVertical: 5,
              paddingHorizontal: 20,
              alignItems: 'center',
            }}>
            <Text
              onPress={() => Actions['app1']()}
              style={{
                fontFamily: 'GeorgiaPro-Bold',
                color: '#fff',
                fontSize: 15,
                marginLeft: 80,
              }}>
              {this.props.cityTitle}
            </Text>
            <View
              style={{
                marginLeft: 'auto',
                flexDirection: 'row',
                alignItems: 'center',
              }}>
              <View style={{alignItems: 'center', flexDirection: 'row'}}>
                <Image
                  style={{width: 35, height: 35, resizeMode: 'contain'}}
                  source={{uri: this.state.icon}}
                />
                <Text
                  style={{
                    fontFamily: 'BAHNSCHRIFT',
                    color: '#fff',
                    marginLeft: 5,
                  }}>
                  {this.state.temperatureC}&deg;C
                </Text>
              </View>
              <View
                style={{
                  alignItems: 'center',
                  flexDirection: 'row',
                  marginLeft: 15,
                }}>
                {/* <Image
                  style={{width: 14, resizeMode: 'contain'}}
                  source={require('../../assets/dollar.png')}
                /> */}
                <Text style={{fontFamily: 'BAHNSCHRIFT', color: '#fff'}}>
                  {this.state.currency}50
                </Text>
              </View>
            </View>
          </View>
          <View style={styles.bannerArea}>
            <Image
              style={styles.bannerImg}
              source={require('../../assets/cities.png')}
            />
            <View style={styles.createPic}>
              <TouchableOpacity
                onPress={() => {
                  this._showPhotoItenary();
                }}>
                <Image
                  style={styles.createPicImg}
                  source={require('../../assets/clickcreate.png')}
                />
              </TouchableOpacity>
            </View>
          </View>
          {/* ******************************CITIES******************************  */}
          <View style={styles.contentArea}>
            <View
              style={{
                justifyContent: 'center',
                position: 'relative',
                flexDirection: 'row',
              }}>
              {this.state.loader == false ? (
                this.state.cityLocations.length > 0 ? (
                  <FlatList
                    data={this.state.cityLocations}
                    renderItem={({item, index}) =>
                      this.props.paymentStatus === 'false' ? (
                        index < 3 ? (
                          <View style={styles.flexGrid}>
                            <TouchableOpacity
                              style={styles.touchcss}
                              onPress={() => {
                                this.setState({
                                  modalCityLat: item.lat,
                                  modalCityLng: item.long,
                                  modalCityLocationId: item.id,
                                  modalCityHearted: item.hearted,
                                  showLocationPopup: true,
                                  user_reviews: item.reviews,
                                  users_ratings: Number(item.rating),
                                  location_image:
                                    'http://3.135.63.132:3000/images/' +
                                    item.image,
                                });
                              }}>
                              <Image
                                style={styles.gridImg}
                                source={{
                                  uri:
                                    'http://3.135.63.132:3000/images/' +
                                    item.image,
                                }}
                              />
                              <Text style={styles.gridTitle}>{item.name}</Text>

                              <View style={styles.favorite}>
                                <TouchableOpacity
                                  style={styles.touchcss}
                                  onPress={() => {
                                    this._heartLocations(
                                      item.id,
                                      item.hearted,
                                      item.lat,
                                      item.long,
                                    );
                                  }}>
                                  {item.hearted == 'true' ? (
                                    <Icon
                                      name="heart"
                                      size={20}
                                      type="font-awesome"
                                      color="#ff7dbb"
                                    />
                                  ) : (
                                    <Icon
                                      name="heart-o"
                                      size={20}
                                      type="font-awesome"
                                      color="#ff7dbb"
                                    />
                                  )}
                                </TouchableOpacity>
                              </View>
                            </TouchableOpacity>
                          </View>
                        ) : (
                          <View style={styles.flexGridBlur}>
                            <TouchableOpacity
                              style={styles.touchcss}
                              onPress={() => {
                                this.setState({
                                  pricingmodalVisible: true,
                                });
                              }}>
                              <Image
                                style={styles.gridImg}
                                source={{
                                  uri: '',
                                }}
                              />
                              <Text style={styles.gridTitle}>{item.name}</Text>

                              <View style={styles.favorite}>
                                <TouchableOpacity
                                  style={styles.touchcss}
                                  onPress={() => {
                                    this._heartLocations(
                                      item.id,
                                      item.hearted,
                                      item.lat,
                                      item.long,
                                    );
                                  }}>
                                  {/* {item.hearted == 'true' ? (
                                    <Icon
                                      name="heart"
                                      size={20}
                                      type="font-awesome"
                                      color="#ff7dbb"
                                    />
                                  ) : ( */}
                                  <Icon
                                    name="heart-o"
                                    size={20}
                                    type="font-awesome"
                                    color="#ff7dbb"
                                  />
                                  {/* )} */}
                                </TouchableOpacity>
                              </View>
                            </TouchableOpacity>
                          </View>
                        )
                      ) : (
                        <View style={styles.flexGrid}>
                          <TouchableOpacity
                            style={styles.touchcss}
                            onPress={() => {
                              this.setState({
                                modalCityLat: item.lat,
                                modalCityLng: item.long,
                                modalCityLocationId: item.id,
                                modalCityHearted: item.hearted,
                                showLocationPopup: true,
                                user_reviews: item.reviews,
                                users_ratings: Number(item.rating),
                                location_image:
                                  'http://3.135.63.132:3000/images/' +
                                  item.image,
                              });
                            }}>
                            <Image
                              style={styles.gridImg}
                              source={{
                                uri:
                                  'http://3.135.63.132:3000/images/' +
                                  item.image,
                              }}
                            />
                            <Text style={styles.gridTitle}>{item.name}</Text>

                            <View style={styles.favorite}>
                              <TouchableOpacity
                                style={styles.touchcss}
                                onPress={() => {
                                  this._heartLocations(
                                    item.id,
                                    item.hearted,
                                    item.lat,
                                    item.long,
                                  );
                                }}>
                                {item.hearted == 'true' ? (
                                  <Icon
                                    name="heart"
                                    size={20}
                                    type="font-awesome"
                                    color="#ff7dbb"
                                  />
                                ) : (
                                  <Icon
                                    name="heart-o"
                                    size={20}
                                    type="font-awesome"
                                    color="#ff7dbb"
                                  />
                                )}
                              </TouchableOpacity>
                            </View>
                          </TouchableOpacity>
                        </View>
                      )
                    }
                    //Setting the number of column
                    numColumns={3}
                    keyExtractor={(item, index) => index.toString()}
                  />
                ) : (
                  <View style={styles.fullWidth}>
                    <Text style={styles.notFound}>
                      No City Photo Locations Found !
                    </Text>
                  </View>
                )
              ) : (
                <Grid>
                  {this._renderCol()}
                  {this._renderCol()}
                  {this._renderCol()}
                </Grid>
              )}
            </View>

            {/* Expand Popup 3 */}
            <View style={{alignItems: 'center', justifyContent: 'center'}}>
              {/* Loading Popup */}

              <Modal
                style={{
                  alignItems: 'center',
                  justifyContent: 'center',
                  height: '100%',
                }}
                animationType="fade"
                transparent={true}
                visible={this.state.showPhotoitenaryPopup}
                onRequestClose={() => {
                  Alert.alert('Modal has been closed.');
                }}>
                <ImageBackground
                  source={require('../../assets/overlaybg.png')}
                  style={{width: '100%', height: '100%'}}>
                  <View
                    style={{
                      alignItems: 'center',
                      marginTop: 0,
                      height: 900,
                      paddingTop: 140,
                      width: '100%',
                    }}>
                    <View style={styles.scrollPricing} />
                    <View
                      style={{
                        width: '100%',
                        justifyContent: 'center',
                        alignItems: 'center',
                      }}>
                      <View style={styles.loadingmain}>
                        <Text style={styles.loadingTxt}>
                          Loading your customized photo itinerary...
                        </Text>
                        <Image
                          style={styles.loadingImg}
                          source={require('../../assets/Blooming-Rose-GIF.gif')}
                        />
                      </View>
                    </View>
                  </View>
                </ImageBackground>
              </Modal>

              {/* Payments Popup */}

              <Modal
                style={{
                  alignItems: 'center',
                  justifyContent: 'center',
                  height: '100%',
                }}
                animationType="fade"
                transparent={true}
                visible={this.state.pricingmodalVisible}
                onRequestClose={() => {
                  Alert.alert('Modal has been closed.');
                }}>
                <ImageBackground
                  source={require('../../assets/overlaybg.png')}
                  style={{width: '100%', height: '100%'}}>
                  <View
                    style={{
                      alignItems: 'center',
                      marginTop: 0,
                      height: 900,
                      paddingTop: 180,
                      width: '100%',
                    }}>
                    <TouchableHighlight
                      style={styles.pribtnClose}
                      onPress={() => {
                        this.setState({
                          pricingmodalVisible: false,
                        });
                      }}>
                      <Icon
                        name="ios-close"
                        size={25}
                        type="ionicon"
                        color="#fff"
                      />
                    </TouchableHighlight>

                    <View style={styles.scrollPricing}>
                      <ScrollView horizontal={true}>
                        {/* <View style={styles.priColumn}>
                          <Text style={styles.priceHdng}>Editing</Text>
                          <View
                            style={{position: 'relative', marginBottom: 40}}>
                            <Text style={styles.priceTxt}>$4.99</Text>
                            <Text style={styles.dollarTxt}>per month</Text>
                          </View>
                          <Text style={styles.priFeatures}>
                            Unlimited access to editing features
                          </Text>
                          <Text style={styles.priFeatures}>StoryTemplates</Text>
                          <Text style={styles.priFeatures}>MoodBoards</Text>
                          <Text style={styles.priFeatures}>Stickers</Text>
                          <Image
                            style={styles.priflowerImg}
                            source={require('../../assets/pic2.png')}
                          />
                        </View> */}
                        <View style={styles.priColumn}>
                          <Text style={styles.priceHdng}>Per City</Text>
                          <View
                            style={{position: 'relative', marginBottom: 40}}>
                            <Text style={styles.priceTxt}>$59.99</Text>
                            <Text style={styles.dollarTxt}>pay per city</Text>
                          </View>
                          <Text style={styles.priFeatures}>
                            UNLIMITED ACCESS TO LOCATIONS IN THIS CITY
                          </Text>
                          <Text style={styles.priFeatures}>
                            CUSTOM ITINEARARY CREATION OUTFIT PLANNER ACCESS
                          </Text>
                          <Image
                            style={styles.priflowerImg}
                            source={require('../../assets/pic2.png')}
                          />
                        </View>
                        {/* <View style={styles.priColumn}>
                          <Text style={styles.priceHdngBundle}>Bundle two</Text>
                          <Text style={styles.priceHdng}>
                            ... Or more cities
                          </Text>
                          <View
                            style={{
                              position: 'relative',
                              marginBottom: 40,
                              marginTop: 20,
                            }}>
                            <Text style={styles.priceTxt}>15%</Text>
                            <Text style={styles.dollarTxt}>off</Text>
                          </View>
                          <Text style={styles.priFeatures}>
                            Purchase two or more cities to receive discount
                          </Text>
                          <Image
                            style={styles.priflowerImg}
                            source={require('../../assets/pic2.png')}
                          />
                        </View> */}
                      </ScrollView>
                    </View>
                  </View>
                </ImageBackground>
              </Modal>

              {/* PAYMENTS POPUP */}

              {/* COMMENTS POPUP */}
              <Modal
                style={{
                  alignItems: 'center',
                  justifyContent: 'center',
                  height: '100%',
                }}
                animationType="fade"
                transparent={true}
                visible={this.state.showLocationPopup}
                onRequestClose={() => {}}>
                <ImageBackground style={{width: '100%', height: '100%'}}>
                  <View
                    style={
                      this.state._showReviews === true
                        ? {
                            backgroundColor: '#e6c4d4',
                            height: 600,
                            paddingLeft: 20,
                            paddingVertical: 20,
                            marginTop: 90,
                            alignItems: 'center',
                            justifyContent: 'center',
                            width: '100%',
                          }
                        : {
                            backgroundColor: '#e6c4d4',
                            height: 420,
                            paddingLeft: 20,
                            paddingVertical: 20,
                            marginTop: 90,
                            alignItems: 'center',
                            justifyContent: 'center',
                            width: '100%',
                          }
                    }>
                    <TouchableOpacity
                      style={styles.btnClose}
                      onPress={() => {
                        this.setState({showLocationPopup: false});
                      }}>
                      <View>
                        <Icon
                          name="ios-close"
                          size={30}
                          type="ionicon"
                          color="#fff"
                        />
                      </View>
                    </TouchableOpacity>
                    <ScrollView style={{maxHeight: 680, paddingRight: 20}}>
                      <View
                        style={{
                          borderRadius: 10,
                          marginTop: 25,
                          alignItems: 'center',
                          paddingBottom: 25,
                        }}>
                        <Image
                          style={styles.locationImg}
                          source={{uri: this.state.location_image}}
                        />
                        <View
                          style={{
                            flexDirection: 'row',
                            alignItems: 'center',
                            width: '100%',
                          }}>
                          <View style={styles.ratingMain}>
                            <StarRating
                              disabled={false}
                              maxStars={5}
                              starSize={20}
                              fullStarColor="#cf8cac"
                              rating={this.state.users_ratings}
                            />
                          </View>
                          <View style={{marginLeft: 'auto'}}>
                            {this.state.modalCityHearted === 'true' ? (
                              <Icon
                                name="heart"
                                size={20}
                                type="font-awesome"
                                color="#ff7dbb"
                                onPress={() =>
                                  this._heartLocations(
                                    this.state.modalCityLocationId,
                                    this.state.modalCityHearted,
                                    this.state.modalCityLat,
                                    this.state.modalCityLng,
                                  )
                                }
                              />
                            ) : (
                              <Icon
                                name="heart-o"
                                size={20}
                                type="font-awesome"
                                color="#ff7dbb"
                                onPress={() =>
                                  this._heartLocations(
                                    this.state.modalCityLocationId,
                                    this.state.modalCityHearted,
                                    this.state.modalCityLat,
                                    this.state.modalCityLng,
                                  )
                                }
                              />
                            )}
                          </View>
                        </View>
                        <View style={styles.btnsecret}>
                          <Text
                            style={{
                              fontSize: 15,
                              color: '#fff',
                              fontFamily: 'BAHNSCHRIFT',
                            }}>
                            ‘HEART’ TO ADD TO ITINERARY
                          </Text>
                          <Image
                            style={styles.flowerImgpop}
                            source={require('../../assets/pic3.png')}
                          />
                        </View>
                      </View>

                      <View
                        style={{
                          padding: 15,
                          backgroundColor: '#f0e0e2',
                          borderWidth: 1,
                          borderColor: '#e7c7d6',
                          borderRadius: 10,
                          flex: 1,
                          justifyContent: 'flex-end',
                          alignItems: 'flex-end',
                        }}>
                        <TouchableOpacity
                          style={styles.touchcss}
                          onPress={() => {
                            this.setState({_showReviews: true});
                          }}>
                          <View
                            style={{
                              width: '100%',
                              marginBottom: 10,
                              flexDirection: 'row',
                              alignItems: 'center',
                            }}>
                            <View>
                              <Text
                                style={{
                                  fontSize: 10,
                                  color: '#cf8cac',
                                  fontFamily: 'GeorgiaPro-Bold',
                                  textTransform: 'uppercase',
                                }}>
                                Comments
                              </Text>
                            </View>
                            <View style={{marginLeft: 'auto'}}>
                              <Icon
                                name="angle-down"
                                size={25}
                                type="font-awesome"
                                color="#cf8cac"
                              />
                            </View>
                          </View>

                          {this.state._showReviews == true
                            ? this._renderReviews()
                            : null}
                        </TouchableOpacity>
                      </View>
                    </ScrollView>
                  </View>
                </ImageBackground>
              </Modal>
            </View>
          </View>
        </ScrollView>

        <View style={styles.footMain}>
          <View style={styles.footFlex}>
            <TouchableOpacity onPress={() => Actions.exploreRegion()}>
              <Icon name="ios-planet" size={35} type="ionicon" color="#fff" />
            </TouchableOpacity>
          </View>

          <View style={styles.footFlex}>
            <TouchableOpacity onPress={() => Actions.Profile()}>
              <Icon size={30} name="ios-person" type="ionicon" color="#fff" />
            </TouchableOpacity>
          </View>

          <View style={styles.footFlex}>
            <TouchableOpacity
              style={styles.centerMenu}
              onPress={this._onSubmit.bind(this)}>
              <Image
                style={{
                  marginTop: 20,
                  width: 40,
                  height: 40,
                  resizeMode: 'contain',
                }}
                source={require('../../assets/heartlock.png')}
              />
            </TouchableOpacity>
          </View>

          <View style={styles.footFlex}>
            <TouchableOpacity onPress={() => Actions.home()}>
              <Icon size={27} name="ios-home" type="ionicon" color="#fff" />
            </TouchableOpacity>
          </View>

          <View style={styles.footFlex}>
            <TouchableOpacity onPress={() => Actions.Profile()}>
              <Icon size={27} name="ios-search" type="ionicon" color="#fff" />
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  favNoti: {
    position: 'absolute',
    top: -5,
    right: -10,
    fontFamily: 'BAHNSCHRIFT',
    width: 15,
    fontSize: 11,
    height: 15,
    borderRadius: 50,
    backgroundColor: '#cf8cac',
    color: '#fff',
    textAlign: 'center',
    alignItems: 'center',
    justifyContent: 'center',
  },
  createPic: {
    position: 'absolute',
    top: -48,
    left: 0,
    zIndex: 99,
    backgroundColor: '#e7c7d6',
    alignItems: 'center',
    justifyContent: 'center',
    borderTopRightRadius: 100,
    borderBottomLeftRadius: 100,
    borderBottomRightRadius: 100,
    width: 90,
    height: 90,
    borderWidth: 1,
    borderColor: '#fff',
  },
  createPicImg: {
    width: 80,
    height: 80,
    resizeMode: 'contain',
    marginRight: -10,
    marginBottom: -6,
  },
  logout: {
    position: 'absolute',
    right: 20,
    top: 15,
  },
  favorite: {
    position: 'absolute',
    right: 5,
    top: 5,
  },
  letsEdit: {
    position: 'absolute',
    right: 10,
    bottom: 0,
    top: 0,
    zIndex: 0,
    opacity: 0.5,
    width: 140,
    resizeMode: 'contain',
  },
  gridTitle: {
    position: 'absolute',
    left: 0,
    right: 0,
    margin: 'auto',
    fontSize: 12,
    textAlign: 'center',
    color: '#cf8cac',
    paddingVertical: 5,
    backgroundColor: 'rgba(240,224,226, .8)',
    elevation: 1,
    marginVertical: 10,
    fontFamily: 'GeorgiaPro-Bold',
  },
  gridImg: {
    width: '100%',
    height: 120,
    resizeMode: 'cover',
    opacity: 0.5,
    borderRadius: 5,
    borderWidth: 1,
    borderColor: '#e9f0f6',
  },
  flexGrid: {
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 0,
    width: '33.3%',
    borderRadius: 5,
    backgroundColor: '#fff',
  },
  flexGridBlur: {
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 0,
    width: '33.3%',
    borderRadius: 5,
    backgroundColor: '#4472c4',
  },
  weKnow: {
    color: '#cf8cac',
    fontSize: 16,
    textAlign: 'center',
    lineHeight: 22,
    marginTop: 20,
    marginBottom: 20,
    paddingVertical: 10,
    paddingHorizontal: 40,
    fontWeight: 'bold',
    backgroundColor: '#f0e0e2',
    borderRadius: 100,
  },
  absoluTxt: {
    position: 'absolute',
    bottom: 20,
    left: 20,
    top: 20,
    backgroundColor: 'rgba(255,255,255, .7)',
    right: 20,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    paddingBottom: 10,
    borderRadius: 5,
    elevation: 1,
  },
  bannerArea: {
    alignItems: 'center',
    justifyContent: 'center',
    position: 'relative',
    backgroundColor: '#fff',
    marginBottom: 10,
  },
  bannerImg: {
    height: 400,
    width: '100%',
    resizeMode: 'cover',
  },
  contentArea: {
    paddingHorizontal: 20,
    paddingBottom: 20,
  },
  footMain: {
    backgroundColor: '#cf8cac',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    paddingTop: 10,
    paddingBottom: 10,
    maxHeight: 55,
  },
  centerMenu: {
    backgroundColor: '#cf8cac',
    position: 'relative',
    top: -15,
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    borderTopLeftRadius: 50,
    borderTopRightRadius: 50,
  },
  footFlex: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-start',
    height: '100%',
    paddingTop: 30,
  },
  header: {
    position: 'relative',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    elevation: 1,
    // backgroundColor: '#f0e0e2',
    width: '100%',
    paddingTop: 6,
    paddingBottom: 6,
  },
  hdr_img: {
    width: 200,
    height: 50,
    marginBottom: 5,
    resizeMode: 'stretch',
  },
  body: {
    flex: 1,
    height: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  body_img: {
    alignItems: 'center',
    width: 220,
    height: 186,
  },
  ftr_hdngs: {
    textAlign: 'center',
    color: '#fff',
    fontSize: 25,
  },
  instagram: {
    backgroundColor: '#cf8cac',
    alignItems: 'stretch',
    paddingVertical: 13,
    textAlign: 'center',
    lineHeight: 20,
    borderRadius: 30,
    paddingBottom: 15,
    fontSize: 16,
    marginBottom: 15,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    flex: 1,
    marginRight: 5,
  },
  btnEmail: {
    paddingVertical: 5,
    backgroundColor: '#cf8cac',
    textAlign: 'center',
    borderRadius: 30,
    fontSize: 14,
    marginBottom: 15,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    flex: 1,
    marginLeft: 5,
  },
  touchcss: {
    position: 'relative',
    width: '100%',
    justifyContent: 'center',
  },

  /*************MODAL********* */
  btnClose: {
    borderRadius: 30,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    position: 'absolute',
    top: 0,
    right: 20,
    zIndex: 99,
    width: 30,
    height: 30,
    paddingBottom: 0,
    backgroundColor: '#cf8cac',
  },
  locationImg: {
    width: '100%',
    height: 250,
    resizeMode: 'cover',
    borderRadius: 10,
  },
  ratingMain: {
    flexDirection: 'row',
  },
  btnsecret: {
    paddingVertical: 15,
    backgroundColor: '#cf8cac',
    borderRadius: 30,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    flex: 1,
    maxHeight: 50,
    borderWidth: 1,
    borderColor: '#fff',
    minHeight: 40,
    paddingHorizontal: 20,
    position: 'absolute',
    top: -25,
    zIndex: 11,
  },
  flowerImgpop: {
    width: 40,
    zIndex: 11,
    height: 40,
    position: 'absolute',
    resizeMode: 'contain',
    right: -15,
    bottom: -5,
  },
  comntList: {
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: 'rgba(255,255,255, 1)',
    paddingHorizontal: 15,
    paddingVertical: 10,
    borderRadius: 5,
    marginBottom: 5,
  },
  UserName: {
    fontFamily: 'GeorgiaPro-Bold',
    fontSize: 13,
    marginBottom: 3,
  },
  ComntTxt: {
    fontFamily: 'BAHNSCHRIFT',
    fontSize: 12,
    lineHeight: 15,
  },
  comntCols: {
    flex: 1,
  },
  comntCols2: {
    flex: 1,
    marginRight: 'auto',
    alignItems: 'flex-end',
    maxWidth: 80,
  },
  ratingUser: {
    flexDirection: 'row',
  },
  loadingImg: {
    width: 250,
    height: 250,
    resizeMode: 'contain',
  },
  loadingTxt: {
    fontFamily: 'BAHNSCHRIFT',
    textTransform: 'uppercase',
    fontSize: 20,
    textAlign: 'center',
    color: '#cf8cac',
  },
  loadingmain: {
    backgroundColor: '#fff',
    justifyContent: 'center',
    alignItems: 'center',
    padding: 20,
    maxWidth: '90%',
    borderRadius: 10,
    top: 70,
    elevation: 1,
  },
  notFound: {
    fontSize: 16,
    textAlign: 'center',
    alignItems: 'center',
    backgroundColor: '#fcfcfc',
    paddingHorizontal: 60,
    paddingVertical: 10,
    marginTop: 15,
    borderColor: '#ddd',
    borderWidth: 1,
    borderRadius: 5,
    width: '100%',
  },
  fullWidth: {
    width: '100%',
    display: 'flex',
    flexDirection: 'column',
  },

  /************PRICING MODAL********** */

  priceHdngBundle: {
    color: '#fff',
    fontSize: 25,
    textAlign: 'center',
    fontFamily: 'GeorgiaPro-Bold',
    textTransform: 'uppercase',
  },
  priceHdng: {
    color: '#fff',
    fontSize: 18,
    textAlign: 'center',
    marginBottom: 10,
    fontFamily: 'GeorgiaPro-Bold',
    textTransform: 'uppercase',
  },
  priceTxt: {
    color: '#fff',
    fontSize: 50,
    textAlign: 'center',
    fontFamily: 'GeorgiaPro-Bold',
  },
  dollarTxt: {
    fontSize: 40,
    fontFamily: 'Southampton',
    color: '#fff',
    position: 'absolute',
    bottom: -30,
    right: 10,
  },
  scrollPricing: {
    width: '100%',
    flexDirection: 'row',
    paddingLeft: 20,
  },
  priColumn: {
    backgroundColor: '#cf8cac',
    paddingHorizontal: 25,
    paddingVertical: 30,
    width: 320,
    marginBottom: 10,
    marginRight: 10,
    borderRadius: 15,
  },
  priFeatures: {
    color: '#fff',
    fontSize: 16,
    textAlign: 'center',
    marginBottom: 20,
    textTransform: 'uppercase',
    fontFamily: 'BAHNSCHRIFT',
  },
  pribtnClose: {
    borderRadius: 30,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    position: 'absolute',
    top: 30,
    right: 20,
    zIndex: 99,
    width: 30,
    height: 30,
    backgroundColor: '#cf8cac',
  },
  priflowerImg: {
    width: 60,
    height: 60,
    resizeMode: 'contain',
    position: 'absolute',
    bottom: -10,
    transform: [{rotate: '50deg'}],
    left: '46%',
  },
});
