import React, {Component, useState} from 'react';
import {
  ImageBackground,
  Modal,
  Alert,
  Platform,
  AppRegistry,
  StyleSheet,
  Text,
  View,
  TextInput,
  Image,
  Button,
  TouchableHighlight,
  TouchableOpacity,
  CheckBox,
  ScrollView,
  FlatList,
  AsyncStorage,
  Video,
} from 'react-native';
import {Icon} from 'react-native-elements';
import {Actions} from 'react-native-router-flux';
import {Col, Row, Grid} from 'react-native-easy-grid';
import ContentLoader from 'react-native-content-loader';
import {Circle, Rect} from 'react-native-svg';

export default class moodBroad extends React.Component {
  constructor() {
    super();
    this.state = {
      dataSource: [],
      modalVisible: false,
      loader: true,
    };
    this._checkForFirstTimeModal();
  }

  async _checkForFirstTimeModal() {
    await AsyncStorage.getItem('@moodBoardModal').then(value => {
      if (value == null || value === '' || value === undefined) {
        this.setState({
          modalVisible: true,
        });
        AsyncStorage.setItem('@moodBoardModal', 'true');
      } else {
        
      }
    });
  }

  _onSubmit = () => {
    AsyncStorage.clear();
    Actions.Signin();
  };

  /***********RENDER LOADER COLS********** */
  _renderCol() {
    return (
      <Col>
        <Row>
          <ContentLoader height={200} duration={1000}>
            <Rect x="0" y="5" rx="0" ry="0" width="160" height="200" />
          </ContentLoader>
        </Row>
        <Row>
          <ContentLoader height={200} duration={1000}>
            <Rect x="0" y="5" rx="0" ry="0" width="160" height="200" />
          </ContentLoader>
        </Row>
        <Row>
          <ContentLoader height={200} duration={1000}>
            <Rect x="0" y="5" rx="0" ry="0" width="160" height="200" />
          </ContentLoader>
        </Row>
        <Row>
          <ContentLoader height={200} duration={1000}>
            <Rect x="0" y="5" rx="0" ry="0" width="160" height="200" />
          </ContentLoader>
        </Row>
      </Col>
    );
  }

  componentDidMount() {
    this._getMoodBoards();
  }

  setModalVisible() {
    this.setState({modalVisible: false});
  }

  _getMoodBoards() {
    fetch('http://3.135.63.132:3000/api/v1/tplp/moodbroads/getMoodBroadsList')
      .then(response => response.json())
      .then(json => {
        setTimeout(() => {
          this.setState({
            loader: false,
          });
        }, 2000);

        if (json.list.length > 0) {
          for (var i = 0; i < json.list.length; i++) {
            json.list[i].image =
              'http://3.135.63.132:3000/images/' + json.list[i].image;
          }
          this.setState({
            dataSource: json.list,
          });
          console.log(this.state.dataSource);
        } else {
        }
      });
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.header}>
          <Image
            style={styles.hdr_img}
            source={require('../../assets/LOGO_Blk_.png')}
          />
        </View>
        <ScrollView style={{width: '100%', backgroundColor: '#fff'}}>
          <View style={styles.contentArea}>
            {this.state.loader == false ? (
              this.state.dataSource.length > 0 ? (
                <FlatList
                  data={this.state.dataSource}
                  renderItem={({item}) => (
                    <View style={styles.flexGrid}>
                      <TouchableOpacity
                        style={styles.touchcss}
                        onPress={() =>
                          Actions.moodbroadblank({
                            moodboardtitle: item.title,
                            moodboardid: item._id,
                          })
                        }>
                        <Image
                          style={styles.gridImg}
                          source={{uri: item.image}}
                        />
                      </TouchableOpacity>
                      <Text style={styles.gridTitle}>{item.title}</Text>
                    </View>
                  )}
                  //Setting the number of column
                  numColumns={2}
                  keyExtractor={(item, index) => index.toString()}
                />
              ) : (
                <View style={styles.fullWidth}>
                  <Text style={styles.notFound}>
                    No City Photo Locations Found !
                  </Text>
                </View>
              )
            ) : (
              <Grid>
                {this._renderCol()}
                {this._renderCol()}
              </Grid>
            )}
          </View>

          <View style={{alignItems: 'center', justifyContent: 'center'}}>
            <Modal
              style={{
                alignItems: 'center',
                justifyContent: 'center',
                height: '100%',
              }}
              animationType="fade"
              transparent={true}
              visible={this.state.modalVisible}
              onRequestClose={() => {
                // Alert.alert('Modal has been closed.');
              }}>
              <ImageBackground
                source={require('../../assets/overlaybg.png')}
                style={{width: '100%', height: '100%'}}>
                <TouchableOpacity
                  style={styles.btnClose}
                  onPress={() => {
                    this.setModalVisible();
                  }}>
                  <View style={{alignItems: 'center', flexDirection: 'row'}}>
                    <Icon
                      name="ios-close"
                      size={30}
                      type="ionicon"
                      color="#fff"
                    />
                  </View>
                </TouchableOpacity>
                <View
                  style={{
                    alignItems: 'center',
                    marginTop: 0,
                    height: 900,
                    paddingHorizontal: 20,
                    paddingTop: 180,
                  }}>
                  <View
                    style={{
                      backgroundColor: '#cf8cac',
                      evelation: 1,
                      borderRadius: 10,
                      marginTop: 0,
                      paddingHorizontal: 20,
                      paddingTop: 40,
                      paddingBottom: 40,
                    }}>
                    <Image
                      style={styles.flowerImg}
                      source={require('../../assets/pic3.png')}
                    />
                    <Image
                      style={styles.flowerImg2}
                      source={require('../../assets/pic2.png')}
                    />
                    <View style={styles.fullWidth}>
                      <Text
                        style={{
                          fontFamily: 'GeorgiaPro-Bold',
                          color: '#fff',
                          fontSize: 25,
                          textAlign: 'center',
                          marginBottom: 10,
                        }}>
                        MOOD BOARDS
                      </Text>
                      <Text
                        style={{
                          fontFamily: 'BAHNSCHRIFT',
                          color: '#fff',
                          fontSize: 14,
                          textAlign: 'center',
                          lineHeight: 17,
                        }}>
                        Great shots don’t just happen…prep for your trip with
                        custom TPLP Mood Boards. Collect outfits, poses, and
                        angle inspo to get the most out of your custom
                        itinerary.
                      </Text>
                    </View>
                  </View>
                </View>
              </ImageBackground>
            </Modal>
          </View>
        </ScrollView>

        <View style={styles.footMain}>
          <View style={styles.footFlex}>
            <TouchableOpacity onPress={() => Actions.exploreRegion()}>
              <Icon name="ios-planet" size={35} type="ionicon" color="#fff" />
            </TouchableOpacity>
          </View>

          <View style={styles.footFlex}>
            <TouchableOpacity onPress={() => Actions.Profile()}>
              <Icon size={30} name="ios-person" type="ionicon" color="#fff" />
            </TouchableOpacity>
          </View>

          <View style={styles.footFlex}>
            <TouchableOpacity
              style={styles.centerMenu}
              onPress={this._onSubmit.bind(this)}>
              <Image
                style={{
                  marginTop: 20,
                  width: 40,
                  height: 40,
                  resizeMode: 'contain',
                }}
                source={require('../../assets/heartlock.png')}
              />
            </TouchableOpacity>
          </View>

          <View style={styles.footFlex}>
            <TouchableOpacity onPress={() => Actions.home()}>
              <Icon size={27} name="ios-home" type="ionicon" color="#fff" />
            </TouchableOpacity>
          </View>

          <View style={styles.footFlex}>
            <TouchableOpacity onPress={() => Actions.Profile()}>
              <Icon size={27} name="ios-search" type="ionicon" color="#fff" />
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    justifyContent: 'flex-start',
    flex: 1,
    paddingTop: 0,
    height: '100%',
    width: '100%',
  },
  header: {
    position: 'relative',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    elevation: 1,
    backgroundColor: '#fff',
    width: '100%',
    paddingTop: 6,
    paddingBottom: 6,
  },
  hdr_img: {
    width: 200,
    height: 50,
    resizeMode: 'stretch',
  },
  imageThumbnail: {
    justifyContent: 'center',
    alignItems: 'center',
    height: 100,
  },
  gridImg: {
    width: '100%',
    height: 220,
    resizeMode: 'stretch',
    borderRadius: 5,
    resizeMode: 'cover',
    opacity: 0.5,
  },
  gridTitle: {
    position: 'absolute',
    left: 0,
    right: 0,
    margin: 'auto',
    fontSize: 14,
    textAlign: 'center',
    color: '#fff',
    paddingVertical: 5,
    backgroundColor: 'rgba(207,140,172, .9)',
    fontFamily: 'GeorgiaPro-Bold',
  },
  flexGrid: {
    // alignItems: 'center',
    justifyContent: 'center',
    marginTop: 10,
    width: '48.5%',
    // marginRight: 10,
    borderRadius: 5,
    flex: 1,
    flexDirection: 'column',
    marginRight: 10,
    marginLeft: 10,
  },
  contentArea: {
    // paddingHorizontal: 20,
    // paddingBottom: 20,
    // paddingTop: 10,
  },
  touchcss: {
    position: 'relative',
    width: '100%',
    justifyContent: 'center',
  },
  footMain: {
    backgroundColor: '#cf8cac',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    paddingTop: 10,
    paddingBottom: 10,
    maxHeight: 55,
  },
  footFlex: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  fullWidth: {
    width: '100%',
    position: 'relative',
  },
  flowerImg: {
    width: 60,
    height: 60,
    position: 'absolute',
    resizeMode: 'contain',
    right: -15,
    bottom: -15,
  },
  flowerImg2: {
    width: 60,
    height: 60,
    position: 'absolute',
    resizeMode: 'contain',
    left: -15,
    top: -15,
  },
  btnClose: {
    borderRadius: 30,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    position: 'absolute',
    top: 30,
    right: 20,
    zIndex: 99,
    width: 30,
    height: 30,
    paddingBottom: 0,
    backgroundColor: '#cf8cac',
  },
  notFound: {
    fontSize: 16,
    textAlign: 'center',
    alignItems: 'center',
    backgroundColor: '#fcfcfc',
    paddingHorizontal: 60,
    paddingVertical: 10,
    marginTop: 15,
    borderColor: '#ddd',
    borderWidth: 1,
    borderRadius: 5,
    width: '100%',
  },
  fullWidth: {
    width: '100%',
    display: 'flex',
    flexDirection: 'column',
  },
});
