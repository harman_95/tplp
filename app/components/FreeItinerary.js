import React from 'react';
import {
  Platform,
  AppRegistry,
  StyleSheet,
  Text,
  View,
  TextInput,
  Image,
  Button,
  TouchableHighlight,
  TouchableOpacity,
  CheckBox,
  ScrollView,
  AsyncStorage,
} from 'react-native';
import {Icon} from 'react-native-elements';
import {Actions} from 'react-native-router-flux';
import * as geolib from 'geolib';
import MapView, {AnimatedRegion, Animated} from 'react-native-maps';
import {UrlTile} from 'react-native-maps';
import {Marker} from 'react-native-maps';
import Geolocation from '@react-native-community/geolocation';

export default class individualCity extends React.Component {
  constructor(props) {
    super(props);
    console.log('CHECK PROPS');
    console.log(this.props);
    var newarr = [];
    for (var i = 0; i < 3; i++) {
      newarr.push(this.props.cityLocations[i]);
      console.log(newarr);
    }
    this.state = {
      freeLocations: newarr,
    };
  }

  _showFreePhotoLocations() {
    console.log(this.props.citylat + ',,' + this.props.citylng);
    var minX = this.props.citylat;
    var maxX = this.props.citylat;
    var minY = this.props.citylng;
    var maxY = this.props.citylng;

    minX = Math.min(minX, this.props.citylat);
    maxX = Math.max(maxX, this.props.citylat);
    minY = Math.min(minY, this.props.citylng);
    maxY = Math.max(maxY, this.props.citylng);

    const midX = (minX + maxX) / 2;
    const midY = (minY + maxY) / 2;
    const deltaX = maxX - minX;
    const deltaY = maxY - minY;

    return (
      <Animated
        style={styles.bannerImg}
        initialRegion={{
          latitude: midX,
          longitude: midY,
          latitudeDelta: 0.4,
          longitudeDelta: 0.4,
        }}>
        {this.state.freeLocations.map(marker => (
          <MapView.Marker.Animated
            key={marker.name}
            coordinate={{
              latitude: marker.lat,
              longitude: marker.long,
            }}
            title={marker.name}>
            <View style={styles.radius}>
              <View style={styles.markerStyle} />
            </View>
            <Image
              source={require('../../assets/pic1.png')}
              style={{height: 48, width: 48}}
            />
          </MapView.Marker.Animated>
        ))}
      </Animated>
    );
  }

  _onSubmit = () => {
    AsyncStorage.clear();
    Actions.Signin();
  };

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.header}>
          <Image
            style={styles.hdr_img}
            source={require('../../assets/LOGO_Blk_.png')}
          />
        </View>

        <ScrollView style={{width: '100%', backgroundColor: '#fff'}}>
          <View
            style={{
              justifyContent: 'center',
              position: 'relative',
              flexDirection: 'row',
              backgroundColor: '#cf8cac',
              paddingVertical: 5,
              paddingHorizontal: 20,
              alignItems: 'center',
            }}>
            <Text
              onPress={() => Actions['app1']()}
              style={{
                fontFamily: 'GeorgiaPro-Bold',
                color: '#fff',
                fontSize: 15,
                marginLeft: 80,
              }}>
              {this.props.cityName}
            </Text>
            <View
              style={{
                marginLeft: 'auto',
                flexDirection: 'row',
                alignItems: 'center',
              }}>
              <View style={{alignItems: 'center', flexDirection: 'row'}}>
                <Image
                  style={{width: 35, height: 35, resizeMode: 'contain'}}
                  source={{uri: this.props.cityIcon}}
                />
                <Text
                  style={{
                    fontFamily: 'BAHNSCHRIFT',
                    color: '#fff',
                    marginLeft: 5,
                  }}>
                  {this.props.temperature}&deg;C
                </Text>
              </View>
              <View
                style={{
                  alignItems: 'center',
                  flexDirection: 'row',
                  marginLeft: 15,
                }}>
                <Image
                  style={{width: 14, resizeMode: 'contain'}}
                  source={require('../../assets/dollar.png')}
                />
                <Text style={{fontFamily: 'BAHNSCHRIFT', color: '#fff'}}>
                  50
                </Text>
              </View>
            </View>
          </View>
          <View style={styles.bannerArea}>
            {/* <Image
              style={styles.bannerImg}
              source={require('../../assets/map.jpg')}
            /> */}

            {this._showFreePhotoLocations()}
            <View style={styles.createPic}>
              <Image
                style={styles.createPicImg}
                source={require('../../assets/profile3.jpg')}
              />
              <Image
                style={styles.flowerImg}
                source={require('../../assets/pic3.png')}
              />
            </View>
          </View>

          {/* <View style={styles.contentArea}>
            <View
              style={{
                justifyContent: 'center',
                position: 'relative',
                flexDirection: 'row',
              }}>
              <View style={styles.flexGrid}>
                <Image
                  style={styles.gridImg}
                  source={require('../../assets/Picture5.jpg')}
                />
                <Text style={styles.gridTitle}>BALI</Text>
                <View style={styles.favorite}>
                  <Image
                    style={styles.eyeImg}
                    source={require('../../assets/openeye.png')}
                  />
                </View>
              </View>
              <View style={styles.flexGrid}>
                <Image
                  style={styles.gridImg}
                  source={require('../../assets/Picture4.jpg')}
                />
                <Text style={styles.gridTitle}>BARCELONA</Text>
                <View style={styles.favorite}>
                  <Image
                    style={styles.eyeImg}
                    source={require('../../assets/openeye.png')}
                  />
                </View>
              </View>
              <View style={styles.flexGrid}>
                <Image
                  style={styles.gridImg}
                  source={require('../../assets/Picture4.jpg')}
                />
                <Text style={styles.gridTitle}>CHARLESTON</Text>
                <View style={styles.favorite}>
                  <Image
                    style={styles.eyeImg}
                    source={require('../../assets/openeye.png')}
                  />
                </View>
              </View>
            </View>
            <View
              style={{
                justifyContent: 'center',
                position: 'relative',
                flexDirection: 'row',
              }}>
              <View style={styles.flexGrid}>
                <Image
                  style={styles.gridImg}
                  source={require('../../assets/Picture6.jpg')}
                />
                <Text style={styles.gridTitle}>FLORENCE</Text>
                <View style={styles.favorite}>
                  <Image
                    style={styles.eyeImg}
                    source={require('../../assets/openeye.png')}
                  />
                </View>
              </View>
              <View style={styles.flexGrid}>
                <Image
                  style={styles.gridImg}
                  source={require('../../assets/Picture4.jpg')}
                />
                <Text style={styles.gridTitle}>L.A</Text>
                <View style={styles.favorite}>
                  <Image
                    style={styles.eyeImg}
                    source={require('../../assets/closeeye.png')}
                  />
                </View>
              </View>
              <View style={styles.flexGrid}>
                <Image
                  style={styles.gridImg}
                  source={require('../../assets/Picture5.jpg')}
                />
                <Text style={styles.gridTitle}>L.A. WALLS</Text>
                <View style={styles.favorite}>
                  <Image
                    style={styles.eyeImg}
                    source={require('../../assets/openeye.png')}
                  />
                </View>
              </View>
            </View>
            <View
              style={{
                justifyContent: 'center',
                position: 'relative',
                flexDirection: 'row',
              }}>
              <View style={styles.flexGrid}>
                <Image
                  style={styles.gridImg}
                  source={require('../../assets/Picture6.jpg')}
                />
                <Text style={styles.gridTitle}>LA TRIPPIN</Text>
                <View style={styles.favorite}>
                  <Image
                    style={styles.eyeImg}
                    source={require('../../assets/openeye.png')}
                  />
                </View>
              </View>
              <View style={styles.flexGrid}>
                <Image
                  style={styles.gridImg}
                  source={require('../../assets/Picture5.jpg')}
                />
                <Text style={styles.gridTitle}>LONDON</Text>
                <View style={styles.favorite}>
                  <Image
                    style={styles.eyeImg}
                    source={require('../../assets/openeye.png')}
                  />
                </View>
              </View>
              <View style={styles.flexGrid}>
                <Image
                  style={styles.gridImg}
                  source={require('../../assets/Picture6.jpg')}
                />
                <Text style={styles.gridTitle}>MOROCCO</Text>
                <View style={styles.favorite}>
                  <Image
                    style={styles.eyeImg}
                    source={require('../../assets/openeye.png')}
                  />
                </View>
              </View>
            </View>
            <View
              style={{
                justifyContent: 'center',
                position: 'relative',
                flexDirection: 'row',
              }}>
              <View style={styles.flexGrid}>
                <Image
                  style={styles.gridImg}
                  source={require('../../assets/Picture4.jpg')}
                />
                <Text style={styles.gridTitle}>MUNICH</Text>
                <View style={styles.favorite}>
                  <Image
                    style={styles.eyeImg}
                    source={require('../../assets/closeeye.png')}
                  />
                </View>
              </View>
              <View style={styles.flexGrid}>
                <Image
                  style={styles.gridImg}
                  source={require('../../assets/Picture2.jpg')}
                />
                <Text style={styles.gridTitle}>NYC</Text>
                <View style={styles.favorite}>
                  <Image
                    style={styles.eyeImg}
                    source={require('../../assets/closeeye.png')}
                  />
                </View>
              </View>
              <View style={styles.flexGrid}>
                <Image
                  style={styles.gridImg}
                  source={require('../../assets/Picture1.jpg')}
                />
                <Text style={styles.gridTitle}>PALM SPRINGS</Text>
                <View style={styles.favorite}>
                  <Image
                    style={styles.eyeImg}
                    source={require('../../assets/openeye.png')}
                  />
                </View>
              </View>
            </View>
            <View
              style={{
                justifyContent: 'center',
                position: 'relative',
                flexDirection: 'row',
              }}>
              <View style={styles.flexGrid}>
                <Image
                  style={styles.gridImg}
                  source={require('../../assets/Picture5.jpg')}
                />
                <Text style={styles.gridTitle}>PARIS</Text>
                <View style={styles.favorite}>
                  <Image
                    style={styles.eyeImg}
                    source={require('../../assets/closeeye.png')}
                  />
                </View>
              </View>
              <View style={styles.flexGrid}>
                <Image
                  style={styles.gridImg}
                  source={require('../../assets/Picture6.jpg')}
                />
                <Text style={styles.gridTitle}>ROME</Text>
                <View style={styles.favorite}>
                  <Image
                    style={styles.eyeImg}
                    source={require('../../assets/openeye.png')}
                  />
                </View>
              </View>
              <View style={styles.flexGrid}>
                <Image
                  style={styles.gridImg}
                  source={require('../../assets/Picture4.jpg')}
                />
                <Text style={styles.gridTitle}>SANTORINI</Text>
                <View style={styles.favorite}>
                  <Image
                    style={styles.eyeImg}
                    source={require('../../assets/closeeye.png')}
                  />
                </View>
              </View>
            </View>
            <View
              style={{
                justifyContent: 'center',
                position: 'relative',
                flexDirection: 'row',
              }}>
              <View style={styles.flexGrid}>
                <Image
                  style={styles.gridImg}
                  source={require('../../assets/Picture3.jpg')}
                />
                <Text style={styles.gridTitle}>SOUTH OF FRANCE</Text>
                <View style={styles.favorite}>
                  <Image
                    style={styles.eyeImg}
                    source={require('../../assets/openeye.png')}
                  />
                </View>
              </View>
              <View style={styles.flexGrid}>
                <Image
                  style={styles.gridImg}
                  source={require('../../assets/Picture2.jpg')}
                />
                <Text style={styles.gridTitle}>TURKEY</Text>
                <View style={styles.favorite}>
                  <Image
                    style={styles.eyeImg}
                    source={require('../../assets/openeye.png')}
                  />
                </View>
              </View>
              <View style={styles.flexGrid}>
                <Image
                  style={styles.gridImg}
                  source={require('../../assets/Picture1.jpg')}
                />
                <Text style={styles.gridTitle}>FIND ME</Text>
                <View style={styles.favorite}>
                  <Image
                    style={styles.eyeImg}
                    source={require('../../assets/closeeye.png')}
                  />
                </View>
              </View>
            </View>
          </View> */}
        </ScrollView>

        <View style={styles.footMain}>
          <View style={styles.footFlex}>
            <TouchableOpacity onPress={() => Actions.exploreRegion()}>
              <Icon name="ios-planet" size={35} type="ionicon" color="#fff" />
            </TouchableOpacity>
          </View>

          <View style={styles.footFlex}>
            <TouchableOpacity onPress={() => Actions.Profile()}>
              <Icon size={30} name="ios-person" type="ionicon" color="#fff" />
            </TouchableOpacity>
          </View>

          <View style={styles.footFlex}>
            <TouchableOpacity
              style={styles.centerMenu}
              onPress={this._onSubmit.bind(this)}>
              <Image
                style={{
                  marginTop: 20,
                  width: 40,
                  height: 40,
                  resizeMode: 'contain',
                }}
                source={require('../../assets/heartlock.png')}
              />
            </TouchableOpacity>
          </View>

          <View style={styles.footFlex}>
            <TouchableOpacity onPress={() => Actions.home()}>
              <Icon size={27} name="ios-home" type="ionicon" color="#fff" />
            </TouchableOpacity>
          </View>

          <View style={styles.footFlex}>
            <TouchableOpacity onPress={() => Actions.Profile()}>
              <Icon size={27} name="ios-search" type="ionicon" color="#fff" />
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  createPic: {
    position: 'absolute',
    top: -48,
    left: 0,
    zIndex: 99,
    backgroundColor: '#e7c7d6',
    alignItems: 'center',
    justifyContent: 'center',
    borderTopRightRadius: 100,
    borderBottomLeftRadius: 100,
    borderBottomRightRadius: 100,
    width: 90,
    height: 90,
    borderWidth: 1,
    borderColor: '#fff',
  },
  eyeImg: {
    width: 20,
    resizeMode: 'contain',
  },
  flowerImg: {
    width: 60,
    height: 60,
    position: 'absolute',
    resizeMode: 'contain',
    right: -15,
    bottom: -15,
  },
  createPicImg: {
    width: 80,
    height: 80,
    resizeMode: 'cover',
    borderTopRightRadius: 100,
    borderBottomRightRadius: 100,
    borderBottomLeftRadius: 100,
  },
  logout: {
    position: 'absolute',
    right: 20,
    top: 15,
  },
  favorite: {
    position: 'absolute',
    right: 5,
    bottom: 5,
  },
  letsEdit: {
    position: 'absolute',
    right: 10,
    bottom: 0,
    top: 0,
    zIndex: 0,
    opacity: 0.5,
    width: 140,
    resizeMode: 'contain',
  },
  gridTitle: {
    position: 'absolute',
    left: 0,
    right: 0,
    margin: 'auto',
    fontSize: 12,
    textAlign: 'center',
    color: '#cf8cac',
    paddingVertical: 5,
    backgroundColor: 'rgba(240,224,226, .8)',
    elevation: 1,
    marginVertical: 10,
    fontFamily: 'GeorgiaPro-Bold',
  },
  gridImg: {
    width: '100%',
    height: 120,
    resizeMode: 'cover',
    opacity: 0.5,
    borderRadius: 5,
    borderWidth: 1,
    borderColor: '#e9f0f6',
  },
  flexGrid: {
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 0,
    width: '33.3%',
    borderRadius: 5,
    backgroundColor: '#fff',
  },
  weKnow: {
    color: '#cf8cac',
    fontSize: 16,
    textAlign: 'center',
    lineHeight: 22,
    marginTop: 20,
    marginBottom: 20,
    paddingVertical: 10,
    paddingHorizontal: 40,
    fontWeight: 'bold',
    backgroundColor: '#f0e0e2',
    borderRadius: 100,
  },
  absoluTxt: {
    position: 'absolute',
    bottom: 20,
    left: 20,
    top: 20,
    backgroundColor: 'rgba(255,255,255, .7)',
    right: 20,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    paddingBottom: 10,
    borderRadius: 5,
    elevation: 1,
  },
  bannerArea: {
    alignItems: 'center',
    justifyContent: 'center',
    position: 'relative',
    backgroundColor: '#fff',
    marginBottom: 25,
    borderWidth: 1,
    borderColor: '#ddd',
  },
  bannerImg: {
    height: 600,
    width: '100%',
    resizeMode: 'cover',
  },
  contentArea: {
    paddingHorizontal: 20,
    paddingBottom: 20,
  },
  footMain: {
    backgroundColor: '#cf8cac',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    paddingTop: 10,
    paddingBottom: 10,
    maxHeight: 55,
  },
  centerMenu: {
    backgroundColor: '#cf8cac',
    position: 'relative',
    top: -15,
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    borderTopLeftRadius: 50,
    borderTopRightRadius: 50,
  },
  footFlex: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-start',
    height: '100%',
    paddingTop: 30,
  },
  header: {
    position: 'relative',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    elevation: 1,
    backgroundColor: '#ffffff',
    width: '100%',
    paddingTop: 6,
    paddingBottom: 6,
  },
  hdr_img: {
    width: 200,
    height: 50,
    marginBottom: 5,
    resizeMode: 'stretch',
  },
  body: {
    flex: 1,
    height: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  body_img: {
    alignItems: 'center',
    width: 220,
    height: 186,
  },
  ftr_hdngs: {
    textAlign: 'center',
    color: '#fff',
    fontSize: 25,
  },
  instagram: {
    backgroundColor: '#cf8cac',
    alignItems: 'stretch',
    paddingVertical: 13,
    textAlign: 'center',
    lineHeight: 20,
    borderRadius: 30,
    paddingBottom: 15,
    fontSize: 16,
    marginBottom: 15,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    flex: 1,
    marginRight: 5,
  },
  btnEmail: {
    paddingVertical: 5,
    backgroundColor: '#cf8cac',
    textAlign: 'center',
    borderRadius: 30,
    fontSize: 14,
    marginBottom: 15,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    flex: 1,
    marginLeft: 5,
  },
});
