import React, {Component} from 'react';
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  Image,
  TouchableOpacity,
  TouchableHighlight,
  ScrollView,
  KeyboardAvoidingView,
} from 'react-native';
import {Icon} from 'react-native-elements';
import {Snackbar} from 'react-native-paper';
import Spinner from 'react-native-loading-spinner-overlay';
import {Actions} from 'react-native-router-flux';
import axios from 'axios';

class Register extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      uname: '',
      email: '',
      password: '',
      cpassword: '',
      error: '',
      spinner: false,
      loggedInUser: '',
      accessToken: null,
      visible: false,
      validation: '',
      color: 'black',
    };
  }

  /*************************REGISTER FORM SUBMISSION*********** */
  async _onSubmit() {
    var reg = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    if (
      (this.state.email == '' ||
        this.state.email == null ||
        this.state.email == undefined) &&
      (this.state.password == '' ||
        this.state.password == null ||
        this.state.password == undefined) &&
      (this.state.cpassword == '' ||
        this.state.cpassword == null ||
        this.state.cpassword == undefined)
    ) {
      this.setState({visible: true, validation: '*All Fields Are Required!'});
    } else if (
      this.state.email == '' ||
      this.state.email == null ||
      this.state.email == undefined
    ) {
      this.setState({visible: true, validation: '*Email Is Required!'});
    } else if (reg.test(this.state.email) == false) {
      this.setState({visible: true, validation: '*Email is not valid!'});
    } else if (
      this.state.password == '' ||
      this.state.password == null ||
      this.state.password == undefined
    ) {
      this.setState({visible: true, validation: '*Password Is Required!'});
    } else if (
      this.state.cpassword == '' ||
      this.state.cpassword == null ||
      this.state.cpassword == undefined
    ) {
      this.setState({
        visible: true,
        validation: '*Confirm Password Is Required!',
      });
    } else if (this.state.password != this.state.cpassword) {
      this.setState({
        visible: true,
        validation: '*Password & Confirm Password Do Not Match!',
      });
    } else if (this.state.password.length <= 5) {
      this.setState({
        visible: true,
        validation: '*Password Must be 6-12 Characters Long!',
      });
    } else {
      this.setState({spinner: true});
      const stremail =
        (await this.state.email.charAt(0).toLowerCase()) +
        this.state.email.slice(1);

      await this.setState({
        email: stremail,
      });

      await axios
        .post('http://3.135.63.132:3000/api/v1/tplp/add_user', {
          username: this.state.uname,
          email: this.state.email,
          password: this.state.password,
        })
        .then(responseJson => {
          console.log('check response');
          console.log(JSON.stringify(responseJson));

          if (JSON.stringify(responseJson.data.status) == 0) {
            this.setState({
              visible: true,
              validation: 'Registered Successfully!',
              color: '#228B22',
            });
            setTimeout(() => {
              this.setState({spinner: false});
              Actions.emailsignin();
            }, 2000);
          } else if (JSON.stringify(responseJson.data.status) == 2) {
            this.setState({
              spinner: false,
              visible: true,
              validation:
                'User Already Registered with this email. Try Different Email Address!',
              color: 'red',
            });
          } else {
            this.setState({
              spinner: false,
              visible: true,
              validation: 'Try Again!',
              color: 'red',
            });
          }
        })
        .catch(error => {
          this.setState({spinner: false});
        });
    }
  }

  /************LOADER********* */

  renderLoader() {
    if (this.state.spinner) {
      return (
        <Spinner
          visible={this.state.spinner}
          size={'large'}
          textContent={'Loading...'}
          animation={'slide'}
          textStyle={styles.spinnerTextStyle}
        />
      );
    }
  }

  /*****************RENDER VIEW***************** */
  render() {
    return (
      <KeyboardAvoidingView enabled keyboardVerticalOffset={100}>
        <ScrollView contentContainerStyle={styles.contentContainer}>
          {/*
                      <Toast
                      ref="toast"
                      style={{backgroundColor:'gray'}}
                      position='top'
                      positionValue={200}
                      fadeInDuration={750}
                      fadeOutDuration={1000}
                      opacity={0.8}
                      textStyle={{color:'white'}}
                      /> */}

          <View style={styles.container}>
            <Snackbar
              style={{backgroundColor: this.state.color}}
              visible={this.state.visible}
              onDismiss={() => this.setState({visible: false})}
              action={{
                // label: 'Undo',
                onPress: () => {
                  // Do something
                },
              }}>
              {this.state.validation}
            </Snackbar>


            <View style={styles.header}>
              <Image
                style={styles.hdr_img}
                source={require('../../assets/LOGO_Blk_.png')}
              />
            </View>
            <Text style={{marginTop: 30}} />
            {/* <View style={styles.SectionStyle}>
              <Icon color="#fff" name="person" />
              <TextInput
                ref="fname"
                style={styles.input}
                onChangeText={uname => this.setState({uname})}
                value={this.state.uname}
                placeholder="Enter Username"
                underlineColorAndroid="transparent"
                placeholderTextColor="#fff"
              />
            </View> */}

            <View style={styles.SectionStyle}>
              <Icon color="#fff" name="email" />
              <TextInput
                ref="email"
                style={styles.input}
                onChangeText={email => this.setState({email})}
                value={this.state.email}
                placeholder="Enter Email"
                underlineColorAndroid="transparent"
                placeholderTextColor="#fff"
              />
            </View>
            <View style={styles.SectionStyle}>
              <Icon color="#fff" name="lock" />
              <TextInput
                secureTextEntry={true}
                ref="password"
                style={styles.input}
                onChangeText={password => this.setState({password})}
                value={this.state.password}
                placeholder="Enter Password"
                underlineColorAndroid="transparent"
                placeholderTextColor="#fff"
              />
            </View>
            <View style={styles.SectionStyle}>
              <Icon color="#fff" name="lock" />
              <TextInput
                secureTextEntry={true}
                ref="cpassword"
                style={styles.input}
                onChangeText={cpassword => this.setState({cpassword})}
                value={this.state.cpassword}
                placeholder="Enter Confirm Password"
                underlineColorAndroid="transparent"
                placeholderTextColor="#fff"
              />
            </View>

            <View style={styles.Flexbox}>
              <TouchableOpacity onPress={this._onSubmit.bind(this)}>
                <Text style={styles.button}>START HERE</Text>
              </TouchableOpacity>

              {this.renderLoader()}

              {/* <TouchableHighlight> */}
              {/* <TouchableOpacity onPress={()=>this.props.navigation.push('ForgotPassword')}>
            <Text style={{
              color:'#fff',
              textDecorationLine: 'underline',
              textAlign: 'right',
              fontSize: 15,
              marginTop: 5,
            }}>Forgot password?</Text>
            </TouchableOpacity> */}
              {/* </TouchableHighlight> */}
            </View>

            <View style={styles.boldLine} />

            <View>
              <TouchableHighlight>
                <TouchableOpacity onPress={() => Actions.emailsignin()}>
                  <Text
                    style={{
                      color: '#fff',
                      textDecorationLine: 'underline',
                      textAlign: 'right',
                      fontSize: 15,
                      marginTop: 5,
                    }}>
                    Already Have An Account? Login Now
                  </Text>
                </TouchableOpacity>
              </TouchableHighlight>
            </View>
          </View>
        </ScrollView>
      </KeyboardAvoidingView>
    );
  }
}

const styles = StyleSheet.create({
  contentContainer: {
    paddingBottom: 20,
    width: '100%',
    backgroundColor: '#cf8cac',
    height: '100%',
  },
  container: {
    backgroundColor: '#cf8cac',
    display: 'flex',
    height: '100%',
    width: '100%',
    paddingLeft: 20,
    paddingRight: 20,
    paddingTop: 110,
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  SectionStyle: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 1,
    borderColor: '#d3d3d3',
    height: 50,
    marginTop: 10,
    width: '100%',
    borderRadius: 50,
    paddingLeft: 25,
  },
  Flexbox: {
    marginTop: 30,
    width: '100%',
  },
  button: {
    backgroundColor: '#fff',
    height: 50,
    flexDirection: 'row',
    fontSize: 16,
    color: '#cf8cac',
    textAlign: 'center',
    textTransform: 'uppercase',
    width: '100%',
    borderRadius: 50,
    paddingTop: 14,
  },
  input: {
    color: '#d3d3d3',
    width: '100%',
    paddingLeft: 5,
    fontSize: 14,
  },
  CheckBoxRow: {
    flexDirection: 'row',
    width: '100%',
    height: 30,
    marginTop: 10,
  },
  boldLine: {
    flexDirection: 'row',
    height: 6,
    width: 150,
    backgroundColor: '#d3d3d3',
    marginTop: 40,
    borderRadius: 5,
  },
  header: {
    position: 'relative',
    display: 'flex',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
  hdr_img: {
    width: 320,
    height: 91,
    marginBottom: 20,
    resizeMode: 'contain',
  },
});
export default Register;
