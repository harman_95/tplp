import React from 'react';
import {StyleSheet, View, Text, Image, TouchableOpacity} from 'react-native';
import Hand from '../../assets/hand.svg';
import {RNCamera} from 'react-native-camera';
import {
  responsiveHeight,
  responsiveWidth,
  responsiveFontSize,
} from 'react-native-responsive-dimensions';
import {Actions} from 'react-native-router-flux';

const PendingView = () => (
  <View
    style={{
      flex: 1,
      backgroundColor: 'lightgreen',
      justifyContent: 'center',
      alignItems: 'center',
    }}>
    <Text>Waiting</Text>
  </View>
);
export default class CameraPreview extends React.Component {
  constructor(props) {
    super(props);
  }

  async takePicture() {
    console.log('Take picture called');
    const options = {
      quality: 0.5,
    };
    const uri = await this.camera.takePictureAsync(options);
    console.log(uri);
    Actions.templates({
      uri: uri,
    });
  }

  render() {
    return (
      <View style={styles.container}>
        {/* <RNCamera
          ref={ref => {
            this.camera = ref;
          }}
          style={styles.camera}
          androidCameraPermissionOptions={{
            title: 'Permission to use camera',
            message: 'We need your permission to use your camera',
            buttonPositive: 'Ok',
            buttonNegative: 'Cancel',
          }}
        /> */}
        <RNCamera
          ref={ref => {
            this.camera = ref;
          }}
          style={styles.camera}
          type={RNCamera.Constants.Type.back}
          androidCameraPermissionOptions={{
            title: 'Permission to use camera',
            message: 'We need your permission to use your camera',
            buttonPositive: 'Ok',
            buttonNegative: 'Cancel',
          }}
          androidRecordAudioPermissionOptions={{
            title: 'Permission to use audio recording',
            message: 'We need your permission to use your audio',
            buttonPositive: 'Ok',
            buttonNegative: 'Cancel',
          }}>
          {({camera, status, recordAudioPermissionStatus}) => {
            if (status !== 'READY') return <PendingView />;
            return (
              <View
                style={{
                  flex: 0,
                  flexDirection: 'row',
                  justifyContent: 'center',
                }}>
                <TouchableOpacity
                  onPress={this.takePicture.bind(this)}
                  style={styles.capture}>
                  <View style={styles.btn} />
                </TouchableOpacity>
              </View>
            );
          }}
        </RNCamera>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    justifyContent: 'flex-start',
    height: '100%',
    width: '100%',
  },
  btn: {
    borderRadius: 40,
    width: 80,
    height: 80,
    borderWidth: 7,
    borderColor: '#53BEFE',
    marginBottom: '10%',
    backgroundColor: 'white',
  },
  camera: {
    flex: 1,
    justifyContent: 'space-between',
  },
  ic_hand: {
    position: 'absolute',
    left: '10%',
    top: 0,
    width: '80%',
    height: '100%',
  },
  bottom_container: {
    position: 'absolute',
    left: 0,
    right: 0,
    bottom: 0,
    // height: responsiveHeight(10),
    justifyContent: 'center',
    backgroundColor: 'transparent',
  },
  overlay: {
    position: 'absolute',
    left: 0,
    top: 0,
    right: 0,
  },
});
