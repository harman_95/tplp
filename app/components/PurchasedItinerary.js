/* eslint-disable no-unused-vars */
/* eslint-disable react-native/no-inline-styles */
import React, {Component, useState} from 'react';
import {
  ImageBackground,
  Modal,
  Alert,
  Platform,
  AppRegistry,
  StyleSheet,
  Text,
  View,
  TextInput,
  Image,
  Button,
  AsyncStorage,
  TouchableHighlight,
  TouchableOpacity,
  FlatList,
  ScrollView,
} from 'react-native';
import {Icon, CheckBox} from 'react-native-elements';
import {Actions} from 'react-native-router-flux';
import axios from 'axios';
import * as geolib from 'geolib';
import MapView, {AnimatedRegion, Animated} from 'react-native-maps';
import {UrlTile} from 'react-native-maps';
import {Marker} from 'react-native-maps';
import Geolocation from '@react-native-community/geolocation';

export default class purchasedItenary extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      modalVisible: false,
      modalVisible2: false,
      modalVisible3: false,
      tempCounter: 0,
      _selectedLocations: '',
      _citiesWithinMiles: [],
      num: 0,
      userid: '',
      hotels: [],
      startTravelDate: '',
      endTravelDate: '',
      nodatamodal: false,
      itenaryArray: [],
      openCloseArray: [],
    };
  }

  /********ON VIEW LOAD****** */

  async componentDidMount() {
    await AsyncStorage.getItem('@userLoggedInId').then(value => {
      this.setState({
        userid: value,
      });
    });
    await this._checkNullOrNot();
    await this._getHotel();
    await this._saveForProfile();
    if(this.props.cityNewArr.length > 0){
        await this._getHeartedPhotoLocations();
    }
  }

  /**********SHOW HIDE AREAS BASED ON AREA CLICKED********** */
  _openCloseAreas(index, hide) {
    if (hide === 'true') {
      var arr = [];
      this.setState({
        openCloseArray: arr,
      });
      arr = this.state.itenaryArray[index].arr;
      this.setState({openCloseArray: arr}, () => {
        for (var i = 0; i < this.props.cityNewArr.length; i++) {
          for (var j = 0; j < this.state.openCloseArray.length; j++) {
            if (
              this.props.cityNewArr[i].id === this.state.openCloseArray[j].id
            ) {
              this.props.cityNewArr[i].hide = true;
              this.state.itenaryArray[index].open = false;
              this.setState({
                itenaryArray: this.state.itenaryArray,
              });
              this._checkCityLatLong();
            }
          }
        }
      });
    } else if (hide === 'false') {
      var arr = [];
      this.setState({
        openCloseArray: arr,
      });
      arr = this.state.itenaryArray[index].arr;
      this.setState({openCloseArray: arr}, () => {
        for (var i = 0; i < this.props.cityNewArr.length; i++) {
          for (var j = 0; j < this.state.openCloseArray.length; j++) {
            if (
              this.props.cityNewArr[i].id === this.state.openCloseArray[j].id
            ) {
              this.props.cityNewArr[i].hide = false;
              this.state.itenaryArray[index].open = true;
              this.setState({
                itenaryArray: this.state.itenaryArray,
              });
              this._checkCityLatLong();
            }
          }
        }
      });
    }
  }

  /************GROUPING OF LOCATIONS BY AREAS******* */
  _checkNullOrNot() {
    if (this.props.cityNewArr.length === 0) {
      this.setState({
        nodatamodal: true,
      });
    } else {
      if (this.props.cityNewArr.length <= 5) {
        var arr = [];
        arr = this.props.cityNewArr;
        var itenaryArr = [];
        itenaryArr.push({
          areaName: 'Area',
          arr: arr,
          open: true,
        });
        this.setState({
          itenaryArray: itenaryArr,
        });
      } else if (this.props.cityNewArr.length > 5) {
        var arr = [];
        var itenaryArr = [];
        for (var i = 0; i < this.props.cityNewArr.length; i++) {
          if (arr.length === 5) {
            itenaryArr.push({
              areaName: 'Area',
              arr: arr,
              open: true,
            });
            this.setState({
              itenaryArray: itenaryArr,
            });
            arr = [];
            arr.push(this.props.cityNewArr[i]);
            if (i === this.props.cityNewArr.length - 1) {
              itenaryArr.push({
                areaName: 'Area',
                arr: arr,
                open: true,
              });
              this.setState({
                itenaryArray: itenaryArr,
              });
              arr = [];
            }
          } else {
            arr.push(this.props.cityNewArr[i]);
            if (i === this.props.cityNewArr.length - 1) {
              itenaryArr.push({
                areaName: 'Area',
                arr: arr,
                open: true,
              });
              this.setState({
                itenaryArray: itenaryArr,
              });
              arr = [];
            }
          }
        }
      }
    }
  }

  /**********RENDER AREAS BY LOCATIONS********** */
  _renderAreas(arr) {
    return arr.map((item, index) => {
      return (
        <View style={styles.checkboxRow}>
          <Text style={styles.checkboxTxt}>{item.name}</Text>
          <View style={styles.checkBox}>
            <CheckBox />
          </View>
        </View>
      );
    });
  }
  /**********RENDER ITINARIES BY LOCATIONS********** */
  _renderItinariesAreas() {

    if(this.state.itenaryArray.length == 0){

      return(
                <View style={styles.fullWidthItenary2}>
                  <Text style={styles.notFound}>NO ITINARIES FOUND !</Text>
                </View>
      )

    }else if(this.state.itenaryArray.length > 0){
          return this.state.itenaryArray.map((item, index) => {
            return (
              <View>
                <Text
                  style={{
                    fontFamily: 'metropolis-bold',
                    color: '#fff',
                    fontSize: 16,
                    borderBottomWidth: 1,
                    borderColor: 'rgba(255,255,255, .2)',
                    marginRight: 20,
                  }}>
                  {item.areaName} {index + 1}
                </Text>
                {this._renderAreas(item.arr)}
                <View style={styles.alignDots}>
                  <Text style={styles.dottedstyle}>. . . . . . . . . .</Text>
                </View>
              </View>
            );
          });
    }

  }

  /**********SAVE FOR PROFILE********* */

  _saveForProfile() {
    axios
      .post('http://3.135.63.132:3000/api/v1/tplp/saveCities', {
        userid: this.state.userid,
        cityid: this.props.cityId,
      })
      .then(responseJson => {});
  }

  /*******GET HOTEL*********** */

  async _getHotel() {
    try {
      await AsyncStorage.getItem('@userLoggedInId').then(value => {
        this.setState({
          userid: value,
        });
      });

      await axios
        .post('http://3.135.63.132:3000/api/v1/tplp/hotels/get_hotel', {
          userid: this.state.userid,
          cityid: this.props.cityId,
        })
        .then(responseJson => {
          console.log(responseJson);

          if(responseJson.data.data == null){

               AsyncStorage.getItem('@remindMe').then(value => {
                  if (value == null || value === '' || value === undefined) {
                  } else {
                                setTimeout(() => {
                                  this.setState({
                                    modalVisible2: true
                                  })
                                }, 2000);
                  }
              });
            
          }else if(responseJson.data.data.hotels.length > 1){

              this.setState({
                hotels: responseJson.data.data.hotels,
              });
              this.setState({
                startTravelDate:
                  responseJson.data.data.hotels[0].checkIn.split('-')[0] +
                  '-' +
                  responseJson.data.data.hotels[0].checkIn.split('-')[1],
                endTravelDate:
                  responseJson.data.data.hotels[responseJson.data.data.hotels.length - 1].checkOut.split('-')[0] +
                  '-' +
                  responseJson.data.data.hotels[responseJson.data.data.hotels.length - 1].checkOut.split('-')[1],
              });

          }else if(responseJson.data.data.hotels.length == 0){

              this.setState({
                hotels: responseJson.data.data.hotels,
              });
              this.setState({
                startTravelDate:
                  responseJson.data.data.hotels[0].checkIn.split('-')[0] +
                  '-' +
                  responseJson.data.data.hotels[0].checkIn.split('-')[1],
                endTravelDate:
                  responseJson.data.data.hotels[0].checkOut.split('-')[0] +
                  '-' +
                  responseJson.data.data.hotels[0].checkOut.split('-')[1],
              });

          }

        });
      // await this._checkSelectedLocations(this.state._selectedLocations);
    } catch (e) {
      Alert.alert('Server Error');
    }
  }

  /***********SHOW LOCATIONS WITH CUSTOM MARKERS ON MAP************ */

  _checkCityLatLong() {
    var minX = this.props.citylat;
    var maxX = this.props.citylat;
    var minY = this.props.citylng;
    var maxY = this.props.citylng;

    minX = Math.min(minX, this.props.citylat);
    maxX = Math.max(maxX, this.props.citylat);
    minY = Math.min(minY, this.props.citylng);
    maxY = Math.max(maxY, this.props.citylng);

    const midX = (minX + maxX) / 2;
    const midY = (minY + maxY) / 2;

    return (
      <Animated
        style={styles.bannerImg}
        initialRegion={{
          latitude: midX,
          longitude: midY,
          latitudeDelta: 0.00922 * 2.5,
          longitudeDelta: 0.00922 * 2.5,
        }}>
        {this.props.cityNewArr.map(marker =>
          marker.hide === false || undefined || null ? (
            <MapView.Marker.Animated
              key={marker.name}
              coordinate={{
                latitude: Number(marker.lat),
                longitude: Number(marker.long),
              }}
              title={marker.name}>
              <View style={styles.radius}>
                <View style={styles.markerStyle} />
              </View>
              <Image
                source={require('../../assets/pic1.png')}
                style={{height: 48, width: 48}}
              />
            </MapView.Marker.Animated>
          ) : null,
        )}

        {this.state.hotels.map(marker => (
          <MapView.Marker.Animated
            key={marker.hotelName}
            coordinate={{
              latitude: Number(marker.lat),
              longitude: Number(marker.lng),
            }}
            title={marker.hotelName}>
            <View style={styles.radius}>
              <View style={styles.markerStyle} />
            </View>
            <Image
              source={require('../../assets/hotel-icon.jpg')}
              style={{height: 48, width: 48}}
            />
          </MapView.Marker.Animated>
        ))}
      </Animated>
    );
  }

  /*********LOGOUT*********** */

  _onSubmit = () => {
    AsyncStorage.clear();
    Actions.Signin();
  };

  /*********REDIRECT TO TRIP DETAIL******* */

  _redirectTripDetails() {
    this.setState({
      modalVisible2: false,
    });
    Actions.AddTripDetails({
      cityId: this.props.cityId,
      cityLocations: this.props.cityLocations,
      cityName: this.props.cityName,
      cityWeather: this.props.cityWeather,
      temperature: this.props.temperature,
      citylat: this.props.citylat,
      citylng: this.props.citylng,
      cityNewArr: this.props.cityNewArr,
    });
  }

  /*************GET USER SELECTED LOCATIONS*********** */

  async _getHeartedPhotoLocations() {
    try {
      await AsyncStorage.getItem('@userLoggedInId').then(value => {
        this.setState({
          userid: value,
        });
      });

      await axios
        .post(
          'http://3.135.63.132:3000/api/v1/tplp/cities/getHeartCityByUserId',
          {
            userid: this.state.userid,
            cityid: this.props.cityId,
          },
        )
        .then(responseJson => {
          for (var i = 0; i < this.props.cityLocations.length; i++) {
            for (var j = 0; j < responseJson.data.data.length; j++) {
              if (
                this.props.cityLocations[i].id ===
                responseJson.data.data[j].photo_location_id
              ) {
                responseJson.data.data[j].name = this.props.cityLocations[
                  i
                ].name;
              }
            }
          }
          this.setState({
            _selectedLocations: responseJson.data.data,
          });
        });
    } catch {
      Alert.alert('Server Error');
    }
  }

  /**************SHOW TRIP DETAILS************** */
  _showTripDetailPage() {
    this.setState({modalVisible2: true});
    setTimeout(() => {
      this.setState({modalVisible2: false});
      Actions.AddTripDetails({
        cityId: this.props.cityId,
        cityLocations: this.props.cityLocations,
        cityName: this.props.cityName,
        cityWeather: this.props.cityIcon,
        temperature: this.props.temperature,
        citylat: this.props.citylat,
        citylng: this.props.citylng,
        cityNewArr: this.props.cityNewArr,
      });
    }, 3000);
  }

  /*****************REDIRECT TO NEARBY*********** */
  _redirectToNearByLocations() {
    Actions.NearbyLocations({
      citylat: this.props.citylat,
      citylng: this.props.citylng,
      cityId: this.props.cityId,
      cityLocations: this.props.cityLocations,
      cityIcon: this.props.cityIcon,
      cityName: this.props.cityName,
      cityWeather: this.props.cityWeather,
      temperature: this.props.temperature,
    });
  }

  render() {
    const {
      cityId,
      cityLocations,
      cityIcon,
      cityName,
      cityWeather,
      temperature,
      citylat,
      citylng,
      cityNewArr,
    } = this.props;
    return (
      <View style={styles.container}>
        <View style={styles.header}>
          <Image
            style={styles.hdr_img}
            source={require('../../assets/LOGO_Blk_.png')}
          />
        </View>

        <ScrollView style={{width: '100%', backgroundColor: '#fff'}}>
          <View
            style={{
              justifyContent: 'center',
              position: 'relative',
              flexDirection: 'row',
              backgroundColor: '#cf8cac',
              paddingVertical: 5,
              paddingHorizontal: 20,
              alignItems: 'center',
            }}>
            <Text
              onPress={() =>
                Actions.individualCity({
                  cityTitle: this.props.cityName,
                  cityId: this.props.cityId,
                  cityLocations: this.props.cityLocations,
                  citylat: this.props.citylat,
                  citylng: this.props.citylng,
                  paymentStatus: this.props.paymentStatus,
                })
              }
              style={{
                fontFamily: 'GeorgiaPro-Bold',
                color: '#fff',
                fontSize: 15,
                marginLeft: 80,
              }}>
              {this.props.cityName}
            </Text>
            <View
              style={{
                marginLeft: 'auto',
                flexDirection: 'row',
                alignItems: 'center',
              }}>
              <View style={{alignItems: 'center', flexDirection: 'row'}}>
                <Image
                  style={{width: 35, height: 35, resizeMode: 'contain'}}
                  source={{uri: this.props.cityIcon}}
                />
                <Text
                  style={{
                    fontFamily: 'BAHNSCHRIFT',
                    color: '#fff',
                    marginLeft: 5,
                  }}>
                  {this.props.temperature}&deg;C
                </Text>
              </View>
              <View
                style={{
                  alignItems: 'center',
                  flexDirection: 'row',
                  marginLeft: 15,
                }}>
                <Image
                  style={{width: 14, resizeMode: 'contain'}}
                  source={require('../../assets/dollar.png')}
                />
                <Text style={{fontFamily: 'BAHNSCHRIFT', color: '#fff'}}>
                  50
                </Text>
              </View>
            </View>
          </View>
          <View style={styles.bannerArea}>
            {/* <Image
              style={styles.bannerImg}
              source={require('../../assets/map.jpg')}
            /> */}

            {this._checkCityLatLong()}
            <View style={styles.createPic}>
              <Image
                style={styles.createPicImg}
                source={require('../../assets/profile3.jpg')}
              />
              <Image
                style={styles.flowerImg}
                source={require('../../assets/pic3.png')}
              />
            </View>
            <View style={styles.dateTravel}>
              <Icon name="ios-calendar" size={17} type="ionicon" color="#111" />
              <Text
                style={{
                  marginLeft: 5,
                  fontFamily: 'BAHNSCHRIFT',
                  color: '#111',
                  fontSize: 12,
                }}>
                {this.state.startTravelDate} - {this.state.endTravelDate}
              </Text>
            </View>
          </View>
          <View style={styles.btnGroup}>
            <View style={styles.btnFlex}>
              <TouchableOpacity
                style={styles.btnEmail}
                onPress={() => this._redirectToNearByLocations()}>
                <View style={{flexDirection: 'row'}}>
                  <Icon
                    name="map-marker"
                    size={15}
                    type="font-awesome"
                    color="#cf8cac"
                  />
                  <Text
                    style={{
                      color: '#cf8cac',
                      fontFamily: 'BAHNSCHRIFT',
                      marginLeft: 5,
                    }}>
                    Nearby
                  </Text>
                </View>
              </TouchableOpacity>
            </View>
            <View style={(styles.btnFlex, styles.centerBtn)}>
              <TouchableOpacity
                onPress={() => {
                  this.setState({modalVisible: true});
                }}
                style={(styles.btnEmail, styles.expandBtn)}>
                <Icon
                  name="arrow-up"
                  size={20}
                  type="font-awesome"
                  color="#cf8cac"
                />
              </TouchableOpacity>
            </View>
            <View style={styles.btnFlex}>
              <TouchableOpacity
               onPress={() => {
                      this.setState({
                        modalVisible2: true,
                      });
                }}
                style={styles.btnEmail}>
                <View style={{flexDirection: 'row'}}>
                  <Icon name="eye" size={15} type="font-awesome" color="#cf8cac" />
                  <Text
                    style={{
                      color: '#cf8cac',
                      fontFamily: 'BAHNSCHRIFT',
                      marginLeft: 5,
                    }}>
                    Trip Details
                  </Text>
                </View>
              </TouchableOpacity>
            </View>
          </View>
          <View style={styles.contentArea}>
            <View
              style={{
                justifyContent: 'center',
                position: 'relative',
                flexDirection: 'row',
              }}>
              { this.state.itenaryArray.length > 0 ?
              <FlatList
                data={this.state.itenaryArray}
                renderItem={({item, index}) => (
                  <View style={styles.flexGrid}>
                    <Image
                      style={styles.gridImg}
                      source={{
                        uri: '',
                      }}
                    />
                    <Text style={styles.gridTitle}>
                      {item.areaName} {index + 1}
                    </Text>

                    <View style={styles.favorite}>
                      {item.open === true ? (
                        <TouchableOpacity
                          onPress={() => {
                            this._openCloseAreas(index, 'true');
                          }}>
                          <Image
                            style={styles.eyeImg}
                            source={require('../../assets/openeyepink.png')}
                          />
                        </TouchableOpacity>
                      ) : (
                        <TouchableOpacity
                          onPress={() => {
                            this._openCloseAreas(index, 'false');
                          }}>
                          <Image
                            style={styles.eyeImg}
                            source={require('../../assets/closeeyepink.png')}
                          />
                        </TouchableOpacity>
                      )}
                    </View>
                  </View>
                )}
                //Setting the number of column
                numColumns={3}
                keyExtractor={(item, index) => index.toString()}
              />
              :                 
              <View style={styles.fullWidthItenary}>
                  <Text style={styles.notFound}>NO ITINARIES FOUND !</Text>
                </View> }
            </View>
          </View>

          {/* Expand Popup 1 */}
          <View style={{alignItems: 'center', justifyContent: 'center'}}>
            <Modal
              style={{
                alignItems: 'center',
                justifyContent: 'center',
                height: '100%',
              }}
              animationType="fade"
              transparent={true}
              visible={this.state.modalVisible}
              onRequestClose={() => {
                Alert.alert('Modal has been closed.');
              }}>
              <View
                style={{
                  backgroundColor: '#e6c4d4',
                  marginTop: 0,
                  height: 800,
                  paddingLeft: 20,
                  paddingVertical: 20,
                  width: '100%',
                }}>
                <TouchableOpacity
                  style={styles.btnClose}
                  onPress={() => {
                    this.setState({modalVisible: false});
                  }}>
                  <View>
                    <Icon
                      name="ios-close"
                      size={30}
                      type="ionicon"
                      color="#fff"
                    />
                  </View>
                </TouchableOpacity>

                <View style={styles.fullWidth}>
                  <Text
                    style={{
                      fontFamily: 'Southampton',
                      color: '#fff',
                      fontSize: 45,
                      textAlign: 'center',
                      marginBottom: 10,
                    }}>
                    Full Itinerary
                  </Text>

                  <ScrollView style={{height: 610}}>
                    {this._renderItinariesAreas()}
                  </ScrollView>
                </View>
              </View>
            </Modal>

            {/* Expand Popup 2 */}
            <Modal
              style={{
                alignItems: 'center',
                justifyContent: 'center',
                height: '100%',
              }}
              animationType="fade"
              transparent={true}
              visible={this.state.modalVisible2}
              onRequestClose={() => {
                Alert.alert('Modal has been closed.');
              }}>
              <ImageBackground
                source={require('../../assets/overlaybg.png')}
                style={{width: '100%', height: '100%'}}>
                <View
                  style={{
                    backgroundColor: 'rgba(255,255,255,.4)',
                    marginTop: 0,
                    height: 900,
                    paddingHorizontal: 20,
                    paddingVertical: 20,
                    width: '100%',
                  }}>
                  <View
                    style={{
                      backgroundColor: '#f0e0e2',
                      borderRadius: 10,
                      marginTop: 160,
                      paddingHorizontal: 20,
                      paddingTop: 40,
                      paddingBottom: 30,
                      borderWidth: 1,
                      borderColor: '#cf8cac',
                    }}>
                    <Image
                      style={styles.flowerImg}
                      source={require('../../assets/pic3.png')}
                    />
                    <Image
                      style={styles.flowerImg2}
                      source={require('../../assets/pic3.png')}
                    />
                    <View style={styles.fullWidth}>
                      <Text
                        style={{
                          fontFamily: 'BAHNSCHRIFT',
                          color: '#cf8cac',
                          fontSize: 22,
                          textAlign: 'center',
                          marginBottom: 10,
                          lineHeight: 30,
                        }}>
                        Fill in your trip details BELOW for the optimal TPLP
                        experience…
                      </Text>
                    </View>
                    <TouchableOpacity
                      style={styles.btnTripDetail}
                      onPress={() => this._redirectTripDetails()}>
                      <Text
                        style={{
                          fontSize: 15,
                          color: '#fff',
                          fontFamily: 'GeorgiaPro-Bold',
                        }}>
                        Trip Details
                      </Text>
                    </TouchableOpacity>
                    <Text
                      style={{
                        fontFamily: 'BAHNSCHRIFT',
                        color: '#cf8cac',
                        fontSize: 15,
                        textAlign: 'center',
                        marginTop: 20,
                        lineHeight: 30,
                      }}>
                      Haven't booked yet, that’s okay…
                    </Text>
                  </View>

                  <TouchableOpacity
                    style={styles.btnRemindme}
                     onPress={() => {
                      this.setState({
                        modalVisible2: false,
                      });
                    }}
                    >
                    <Text
                      style={{
                        fontSize: 15,
                        color: '#cf8cac',
                        fontFamily: 'BAHNSCHRIFT',
                      }}>
                      Remind Me Later
                    </Text>
                  </TouchableOpacity>
                </View>
              </ImageBackground>
            </Modal>

            <Modal
              style={{
                alignItems: 'center',
                justifyContent: 'center',
                height: '100%',
              }}
              animationType="fade"
              transparent={true}
              visible={this.state.nodatamodal}>
              <ImageBackground
                source={require('../../assets/overlaybg.png')}
                style={{width: '100%', height: '100%'}}>
                <View
                  style={{
                    backgroundColor: 'rgba(255,255,255,.4)',
                    marginTop: 0,
                    height: 900,
                    paddingHorizontal: 20,
                    paddingVertical: 20,
                    marginTop: 0,
                    width: '100%',
                  }}>
                  <TouchableOpacity
                    style={styles.btnClose}
                    onPress={() => {
                      this.setState({nodatamodal: false});
                    }}>
                    <View>
                      <Icon
                        name="ios-close"
                        size={30}
                        type="ionicon"
                        color="#fff"
                      />
                    </View>
                  </TouchableOpacity>
                  <View
                    style={{
                      backgroundColor: '#f0e0e2',
                      borderRadius: 10,
                      marginTop: 160,
                      paddingHorizontal: 20,
                      paddingTop: 40,
                      paddingBottom: 30,
                      borderWidth: 1,
                      borderColor: '#cf8cac',
                    }}>
                    <Image
                      style={styles.flowerImg}
                      source={require('../../assets/pic3.png')}
                    />
                    <Image
                      style={styles.flowerImg2}
                      source={require('../../assets/pic3.png')}
                    />
                    <View style={styles.fullWidth}>
                      <Text
                        style={{
                          fontFamily: 'BAHNSCHRIFT',
                          color: '#cf8cac',
                          fontSize: 22,
                          textAlign: 'center',
                          marginBottom: 10,
                          lineHeight: 30,
                        }}>
                        Aww shucks we can’t find anything!
                      </Text>
                    </View>
                  </View>
                </View>
              </ImageBackground>
            </Modal>

            {/* Expand Popup 3 */}
            <Modal
              style={{
                alignItems: 'center',
                justifyContent: 'center',
                height: '100%',
              }}
              animationType="fade"
              transparent={true}
              visible={this.state.modalVisible3}
              onRequestClose={() => {
                Alert.alert('Modal has been closed.');
              }}>
              <ImageBackground
                source={require('../../assets/overlaybg.png')}
                style={{width: '100%', height: '100%'}}>
                <View
                  style={{
                    backgroundColor: 'rgba(255,255,255, .9)',
                    marginTop: 0,
                    height: 900,
                    paddingLeft: 20,
                    paddingVertical: 20,
                    width: '100%',
                  }}>
                  <TouchableOpacity
                    style={styles.btnClose}
                    onPress={() => {
                      this.setState({modalVisible3: false});
                    }}>
                    <View>
                      <Icon
                        name="ios-close"
                        size={30}
                        type="ionicon"
                        color="#fff"
                      />
                    </View>
                  </TouchableOpacity>
                  <ScrollView style={{maxHeight: 680, paddingRight: 20}}>
                    <View
                      style={{
                        borderRadius: 10,
                        marginTop: 70,
                        alignItems: 'center',
                        paddingBottom: 25,
                      }}>
                      <Image
                        style={styles.locationImg}
                        source={require('../../assets/Picture7.jpg')}
                      />
                      <View
                        style={{
                          flexDirection: 'row',
                          alignItems: 'center',
                          width: '100%',
                        }}>
                        <View style={styles.ratingMain}>
                          <Icon
                            name="ios-star"
                            size={20}
                            type="ionicon"
                            color="#cf8cac"
                          />
                          <Icon
                            name="ios-star"
                            size={20}
                            type="ionicon"
                            color="#cf8cac"
                          />
                          <Icon
                            name="ios-star"
                            size={20}
                            type="ionicon"
                            color="#cf8cac"
                          />
                          <Icon
                            name="ios-star"
                            size={20}
                            type="ionicon"
                            color="#cf8cac"
                          />
                          <Icon
                            name="ios-star-outline"
                            size={20}
                            type="ionicon"
                            color="#cf8cac"
                          />
                        </View>
                        <View style={{marginLeft: 'auto'}}>
                          <Icon
                            name="ios-heart"
                            size={20}
                            type="ionicon"
                            color="#cf8cac"
                          />
                        </View>
                      </View>
                      <View style={styles.btnsecret}>
                        <Text
                          style={{
                            fontSize: 15,
                            color: '#fff',
                            fontFamily: 'BAHNSCHRIFT',
                          }}>
                          ‘HEART’ TO ADD TO ITINERARY
                        </Text>
                        <Image
                          style={styles.flowerImgpop}
                          source={require('../../assets/pic3.png')}
                        />
                      </View>
                    </View>

                    <View
                      style={{
                        padding: 15,
                        backgroundColor: '#f0e0e2',
                        borderWidth: 1,
                        borderColor: '#e7c7d6',
                        borderRadius: 10,
                        flex: 1,
                        justifyContent: 'flex-end',
                        alignItems: 'flex-end',
                      }}>
                      <View
                        style={{
                          width: '100%',
                          marginBottom: 10,
                          flexDirection: 'row',
                          alignItems: 'center',
                        }}>
                        <View>
                          <Text
                            style={{
                              fontSize: 18,
                              color: '#cf8cac',
                              fontFamily: 'GeorgiaPro-Bold',
                              textTransform: 'uppercase',
                            }}>
                            Comments
                          </Text>
                        </View>
                        <View style={{marginLeft: 'auto'}}>
                          <Icon
                            name="angle-down"
                            size={25}
                            type="font-awesome"
                            color="#cf8cac"
                          />
                        </View>
                      </View>
                      <View style={styles.comntList}>
                        <View style={styles.comntCols}>
                          <Text style={styles.UserName}>Sheton Bishop</Text>
                          <Text style={styles.ComntTxt}>
                            This swing is amazing but go EARLYYY
                          </Text>
                        </View>
                        <View style={styles.comntCols2}>
                          <View style={styles.ratingUser}>
                            <Icon
                              name="ios-star"
                              size={16}
                              type="ionicon"
                              color="#cf8cac"
                            />
                            <Icon
                              name="ios-star"
                              size={16}
                              type="ionicon"
                              color="#cf8cac"
                            />
                            <Icon
                              name="ios-star"
                              size={16}
                              type="ionicon"
                              color="#cf8cac"
                            />
                            <Icon
                              name="ios-star"
                              size={16}
                              type="ionicon"
                              color="#cf8cac"
                            />
                            <Icon
                              name="ios-star"
                              size={16}
                              type="ionicon"
                              color="#cf8cac"
                            />
                          </View>
                        </View>
                      </View>
                      <View style={styles.comntList}>
                        <View style={styles.comntCols}>
                          <Text style={styles.UserName}>Sheton Bishop</Text>
                          <Text style={styles.ComntTxt}>
                            This swing is amazing but go EARLYYY
                          </Text>
                        </View>
                        <View style={styles.comntCols2}>
                          <View style={styles.ratingUser}>
                            <Icon
                              name="ios-star"
                              size={16}
                              type="ionicon"
                              color="#cf8cac"
                            />
                            <Icon
                              name="ios-star"
                              size={16}
                              type="ionicon"
                              color="#cf8cac"
                            />
                            <Icon
                              name="ios-star"
                              size={16}
                              type="ionicon"
                              color="#cf8cac"
                            />
                            <Icon
                              name="ios-star"
                              size={16}
                              type="ionicon"
                              color="#cf8cac"
                            />
                            <Icon
                              name="ios-star"
                              size={16}
                              type="ionicon"
                              color="#cf8cac"
                            />
                          </View>
                        </View>
                      </View>
                      <View style={styles.comntList}>
                        <View style={styles.comntCols}>
                          <Text style={styles.UserName}>Sheton Bishop</Text>
                          <Text style={styles.ComntTxt}>
                            This swing is amazing but go EARLYYY
                          </Text>
                        </View>
                        <View style={styles.comntCols2}>
                          <View style={styles.ratingUser}>
                            <Icon
                              name="ios-star"
                              size={16}
                              type="ionicon"
                              color="#cf8cac"
                            />
                            <Icon
                              name="ios-star"
                              size={16}
                              type="ionicon"
                              color="#cf8cac"
                            />
                            <Icon
                              name="ios-star"
                              size={16}
                              type="ionicon"
                              color="#cf8cac"
                            />
                            <Icon
                              name="ios-star"
                              size={16}
                              type="ionicon"
                              color="#cf8cac"
                            />
                            <Icon
                              name="ios-star"
                              size={16}
                              type="ionicon"
                              color="#cf8cac"
                            />
                          </View>
                        </View>
                      </View>
                      <View style={styles.comntList}>
                        <View style={styles.comntCols}>
                          <Text style={styles.UserName}>Sheton Bishop</Text>
                          <Text style={styles.ComntTxt}>
                            This swing is amazing but go EARLYYY
                          </Text>
                        </View>
                        <View style={styles.comntCols2}>
                          <View style={styles.ratingUser}>
                            <Icon
                              name="ios-star"
                              size={16}
                              type="ionicon"
                              color="#cf8cac"
                            />
                            <Icon
                              name="ios-star"
                              size={16}
                              type="ionicon"
                              color="#cf8cac"
                            />
                            <Icon
                              name="ios-star"
                              size={16}
                              type="ionicon"
                              color="#cf8cac"
                            />
                            <Icon
                              name="ios-star"
                              size={16}
                              type="ionicon"
                              color="#cf8cac"
                            />
                            <Icon
                              name="ios-star"
                              size={16}
                              type="ionicon"
                              color="#cf8cac"
                            />
                          </View>
                        </View>
                      </View>
                      <View style={styles.comntList}>
                        <View style={styles.comntCols}>
                          <Text style={styles.UserName}>Sheton Bishop</Text>
                          <Text style={styles.ComntTxt}>
                            This swing is amazing but go EARLYYY
                          </Text>
                        </View>
                        <View style={styles.comntCols2}>
                          <View style={styles.ratingUser}>
                            <Icon
                              name="ios-star"
                              size={16}
                              type="ionicon"
                              color="#cf8cac"
                            />
                            <Icon
                              name="ios-star"
                              size={16}
                              type="ionicon"
                              color="#cf8cac"
                            />
                            <Icon
                              name="ios-star"
                              size={16}
                              type="ionicon"
                              color="#cf8cac"
                            />
                            <Icon
                              name="ios-star"
                              size={16}
                              type="ionicon"
                              color="#cf8cac"
                            />
                            <Icon
                              name="ios-star"
                              size={16}
                              type="ionicon"
                              color="#cf8cac"
                            />
                          </View>
                        </View>
                      </View>
                    </View>
                  </ScrollView>
                </View>
              </ImageBackground>
            </Modal>
          </View>
        </ScrollView>

        <View style={styles.footMain}>
          <View style={styles.footFlex}>
            <TouchableOpacity onPress={() => Actions.exploreRegion()}>
              <Icon name="ios-planet" size={35} type="ionicon" color="#fff" />
            </TouchableOpacity>
          </View>

          <View style={styles.footFlex}>
            <TouchableOpacity onPress={() => Actions.Profile()}>
              <Icon size={30} name="ios-person" type="ionicon" color="#fff" />
            </TouchableOpacity>
          </View>

          <View style={styles.footFlex}>
            <TouchableOpacity
              style={styles.centerMenu}
              onPress={this._onSubmit.bind(this)}>
              <Image
                style={{
                  marginTop: 20,
                  width: 40,
                  height: 40,
                  resizeMode: 'contain',
                }}
                source={require('../../assets/heartlock.png')}
              />
            </TouchableOpacity>
          </View>

          <View style={styles.footFlex}>
            <TouchableOpacity onPress={() => Actions.home()}>
              <Icon size={27} name="ios-home" type="ionicon" color="#fff" />
            </TouchableOpacity>
          </View>

          <View style={styles.footFlex}>
            <TouchableOpacity onPress={() => Actions.Profile()}>
              <Icon size={27} name="ios-search" type="ionicon" color="#fff" />
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  favNoti: {
    position: 'absolute',
    top: -5,
    right: -10,
    fontFamily: 'BAHNSCHRIFT',
    width: 15,
    fontSize: 11,
    height: 15,
    borderRadius: 50,
    backgroundColor: '#cf8cac',
    color: '#fff',
    textAlign: 'center',
    alignItems: 'center',
    justifyContent: 'center',
  },
  comntList: {
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: 'rgba(255,255,255, 1)',
    paddingHorizontal: 15,
    paddingVertical: 10,
    borderRadius: 5,
    marginBottom: 5,
  },
  UserName: {
    fontFamily: 'GeorgiaPro-Bold',
    fontSize: 13,
    marginBottom: 3,
  },
  ComntTxt: {
    fontFamily: 'BAHNSCHRIFT',
    fontSize: 12,
    lineHeight: 15,
  },
  comntCols: {
    flex: 1,
  },
  comntCols2: {
    flex: 1,
    marginRight: 'auto',
    alignItems: 'flex-end',
    maxWidth: 80,
  },
  ratingUser: {
    flexDirection: 'row',
  },
  ratingMain: {
    flexDirection: 'row',
  },
  goRight: {
    width: 20,
    height: 20,
    resizeMode: 'contain',
    right: 10,
    top: 16,
  },
  locationImg: {
    width: '100%',
    height: 250,
    resizeMode: 'cover',
    borderRadius: 10,
  },
  flowerImg2: {
    width: 60,
    height: 60,
    zIndex: 11,
    position: 'absolute',
    resizeMode: 'contain',
    left: -15,
    top: -15,
  },
  flowerImg5: {
    width: 50,
    zIndex: 11,
    height: 50,
    position: 'absolute',
    resizeMode: 'contain',
    left: 0,
    transform: [{rotate: '-160deg'}],
    top: -20,
  },
  flowerImgpop: {
    width: 40,
    zIndex: 11,
    height: 40,
    position: 'absolute',
    resizeMode: 'contain',
    right: -15,
    bottom: -5,
  },
  flowerImg: {
    width: 60,
    zIndex: 11,
    height: 60,
    position: 'absolute',
    resizeMode: 'contain',
    right: -15,
    bottom: -15,
  },
  eyeImg: {
    width: 20,
    height: 20,
    resizeMode: 'contain',
  },
  checkboxTxt: {
    flex: 1,
    fontFamily: 'BAHNSCHRIFT',
    color: '#fff',
    fontSize: 13,
    lineHeight: 12,
    marginTop: 10,
    padding: 0,
  },
  checkBox: {
    marginLeft: 'auto',
    maxWidth: 60,
    maxHeight: 45,
    color: '#fff',
  },
  fullWidth: {
    width: '100%',
    position: 'relative',
  },
  checkboxRow: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  btnClose: {
    borderRadius: 30,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    position: 'absolute',
    top: 30,
    right: 20,
    zIndex: 99,
    width: 30,
    height: 30,
    paddingBottom: 0,
    backgroundColor: '#cf8cac',
  },
  expandBtn: {
    backgroundColor: '#f0e0e2',
    borderWidth: 2,
    borderColor: '#cf8cac',
    textAlign: 'center',
    borderRadius: 0,
    fontSize: 14,
    marginBottom: 15,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    flex: 1,
    marginHorizontal: 5,
  },
  centerBtn: {
    minWidth: 60,
  },
  btnFlex: {
    flex: 1,
  },
  btnGroup: {
    flexDirection: 'row',
    paddingHorizontal: 20,
    paddingTop: 15,
  },
  dateTravel: {
    position: 'absolute',
    top: 10,
    right: 10,
    zIndex: 99,
    backgroundColor: '#e7c7d6',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 100,
    paddingHorizontal: 15,
    paddingTop: 3,
    paddingBottom: 5,
    flexDirection: 'row',
  },
  createPic: {
    position: 'absolute',
    top: -48,
    left: -1,
    zIndex: 99,
    backgroundColor: '#e7c7d6',
    alignItems: 'center',
    justifyContent: 'center',
    borderTopRightRadius: 100,
    borderBottomLeftRadius: 100,
    borderBottomRightRadius: 100,
    width: 90,
    height: 90,
    borderWidth: 1,
    borderColor: '#fff',
  },
  createPicImg: {
    width: 80,
    height: 80,
    resizeMode: 'cover',
    borderTopRightRadius: 100,
    borderBottomRightRadius: 100,
    borderBottomLeftRadius: 100,
  },
  logout: {
    position: 'absolute',
    right: 20,
    top: 15,
  },
  favorite: {
    position: 'absolute',
    right: 5,
    bottom: 5,
    zIndex: 999999,
  },
  letsEdit: {
    position: 'absolute',
    right: 10,
    bottom: 0,
    top: 0,
    zIndex: 0,
    opacity: 0.5,
    width: 140,
    resizeMode: 'contain',
  },
  gridTitle: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: '30%',
    margin: 'auto',
    fontSize: 12,
    textAlign: 'center',
    color: '#cf8cac',
    paddingVertical: 5,
    backgroundColor: 'rgba(240,224,226, .8)',
    elevation: 1,
    marginVertical: 10,
    fontFamily: 'GeorgiaPro-Bold',
  },
  TouchTitle: {
    position: 'absolute',
    left: 0,
    right: 0,
    zIndex: 999,
  },
  gridImg: {
    width: '100%',
    height: 120,
    resizeMode: 'cover',
    opacity: 0.5,
    borderRadius: 5,
    borderWidth: 1,
    borderColor: '#e9f0f6',
  },
  flexGrid: {
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 0,
    height: 120,
    width: '33.3%',
    borderRadius: 5,
    backgroundColor: '#4472c4',
  },
  weKnow: {
    color: '#cf8cac',
    fontSize: 16,
    textAlign: 'center',
    lineHeight: 22,
    marginTop: 20,
    marginBottom: 20,
    paddingVertical: 10,
    paddingHorizontal: 40,
    fontWeight: 'bold',
    backgroundColor: '#f0e0e2',
    borderRadius: 100,
  },
  absoluTxt: {
    position: 'absolute',
    bottom: 20,
    left: 20,
    top: 20,
    backgroundColor: 'rgba(255,255,255, .7)',
    right: 20,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    paddingBottom: 10,
    borderRadius: 5,
    elevation: 1,
  },
  bannerArea: {
    alignItems: 'center',
    justifyContent: 'center',
    position: 'relative',
    backgroundColor: '#fff',
    borderWidth: 1,
    borderColor: '#ddd',
  },
  bannerImg: {
    height: 400,
    width: '100%',
    // resizeMode: 'cover',
  },
  contentArea: {
    paddingHorizontal: 20,
    paddingBottom: 20,
  },
  footMain: {
    backgroundColor: '#cf8cac',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    paddingTop: 10,
    paddingBottom: 10,
    maxHeight: 55,
  },
  centerMenu: {
    backgroundColor: '#cf8cac',
    position: 'relative',
    top: -15,
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    borderTopLeftRadius: 50,
    borderTopRightRadius: 50,
  },
  footFlex: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-start',
    height: '100%',
    paddingTop: 30,
  },
  header: {
    position: 'relative',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    elevation: 1,
    // backgroundColor: '#f0e0e2',
    width: '100%',
    paddingTop: 6,
    paddingBottom: 6,
  },
  hdr_img: {
    width: 200,
    height: 50,
    marginBottom: 5,
    resizeMode: 'stretch',
  },
  body: {
    flex: 1,
    height: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  body_img: {
    alignItems: 'center',
    width: 220,
    height: 186,
  },
  ftr_hdngs: {
    textAlign: 'center',
    color: '#fff',
    fontSize: 25,
  },
  instagram: {
    backgroundColor: '#cf8cac',
    alignItems: 'stretch',
    paddingVertical: 13,
    textAlign: 'center',
    lineHeight: 20,
    borderRadius: 30,
    paddingBottom: 15,
    fontSize: 16,
    marginBottom: 15,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    flex: 1,
    marginRight: 5,
  },
  btnEmail: {
    paddingVertical: 15,
    backgroundColor: '#f0e0e2',
    textAlign: 'center',
    borderRadius: 0,
    borderWidth: 2,
    borderColor: '#cf8cac',
    fontSize: 14,
    marginBottom: 15,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    flex: 1,
  },
  btnTripDetail: {
    paddingVertical: 15,
    backgroundColor: '#cf8cac',
    borderRadius: 30,
    marginTop: 25,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    flex: 1,
    maxHeight: 50,
    minHeight: 50,
    marginHorizontal: 50,
  },
  btnsecret: {
    paddingVertical: 15,
    backgroundColor: '#cf8cac',
    borderRadius: 30,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    flex: 1,
    maxHeight: 50,
    borderWidth: 1,
    borderColor: '#fff',
    minHeight: 40,
    paddingHorizontal: 20,
    position: 'absolute',
    top: -25,
    zIndex: 11,
  },
  btnRemindme: {
    paddingVertical: 15,
    borderRadius: 30,
    marginTop: 25,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    flex: 1,
    borderWidth: 1,
    borderColor: '#cf8cac',
    maxHeight: 40,
    minHeight: 40,
    marginHorizontal: 70,
  },
  dottedstyle: {
    fontSize: 40,
    color: '#cf8cac',
  },
  alignDots: {
    marginTop: 0,
    alignItems: 'center',
    justifyContent: 'center',
  },
  fullWidthItenary: {
    width: '100%',
    paddingTop: 10,
    paddingBottom: 10,
    display: 'flex',
    flexDirection: 'column',
    borderWidth: 2,
    alignItems: 'center',
    justifyContent: 'center',
    borderColor: '#cf8cac',
  },
  fullWidthItenary2: {
    width: '100%',
    marginTop: 200,
    paddingTop: 10,
    paddingBottom: 10,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
  },
  notFound: {
    color: '#cf8cac',
  }
});
