import React from 'react';
import {View, Image, AsyncStorage} from 'react-native';
import {Actions} from 'react-native-router-flux';

class SplashScreen extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        {/* <Image
          style={styles.bg}
          resizeMode="cover"
          source={require('../../assets/Blooming-Rose-GIF.gif')}
        /> */}
        <Image
          style={styles.logo}
          resizeMode="cover"
          source={require('../../assets/Blooming-Rose-GIF.gif')}
        />
      </View>
    );
  }
  async componentDidMount() {
    await AsyncStorage.getItem('@userRemember').then(value => {
      setTimeout(() => {
        console.log(value);
        if (value == null || value === '' || value === undefined) {
          Actions.Signin();
        } else {
          Actions.home();
        }
      }, 4500);
    });
  }
}

const styles = {
  container: {
    flex: 1,
    justifyContent: 'center',
    color: '#000000',
    width: '100%',
    height: '100%',
  },
  logo: {
    alignSelf: 'center',
  },
  bg: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    width: '100%',
    height: '100%',
  },
};

export default SplashScreen;
