/* eslint-disable react-native/no-inline-styles */
import React, {Component, useState} from 'react';
import {
  Modal,
  Alert,
  Platform,
  AppRegistry,
  StyleSheet,
  Text,
  View,
  TextInput,
  Image,
  Button,
  AsyncStorage,
  TouchableHighlight,
  TouchableOpacity,
  ScrollView,
  FlatList,
  Linking,
  ImageBackground,
} from 'react-native';
import {Icon, CheckBox, Input} from 'react-native-elements';
import {Actions} from 'react-native-router-flux';
import axios from 'axios';
import {
  Collapse,
  CollapseHeader,
  CollapseBody,
  AccordionList,
} from 'accordion-collapse-react-native';
import Mailer from 'react-native-mail';
import moment from 'moment';
import {REACT_APP_WEATHER_API_KEY} from 'react-native-dotenv';
import * as geolib from 'geolib';

class Profile extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      activeSections: [0],
      value: '',
      userid: '',
      photos: [],
      comment: '',
      citiesList: '',
      myOutfitPlannerArr: [],
      myTripsArr: [],
      temperatureC: '',
      icon: '',
      counter: 0,
      arr: [],
      api_key: REACT_APP_WEATHER_API_KEY,
      cityLocations: '',
      showPhotoitenaryPopup: false,
      locationsArr: [],
      hideCities: false,
    };
  }

  /*********GET USER LOGGED IN ID************ */

  async componentDidMount() {
    await AsyncStorage.getItem('@userLoggedInId').then(value => {
      this.setState({
        userid: value,
      });
    });
    await this._getCities();
    await this._renderPhotos();
    await this._outfitPlanner();
    await this._myTrips();
  }

  async _outfitPlanner() {
    var arr = [];
    await axios
      .post('http://3.135.63.132:3000/api/v1/tplp/getCities', {
        userid: this.state.userid,
      })
      .then(responseJson => {
        var purchasedCities = responseJson.data.data;
        if (purchasedCities.length > 0) {
          for (var i = 0; i < purchasedCities.length; i++) {
            this.state.citiesList.filter(city => {
              if (purchasedCities[i].cityId.match(city._id) != null) {
                arr.push(city);
                this.setState({
                  myOutfitPlannerArr: arr,
                });
                console.log(this.state.myOutfitPlannerArr);
              }
            });
          }
        }
      });
  }

  async _myTrips() {
    var arr = [];
    await axios
      .post(
        'http://3.135.63.132:3000/api/v1/tplp/hotels/get_hotel_By_Userid',
        {
          userid: this.state.userid,
        },
      )
      .then(responseJson => {
        var purchasedCities = responseJson.data.data;
        if (purchasedCities.length > 0) {
          for (var i = 0; i < purchasedCities.length; i++) {
            this.state.citiesList.filter(city => {
              if (purchasedCities[i].cityId.match(city._id) != null) {
                arr.push(city);
                this.setState({
                  myTripsArr: arr,
                });
              }
            });
          }
        }
      });
  }

  async _showSelectedLocations(item) {
    this.setState({
      showPhotoitenaryPopup: true,
    });
    console.log(item);
    await AsyncStorage.getItem('@userLoggedInId').then(value => {
      console.log(value);
      this.setState({
        userid: value,
      });
    });

    await axios
      .post(
        'http://3.135.63.132:3000/api/v1/tplp/cities/getHeartCityByUserId',
        {
          userid: this.state.userid,
          cityid: item._id,
        },
      )
      .then(responseJson => {
        console.log(responseJson);

        if (responseJson.data.data.length === 0) {
          this.setState({
            showPhotoitenaryPopup: false,
          });
        } else {
          const locationData = responseJson.data.data;
          for (var i = 0; i < item.photoLocations.length; i++) {
            for (var j = 0; j < locationData.length; j++) {
              if (
                item.photoLocations[i].id.toString() ===
                locationData[j].photo_location_id.toString()
              ) {
                item.photoLocations[i].hearted = 'true';
                this.setState({
                  cityLocations: item.photoLocations,
                });
              }
            }
          }

          var locationsArr = [];
          this.state.cityLocations.filter(city => {
            console.log(city.hearted);
            if (city.hearted === 'true') {
              locationsArr.push(city);
            }
          });

          this.setState({
            cityLocations: locationsArr,
          });
          console.log(this.state.cityLocations);
          this._checkSelectedLocations(this.state.cityLocations, item);
        }
      });
  }

  async _checkSelectedLocations(Locations, item) {
    if (this.state.counter !== Locations.length - 1) {
      for (var j = 0; j < Locations.length; j++) {
        if (j !== Locations.length - 1) {
          if (Locations[this.state.counter] !== Locations[j]) {
            const api_call = await fetch(
              'https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=' +
                Locations[this.state.counter].lat +
                ',' +
                Locations[this.state.counter].long +
                '+&radius=3000&type=point_of_interest&keyword=' +
                Locations[j].name +
                '&key=AIzaSyAUG9ZACOjwqlMYmEqlkXPYMz1wZ14H8zs',
            );
            const locationres = await api_call.json();
            console.log(locationres);
            if (locationres.status === 'OK') {
              const dist = geolib.getDistance(
                {
                  latitude: Locations[this.state.counter].lat,
                  longitude: Locations[this.state.counter].long,
                },
                {latitude: Locations[j].lat, longitude: Locations[j].long},
              );
              Locations[j].distance = dist;
              Locations[j].hide = false;
              this.state.arr.push(Locations[j]);
            }
          }
        } else if (j === Locations.length - 1) {
          if (Locations[this.state.counter] !== Locations[j]) {
            const api_call = await fetch(
              'https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=' +
                Locations[this.state.counter].lat +
                ',' +
                Locations[this.state.counter].long +
                '+&radius=3000&type=point_of_interest&keyword=' +
                Locations[j].name +
                '&key=AIzaSyAUG9ZACOjwqlMYmEqlkXPYMz1wZ14H8zs',
            );
            const locationres = await api_call.json();
            console.log(locationres);
            if (locationres.status === 'OK') {
              const dist = geolib.getDistance(
                {
                  latitude: Locations[this.state.counter].lat,
                  longitude: Locations[this.state.counter].long,
                },
                {latitude: Locations[j].lat, longitude: Locations[j].long},
              );
              Locations[j].distance = dist;
              Locations[j].hide = false;
              this.state.arr.push(Locations[j]);
            } else {
            }
            this.state.counter += 1;
            this._checkSelectedLocations(Locations, item);
          }
        }
      }
    } else if (this.state.counter === Locations.length - 1) {
      // Declare a new array
      let newArray = [];

      // Declare an empty object
      let uniqueObject = {};

      console.log(this.state.arr);

      for (let i in this.state.arr) {
        // Extract the title
        var id = this.state.arr[i]['id'];
        // Use the title as the index
        uniqueObject[id] = this.state.arr[i];
      }

      // Loop to push unique object into array
      for (var i in uniqueObject) {
        newArray.push(uniqueObject[i]);
      }

      console.log(newArray);
      this.setState({
        showPhotoitenaryPopup: false,
        locationsArr: newArray,
        hideCities: true,
      });
    }
  }

  _renderLocations() {
    return this.state.locationsArr.map((item, index) => {
      return (
        <View style={styles.flexGridFrame}>
          <Text style={styles.gridFrameTitle}>{item.name}</Text>
          <View
            style={{
              justifyContent: 'center',
              alignItems: 'center',
              height: '100%',
            }}>
            <TouchableOpacity style={styles.addImg}>
              <Icon name="ios-add" size={30} type="ionicon" color="#fff" />
            </TouchableOpacity>
          </View>
        </View>
      );
    });
  }

  _getCities() {
    fetch('http://3.135.63.132:3000/api/v1/tplp/cities/getCitiesList')
      .then(response => response.json())
      .then(json => {
        console.log(json);
        if (json.list.length > 0) {
          for (var i = 0; i < json.list.length; i++) {
            json.list[i].cityimage =
              'http://3.135.63.132:3000/images/' + json.list[i].cityimage;
          }
          this.setState({
            citiesList: json.list,
          });
        } else {
        }
      });
  }

  async _renderPhotos() {
    await axios
      .post('http://3.135.63.132:3000/api/v1/tplp/getPhotos', {
        userid: this.state.userid,
      })
      .then(responseJson => {
        console.log(responseJson);
        this.setState({
          photos: responseJson.data.data.photos,
        });
      },err=>{
        console.log(err);
      });
  }

  _onSubmit = () => {
    AsyncStorage.clear();
    Actions.Signin();
  };

  listener(uri) {
    Linking.addEventListener(uri, this._handleOpenURL);
  }

  goToInsta() {
    let concatedFinalUrl = 'https://www.instagram.com/theprettylittlepost';
    Linking.canOpenURL(concatedFinalUrl)
      .then(supported => {
        if (!supported) {
        } else {
          this.listener(concatedFinalUrl);
          return Linking.openURL(concatedFinalUrl);
        }
      })
      .catch(err => console.error('An error occurred', err));
  }

  /**********HANDLE MAIL********** */

  handleEmail = () => {
    setTimeout(() => {
      this.setState({
        comment: '',
      });
    }, 3000);
    Mailer.mail(
      {
        subject: 'THEPRETTYLITTLEPOST',
        recipients: ['harmanpreetindiit@gmail.com'],
        // ccRecipients: ['supportCC@example.com'],
        // bccRecipients: ['supportBCC@example.com'],
        body: this.state.comment,
        isHTML: true,
      },
      (error, event) => {
        Alert.alert(
          error,
          event,
          [
            {
              text: 'Ok',
              onPress: () => console.log('OK: Email Error Response'),
            },
            {
              text: 'Cancel',
              onPress: () => console.log('CANCEL: Email Error Response'),
            },
          ],
          {cancelable: true},
        );
      },
    );
  };

  /************GET HEARTED PHOTO LOCATIONS*********** */

  async _getHeartedPhotoLocations(item) {
    console.log(item);
    await AsyncStorage.getItem('@userLoggedInId').then(value => {
      console.log(value);
      this.setState({
        userid: value,
      });
    });

    await axios
      .post(
        'http://3.135.63.132:3000/api/v1/tplp/cities/getHeartCityByUserId',
        {
          userid: this.state.userid,
          cityid: item._id,
        },
      )
      .then(responseJson => {
        console.log(responseJson);

        if (responseJson.data.data.length === 0) {
          this.setState({
            showPhotoitenaryPopup: false,
          });

          Actions.purchasedItenary({
            cityId: item._id,
            cityLocations: item.photoLocations,
            cityIcon: this.state.icon,
            cityName: item.cityname,
            cityWeather: this.state.icon,
            temperature: this.state.temperatureC,
            citylat: item.citylat,
            citylng: item.citylng,
            cityNewArr: [],
          });
        } else {
          const locationData = responseJson.data.data;
          for (var i = 0; i < item.photoLocations.length; i++) {
            for (var j = 0; j < locationData.length; j++) {
              if (
                item.photoLocations[i].id.toString() ===
                locationData[j].photo_location_id.toString()
              ) {
                item.photoLocations[i].hearted = 'true';
                this.setState({
                  cityLocations: item.photoLocations,
                });
              }
            }
          }

          console.log(this.state.cityLocations);
          var locationsArr = [];
          // for (var temp = 0; temp < this.state.cityLocations.length; temp++) {
          this.state.cityLocations.filter(city => {
            console.log(city.hearted);
            if (city.hearted === 'true') {
              locationsArr.push(city);
            }
          });

          this.setState({
            cityLocations: locationsArr,
          });
          console.log(this.state.cityLocations);
          this._checkLocations(this.state.cityLocations, item);
        }
      });
  }

  /********GET WEATHER BASED ON CITY LAT LONG********** */

  _getWeather = async (citylat, citylng) => {
    const lat = citylat;
    const long = citylng;
    const api_call = await fetch(
      'https://api.openweathermap.org/data/2.5/weather?lat=' +
        lat +
        '&lon=' +
        long +
        '&appid=' +
        this.state.api_key +
        '&units=metric',
    );
    const data = await api_call.json();
    this.setState({
      temperatureC: Math.round(data.main.temp),
      icon: 'https://openweathermap.org/img/w/' + data.weather[0].icon + '.png',
    });
  };

  /************SELECTED LOCATIONS***************** */

  async _checkLocations(Locations, item) {
    console.log('HIT LOCS');
    console.log(Locations);
    if (this.state.counter !== Locations.length - 1) {
      for (var j = 0; j < Locations.length; j++) {
        if (j !== Locations.length - 1) {
          if (Locations[this.state.counter] !== Locations[j]) {
            const api_call = await fetch(
              'https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=' +
                Locations[this.state.counter].lat +
                ',' +
                Locations[this.state.counter].long +
                '+&radius=3000&type=point_of_interest&keyword=' +
                Locations[j].name +
                '&key=AIzaSyAUG9ZACOjwqlMYmEqlkXPYMz1wZ14H8zs',
            );
            const locationres = await api_call.json();
            console.log(locationres);
            if (locationres.status === 'OK') {
              const dist = geolib.getDistance(
                {
                  latitude: Locations[this.state.counter].lat,
                  longitude: Locations[this.state.counter].long,
                },
                {latitude: Locations[j].lat, longitude: Locations[j].long},
              );
              Locations[j].distance = dist;
              Locations[j].hide = false;
              this.state.arr.push(Locations[j]);
            }
          }
        } else if (j === Locations.length - 1) {
          if (Locations[this.state.counter] !== Locations[j]) {
            const api_call = await fetch(
              'https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=' +
                Locations[this.state.counter].lat +
                ',' +
                Locations[this.state.counter].long +
                '+&radius=3000&type=point_of_interest&keyword=' +
                Locations[j].name +
                '&key=AIzaSyAUG9ZACOjwqlMYmEqlkXPYMz1wZ14H8zs',
            );
            const locationres = await api_call.json();
            console.log(locationres);
            if (locationres.status === 'OK') {
              const dist = geolib.getDistance(
                {
                  latitude: Locations[this.state.counter].lat,
                  longitude: Locations[this.state.counter].long,
                },
                {latitude: Locations[j].lat, longitude: Locations[j].long},
              );
              Locations[j].distance = dist;
              Locations[j].hide = false;
              this.state.arr.push(Locations[j]);
            } else {
            }
            this.state.counter += 1;
            this._checkLocations(Locations, item);
          }
        }
      }
    } else if (this.state.counter === Locations.length - 1) {
      // Declare a new array
      let newArray = [];

      // Declare an empty object
      let uniqueObject = {};

      console.log(this.state.arr);

      for (let i in this.state.arr) {
        // Extract the title
        var id = this.state.arr[i]['id'];
        // Use the title as the index
        uniqueObject[id] = this.state.arr[i];
      }

      // Loop to push unique object into array
      for (var i in uniqueObject) {
        newArray.push(uniqueObject[i]);
      }

      console.log(newArray);

      this.setState({
        showPhotoitenaryPopup: false,
      });

      // this.setState({showPhotoitenaryPopup: false});
      Actions.purchasedItenary({
        cityId: item._id,
        cityLocations: item.photoLocations,
        cityIcon: this.state.icon,
        cityName: item.cityname,
        cityWeather: this.state.icon,
        temperature: this.state.temperatureC,
        citylat: item.citylat,
        citylng: item.citylng,
        cityNewArr: newArray,
      });
    }
  }

  async _redirectToPurchasedItenaryPage(item) {
    this.setState({
      showPhotoitenaryPopup: true,
    });
    console.log(item);
    await this._getWeather(item.citylat, item.citylng);
    await this._getHeartedPhotoLocations(item);
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.header}>
          <Image
            style={styles.hdr_img}
            source={require('../../assets/LOGO_Blk_.png')}
          />
        </View>

        <ScrollView style={{width: '100%', backgroundColor: '#e9f0f6'}}>
          <View style={styles.bannerArea}>
            <View style={styles.createPic}>
              <View style={{marginRight: 20}}>
                <Image
                  style={styles.createPicImg}
                  source={require('../../assets/profile3.jpg')}
                />
                <Image
                  style={styles.flowerImg}
                  source={require('../../assets/pic3.png')}
                />
              </View>
              <View style={styles.proName}>
                <Text
                  numberOfLines={1}
                  style={{width: 150, fontSize: 20, fontFamily: 'BAHNSCHRIFT'}}>
                  Demo Account
                </Text>
                <Text
                  numberOfLines={1}
                  style={{
                    width: 150,
                    fontSize: 12,
                    letterSpacing: -0.7,
                    fontFamily: 'BAHNSCHRIFT',
                    color: '#777',
                  }}>
                  BAYANN | NY + NJ CREATOR
                </Text>
              </View>
            </View>
            <View style={styles.dateTravel}>
              <Text
                style={{
                  marginLeft: 5,
                  marginTop: 3,
                  fontFamily: 'BAHNSCHRIFT',
                  color: '#cf8cac',
                  fontSize: 25,
                }}>
                0
              </Text>
              <Text
                style={{
                  marginLeft: 5,
                  fontFamily: 'BAHNSCHRIFT',
                  color: '#cf8cac',
                  fontSize: 10,
                  borderWidth: 1,
                  borderColor: '#cf8cac',
                  paddingLeft: 10,
                  paddingRight: 7,
                  borderRadius: 30,
                  paddingTop: 2,
                }}>
                Followers
              </Text>
            </View>
          </View>

          {/* ColThree */}
          <View style={styles.colThree}>
            <TouchableOpacity
              style={styles.colCircle}
              onPress={() => this.goToInsta()}>
              <Text style={styles.circleTitle}>TPLP</Text>
              <Text style={styles.circleDes}>blog</Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.colCircle}
              onPress={() => this.goToInsta()}>
              <Text style={styles.circleTitle}>SHOP</Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.colCircle}
              onPress={() => this.goToInsta()}>
              <Text style={styles.circleTitle}>GRAM</Text>
              <Text style={(styles.circleDes, styles.circleDes2)}>on the</Text>
            </TouchableOpacity>
          </View>
          {/* Accordion End */}

          {/* Accordion */}
          <View>
            <Collapse>
              <CollapseHeader>
                <View style={styles.accordionHeader}>
                  <Text style={styles.headerText}>MY TRIPS</Text>
                </View>
              </CollapseHeader>

              <CollapseBody>
                <View style={styles.content}>
                  <View style={styles.contentArea}>
                    <View
                      style={{
                        justifyContent: 'center',
                        position: 'relative',
                        flexDirection: 'row',
                        marginTop: 20,
                        backgroundColor: '#fff',
                      }}>
                      {this.state.myTripsArr.length > 0 ? (
                        <FlatList
                          data={this.state.myTripsArr}
                          renderItem={({item}) => (
                            <View style={styles.flexGrid}>
                              <TouchableOpacity
                                style={styles.tripstyle}
                                onPress={() =>
                                  this._redirectToPurchasedItenaryPage(item)
                                }>
                                <Image
                                  style={styles.gridImg}
                                  source={{uri: item.cityimage}}
                                />
                                <Text style={styles.gridTitle}>
                                  {item.cityname}
                                </Text>
                              </TouchableOpacity>
                            </View>
                          )}
                          //Setting the number of column
                          numColumns={3}
                          keyExtractor={(item, index) => index.toString()}
                        />
                      ) : (
                        <View style={styles.fullWidth}>
                          <Text style={styles.notFound}>
                            No Trips Added Yet !
                          </Text>
                        </View>
                      )}
                    </View>
                  </View>
                </View>
              </CollapseBody>
            </Collapse>
            {/* <Accordion
              sections={SECTIONS}
              activeSections={this.state.activeSections}
              renderHeader={this._renderHeader}
              renderContent={this._renderContent}
              onChange={this._updateSections}
            /> */}

            {/* /*******************SECOND**************** */}
            <Collapse>
              <CollapseHeader>
                <View style={styles.accordionHeader}>
                  <Text style={styles.headerText}>OUTFIT PLANNER</Text>
                </View>
              </CollapseHeader>

              <CollapseBody>
                <View style={styles.content}>
                  <View style={styles.contentArea}>
                    {this.state.hideCities === false ? (
                      <View
                        style={{
                          justifyContent: 'center',
                          position: 'relative',
                          flexDirection: 'row',
                          marginTop: 20,
                          backgroundColor: '#fff',
                        }}>
                        {this.state.myOutfitPlannerArr.length > 0 ? (
                          <FlatList
                            data={this.state.myOutfitPlannerArr}
                            renderItem={({item}) => (
                              <View style={styles.flexGrid}>
                                <TouchableOpacity
                                  style={styles.nocss}
                                  onPress={() =>
                                    this._showSelectedLocations(item)
                                  }>
                                  <Image
                                    style={styles.gridImg}
                                    source={{uri: item.cityimage}}
                                  />
                                  <Text style={styles.gridTitle}>
                                    {item.cityname}
                                  </Text>
                                </TouchableOpacity>
                              </View>
                            )}
                            //Setting the number of column
                            numColumns={3}
                            keyExtractor={(item, index) => index.toString()}
                          />
                        ) : (
                          <View style={styles.fullWidth}>
                            <Text style={styles.notFound}>Nothing Found !</Text>
                          </View>
                        )}
                      </View>
                    ) : (
                      <View>
                        {this.state.locationsArr.length > 0 ? (
                          <View style={styles.outfitPlanner}>
                            <Text
                              style={{
                                fontSize: 14,
                                fontFamily: 'BAHNSCHRIFT',
                                marginTop: 16,
                                textTransform: 'uppercase',
                                fontWeight: 'bold',
                                color: '#cf8cac',
                                marginBottom: 10,
                                textAlign: 'center',
                                paddingRight: 20,
                              }}>
                              I don’t rely on mirrors so i always take selfies.
                            </Text>

                            <ScrollView
                              horizontal={true}
                              style={{width: '100%'}}>
                              <View
                                style={{
                                  justifyContent: 'center',
                                  position: 'relative',
                                  flexDirection: 'row',
                                  backgroundColor: '#fff',
                                }}>
                                {this.state.locationsArr.length > 0 ? (
                                  this._renderLocations()
                                ) : (
                                  <View style={styles.fullWidth}>
                                    <Text style={styles.notFound}>
                                      Nothing Found !
                                    </Text>
                                  </View>
                                )}
                              </View>
                            </ScrollView>
                          </View>
                        ) : (
                          <View style={styles.fullWidth}>
                            <Text style={styles.notFound}>Nothing Found !</Text>
                          </View>
                        )}
                      </View>
                    )}
                  </View>
                </View>
              </CollapseBody>
            </Collapse>
          </View>

          {/* *********************THIRD************************* */}

          <Collapse>
            <CollapseHeader>
              <View style={styles.accordionHeader}>
                <Text style={styles.headerText}>GALLERY</Text>
              </View>
            </CollapseHeader>

            {/* Gallery */}
            <CollapseBody>
              <View style={styles.contentArea}>
                <View
                  style={{
                    justifyContent: 'center',
                    position: 'relative',
                    flexDirection: 'row',
                    backgroundColor: '#fff',
                    marginTop: 20,
                  }}>
                  {this.state.photos.length > 0 ? (
                    <FlatList
                      data={this.state.photos}
                      renderItem={({item}) => (
                        <View style={styles.flexGrid}>
                          <Image style={styles.gridImg} source={{uri: item}} />
                        </View>
                      )}
                      //Setting the number of column
                      numColumns={3}
                      keyExtractor={(item, index) => index.toString()}
                    />
                  ) : (
                    <View style={styles.fullWidth}>
                      <Text style={styles.notFound}>No Photos Found !</Text>
                    </View>
                  )}
                </View>
              </View>
            </CollapseBody>
          </Collapse>
          {/* Gallery */}

          {/* <View style={styles.content}>
            <View style={styles.contentArea}>
              <View
                style={{
                  justifyContent: 'center',
                  position: 'relative',
                  flexDirection: 'row',
                  marginTop: 20,
                  backgroundColor: '#fff',
                }}>
                <View style={styles.flexGrid}>
                  <Image
                    style={styles.gridImg}
                    source={require('../../assets/Picture5.jpg')}
                  />
                  <Text style={styles.gridTitle}>BALI</Text>
                </View>
                <View style={styles.flexGrid}>
                  <Image
                    style={styles.gridImg}
                    source={require('../../assets/Picture4.jpg')}
                  />
                  <Text style={styles.gridTitle}>BARCELONA</Text>
                </View>
                <View style={styles.flexGrid}>
                  <Image
                    style={styles.gridImg}
                    source={require('../../assets/Picture4.jpg')}
                  />
                  <Text style={styles.gridTitle}>CHARLESTON</Text>
                </View>
              </View>
              <View
                style={{
                  justifyContent: 'center',
                  position: 'relative',
                  flexDirection: 'row',
                }}>
                <View style={styles.flexGrid}>
                  <Image
                    style={styles.gridImg}
                    source={require('../../assets/Picture6.jpg')}
                  />
                  <Text style={styles.gridTitle}>FLORENCE</Text>
                </View>
                <View style={styles.flexGrid}>
                  <Image
                    style={styles.gridImg}
                    source={require('../../assets/Picture4.jpg')}
                  />
                  <Text style={styles.gridTitle}>L.A</Text>
                </View>
                <View style={styles.flexGrid}>
                  <Image
                    style={styles.gridImg}
                    source={require('../../assets/Picture5.jpg')}
                  />
                  <Text style={styles.gridTitle}>L.A. WALLS</Text>
                </View>
              </View>
            </View>
          </View> */}

          {/* Accordion End */}

          {/* OUTFIT PLANNER */}
          {/* <View style={styles.outfitPlanner}>
            <Text
              style={{
                fontSize: 20,
                fontFamily: 'BAHNSCHRIFT',
                marginTop: 16,
                marginBottom: 10,
                textAlign: 'center',
                paddingRight: 20,
              }}>
              I don’t rely on mirrors so i always take selfies.
            </Text>

            <ScrollView horizontal={true} style={{width: '100%'}}>
              <View
                style={{
                  justifyContent: 'center',
                  position: 'relative',
                  flexDirection: 'row',
                  backgroundColor: '#fff',
                }}>
                <View style={styles.flexGridFrame}>
                  <Text style={styles.gridFrameTitle}>
                    The Piece Collective
                  </Text>
                  <View
                    style={{
                      justifyContent: 'center',
                      alignItems: 'center',
                      height: '100%',
                    }}>
                    <TouchableOpacity style={styles.addImg}>
                      <Icon
                        name="ios-add"
                        size={30}
                        type="ionicon"
                        color="#fff"
                      />
                    </TouchableOpacity>
                  </View>
                </View>
                <View style={styles.flexGridFrame}>
                  <Text style={styles.gridFrameTitle}>Venice Canals</Text>
                  <View
                    style={{
                      justifyContent: 'center',
                      alignItems: 'center',
                      height: '100%',
                    }}>
                    <TouchableOpacity style={styles.addImg}>
                      <Icon
                        name="ios-add"
                        size={30}
                        type="ionicon"
                        color="#fff"
                      />
                    </TouchableOpacity>
                  </View>
                </View>
                <View style={styles.flexGridFrame}>
                  <Text style={styles.gridFrameTitle}>Lifeguard Shacks</Text>
                  <View
                    style={{
                      justifyContent: 'center',
                      alignItems: 'center',
                      height: '100%',
                    }}>
                    <TouchableOpacity style={styles.addImg}>
                      <Icon
                        name="ios-add"
                        size={30}
                        type="ionicon"
                        color="#fff"
                      />
                    </TouchableOpacity>
                  </View>
                </View>
                <View style={styles.flexGridFrame}>
                  <Text style={styles.gridFrameTitle}>L.A. WALLS</Text>
                  <View
                    style={{
                      justifyContent: 'center',
                      alignItems: 'center',
                      height: '100%',
                    }}>
                    <TouchableOpacity style={styles.addImg}>
                      <Icon
                        name="ios-add"
                        size={30}
                        type="ionicon"
                        color="#fff"
                      />
                    </TouchableOpacity>
                  </View>
                </View>
                <View style={styles.flexGridFrame}>
                  <Text style={styles.gridFrameTitle}>Lorem Sports</Text>
                  <View
                    style={{
                      justifyContent: 'center',
                      alignItems: 'center',
                      height: '100%',
                    }}>
                    <TouchableOpacity style={styles.addImg}>
                      <Icon
                        name="ios-add"
                        size={30}
                        type="ionicon"
                        color="#fff"
                      />
                    </TouchableOpacity>
                  </View>
                </View>
                <View style={styles.flexGridFrame}>
                  <Text style={styles.gridFrameTitle}>Ipsum Clear</Text>
                  <View
                    style={{
                      justifyContent: 'center',
                      alignItems: 'center',
                      height: '100%',
                    }}>
                    <TouchableOpacity style={styles.addImg}>
                      <Icon
                        name="ios-add"
                        size={30}
                        type="ionicon"
                        color="#fff"
                      />
                    </TouchableOpacity>
                  </View>
                </View>
                <View style={styles.flexGridFrame}>
                  <Text style={styles.gridFrameTitle}>Colorful</Text>
                  <View
                    style={{
                      justifyContent: 'center',
                      alignItems: 'center',
                      height: '100%',
                    }}>
                    <TouchableOpacity style={styles.addImg}>
                      <Icon
                        name="ios-add"
                        size={30}
                        type="ionicon"
                        color="#fff"
                      />
                    </TouchableOpacity>
                  </View>
                </View>
              </View>
            </ScrollView>
          </View> */}
          {/* OUTFIT PLANNER END */}

          {/* What wanna here */}

          <Collapse>
            <CollapseHeader>
              <View style={styles.accordionHeader}>
                <Text style={styles.headerText}>WE WANNA HEAR FROM YOU</Text>
              </View>
            </CollapseHeader>

            <CollapseBody>
              <View style={styles.contentArea}>
                <Text
                  style={{
                    fontSize: 16,
                    fontFamily: 'GeorgiaPro-Bold',
                    marginTop: 20,
                    marginBottom: 10,
                  }}>
                  Chit Chat
                </Text>
                <View style={styles.hearForm}>
                  <Text style={styles.inputLabel}>Message:</Text>
                  <TextInput
                    ref="comment"
                    style={styles.textareaField}
                    onChangeText={comment =>
                      this.setState({
                        comment: comment,
                      })
                    }
                    value={this.state.comment}
                    placeholder="We're so happy you're here, please tell us what you think, need or want so we can make TPLP your absolute favorite app. xx Bay & Liv"
                    multiline
                  />
                </View>
                <View style={{alignItems: 'flex-end'}}>
                  <TouchableOpacity
                    style={styles.btnAddHotel}
                    onPress={this.handleEmail}>
                    <Icon
                      name="ios-checkmark"
                      size={30}
                      type="ionicon"
                      color="#fff"
                    />
                    <Text
                      style={{
                        marginLeft: 5,
                        color: '#fff',
                        fontFamily: 'BAHNSCHRIFT',
                        fontSize: 15,
                      }}>
                      Send with ♥
                    </Text>
                  </TouchableOpacity>
                </View>
              </View>
            </CollapseBody>
          </Collapse>

          <View style={{alignItems: 'center', justifyContent: 'center'}}>
            <Modal
              style={{
                alignItems: 'center',
                justifyContent: 'center',
                height: '100%',
              }}
              animationType="fade"
              transparent={true}
              visible={this.state.showPhotoitenaryPopup}
              onRequestClose={() => {
                Alert.alert('Modal has been closed.');
              }}>
              <ImageBackground
                source={require('../../assets/overlaybg.png')}
                style={{width: '100%', height: '100%'}}>
                <View
                  style={{
                    alignItems: 'center',
                    marginTop: 0,
                    height: 900,
                    paddingTop: 140,
                    width: '100%',
                  }}>
                  <View style={styles.scrollPricing} />
                  <View
                    style={{
                      width: '100%',
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}>
                    <View style={styles.loadingmain}>
                      <Text style={styles.loadingTxt}>
                        Loading your customized photo itinerary...
                      </Text>
                      <Image
                        style={styles.loadingImg}
                        source={require('../../assets/Blooming-Rose-GIF.gif')}
                      />
                    </View>
                  </View>
                </View>
              </ImageBackground>
            </Modal>
          </View>
          {/* end */}
        </ScrollView>

        <View style={styles.footMain}>
          <View style={styles.footFlex}>
            <TouchableOpacity onPress={() => Actions.exploreRegion()}>
              <Icon name="ios-planet" size={35} type="ionicon" color="#fff" />
            </TouchableOpacity>
          </View>

          <View style={styles.footFlex}>
            <TouchableOpacity onPress={() => Actions.Profile()}>
              <Icon size={30} name="ios-person" type="ionicon" color="#fff" />
            </TouchableOpacity>
          </View>

          <View style={styles.footFlex}>
            <TouchableOpacity
              style={styles.centerMenu}
              onPress={this._onSubmit.bind(this)}>
              <Image
                style={{
                  marginTop: 20,
                  width: 40,
                  height: 40,
                  resizeMode: 'contain',
                }}
                source={require('../../assets/heartlock.png')}
              />
            </TouchableOpacity>
          </View>

          <View style={styles.footFlex}>
            <TouchableOpacity onPress={() => Actions.home()}>
              <Icon size={27} name="ios-home" type="ionicon" color="#fff" />
            </TouchableOpacity>
          </View>

          <View style={styles.footFlex}>
            <TouchableOpacity onPress={() => Actions.Profile()}>
              <Icon size={27} name="ios-search" type="ionicon" color="#fff" />
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  btnAddHotel: {
    paddingVertical: 12,
    backgroundColor: '#cf8cac',
    textAlign: 'center',
    borderRadius: 30,
    fontSize: 14,
    marginBottom: 15,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    flex: 1,
    marginTop: 20,
    width: 140,
    maxHeight: 45,
  },
  hearForm: {
    backgroundColor: '#f0e0e2',
    padding: 20,
  },
  fieldRow: {
    marginBottom: 15,
  },
  inputLabel: {
    fontFamily: 'BAHNSCHRIFT',
    marginBottom: 3,
  },
  textareaField: {
    borderColor: '#ddd',
    borderWidth: 1,
    borderRadius: 3,
    paddingHorizontal: 10,
    height: 100,
    width: '100%',
    backgroundColor: '#fff',
    fontFamily: 'BAHNSCHRIFT',
  },
  textField: {
    borderColor: '#ddd',
    borderWidth: 1,
    borderRadius: 3,
    paddingHorizontal: 10,
    height: 40,
    width: '100%',
    backgroundColor: '#fff',
    fontFamily: 'BAHNSCHRIFT',
  },
  circleTitle: {
    fontSize: 27,
    fontFamily: 'GeorgiaPro-Bold',
    color: '#fff',
  },
  circleDes2: {
    fontSize: 40,
    fontFamily: 'Southampton',
    color: '#fff',
    position: 'absolute',
    top: -10,
  },
  circleDes: {
    fontSize: 40,
    fontFamily: 'Southampton',
    color: '#fff',
    position: 'absolute',
    bottom: -10,
  },
  colCircle: {
    flex: 1,
    backgroundColor: '#e8c8d7',
    borderColor: '#cf8cac',
    borderRadius: 150,
    borderWidth: 2,
    maxWidth: 100,
    marginHorizontal: 5,
    height: 100,
    alignItems: 'center',
    justifyContent: 'center',
    elevation: 1,
  },
  colThree: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: 20,
    marginBottom: 20,
  },
  playIcon: {
    position: 'absolute',
  },
  accordionHeader: {
    position: 'relative',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#cf8cac',
    width: '100%',
    paddingTop: 6,
    paddingBottom: 6,
    borderBottomWidth: 1,
    borderBottomColor: '#e9f0f6',
  },
  headerText: {
    fontFamily: 'GeorgiaPro-Bold',
    paddingVertical: 5,
    color: '#fff',
  },
  flowerImg2: {
    width: 60,
    height: 60,
    position: 'absolute',
    resizeMode: 'contain',
    left: -15,
    top: -15,
  },
  flowerImg: {
    width: 60,
    height: 60,
    position: 'absolute',
    resizeMode: 'contain',
    right: -15,
    bottom: -15,
  },
  eyeImg: {
    width: 20,
    resizeMode: 'contain',
  },
  fullWidth: {
    width: '100%',
    position: 'relative',
  },
  dateTravel: {
    alignItems: 'flex-end',
    minWidth: '30%',
    marginLeft: 'auto',
    flex: 1,
  },
  createPic: {
    marginRight: 50,
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
  },
  createPicImg: {
    width: 70,
    height: 70,
    resizeMode: 'cover',
    borderRadius: 100,
    borderColor: '#cf8cac',
    borderWidth: 3,
  },
  favorite: {
    position: 'absolute',
    right: 5,
    bottom: 5,
  },
  addImg: {
    position: 'absolute',
    margin: 'auto',
    bottom: 10,
    width: 30,
    alignItems: 'center',
    justifyContent: 'center',
    height: 30,
    borderRadius: 50,
    backgroundColor: 'rgba(206,57,128, .8)',
  },
  gridTitle: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 35,
    margin: 'auto',
    fontSize: 12,
    textAlign: 'center',
    color: '#cf8cac',
    paddingVertical: 5,
    backgroundColor: 'rgba(240,224,226, .8)',
    marginVertical: 10,
    fontFamily: 'GeorgiaPro-Bold',
  },
  gridFrameTitle: {
    position: 'absolute',
    left: 0,
    right: 0,
    textTransform: 'capitalize',
    margin: 'auto',
    fontSize: 12,
    textAlign: 'center',
    color: '#fff',
    paddingVertical: 5,
    paddingHorizontal: 5,
    backgroundColor: 'rgba(207,140,172, .8)',
    marginVertical: 10,
    fontFamily: 'GeorgiaPro-Bold',
  },
  gridImg: {
    width: '100%',
    height: 120,
    resizeMode: 'cover',
    // opacity: 0.5,
    borderRadius: 5,
    borderWidth: 1,
    borderColor: '#e9f0f6',
  },
  flexGrid: {
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 0,
    width: '33.3%',
    borderRadius: 5,
    backgroundColor: '#fff',
  },
  flexGridFrame: {
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 0,
    width: 104,
    borderRadius: 5,
    height: 150,
    borderColor: '#ddd',
    borderWidth: 1,
    marginRight: 5,
  },
  bannerArea: {
    alignItems: 'center',
    position: 'relative',
    marginBottom: 20,
    paddingTop: 15,
    paddingBottom: 20,
    paddingHorizontal: 20,
    flexDirection: 'row',
    backgroundColor: '#fff',
  },
  contentArea: {
    paddingHorizontal: 20,
    paddingBottom: 20,
    backgroundColor: '#fff',
  },
  outfitPlanner: {
    paddingLeft: 20,
    paddingBottom: 20,
    backgroundColor: '#fff',
  },
  footMain: {
    backgroundColor: '#cf8cac',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    paddingTop: 10,
    paddingBottom: 10,
    maxHeight: 55,
  },
  centerMenu: {
    backgroundColor: '#cf8cac',
    position: 'relative',
    top: -15,
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    borderTopLeftRadius: 50,
    borderTopRightRadius: 50,
  },
  footFlex: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-start',
    height: '100%',
    paddingTop: 0,
  },
  header: {
    position: 'relative',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    elevation: 1,
    backgroundColor: '#ffffff',
    width: '100%',
    paddingTop: 6,
    paddingBottom: 6,
  },
  hdr_img: {
    width: 200,
    height: 50,
    marginBottom: 5,
    resizeMode: 'stretch',
  },
  btnRemindme: {
    paddingVertical: 15,
    borderRadius: 30,
    marginTop: 25,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    flex: 1,
    borderWidth: 1,
    borderColor: '#fff',
    maxHeight: 40,
    minHeight: 40,
    marginHorizontal: 70,
  },
  tripstyle: {
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 0,
    width: '100%',
    borderRadius: 5,
    backgroundColor: '#fff',
  },

  loadingImg: {
    width: 250,
    height: 250,
    resizeMode: 'contain',
  },
  loadingTxt: {
    fontFamily: 'BAHNSCHRIFT',
    textTransform: 'uppercase',
    fontSize: 20,
    textAlign: 'center',
    color: '#cf8cac',
  },
  loadingmain: {
    backgroundColor: '#fff',
    justifyContent: 'center',
    alignItems: 'center',
    padding: 20,
    maxWidth: '90%',
    borderRadius: 10,
    top: 70,
    elevation: 1,
  },
  nocss: {
    width: '100%',
    resizeMode: 'cover',
  },
});
export default Profile;
