/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';

import ViewShot from 'react-native-view-shot';
import {Actions} from 'react-native-router-flux';
//Native
import {
  ActivityIndicator,
  Animated,
  Dimensions,
  Image,
  ImageBackground,
  PanResponder,
  Platform,
  StyleSheet,
  TouchableOpacity,
  Text,
  AsyncStorage,
  View,
} from 'react-native';

import {StickerPicker} from './srcstickers';
import {Icon} from 'react-native-elements';
//3rd party
type Props = {};
export default class App extends Component<Props> {
  constructor(props) {
    super(props);
    this.state = {
      pickerVisible: false,
      userimage:
        'https://img.favpng.com/8/21/25/upload-icon-multimedia-icon-arrows-icon-png-favpng-7sR65JmA1UYf3vbeUAiBALiRP_t.jpg',
    };
  }

  _onSubmit = () => {
    AsyncStorage.clear();
    Actions.Signin();
  };

  render() {
    const {style} = this.props;
    const {finalImage} = this.state;

    return (
      <View style={styles.container}>
        <View style={styles.header}>
          <Image
            style={styles.hdr_img}
            source={require('../../assets/pic.png')}
          />
          <View style={styles.logout}>
            <Icon
              name="ios-cloud-download"
              size={25}
              type="ionicon"
              color="#cf8cac"
            />
          </View>
        </View>
        {!finalImage && (
          <View>
            <TouchableOpacity
              onPress={() => this.setState({pickerVisible: true})}>
              <View style={styles.addImg}>
                <Image source={require('../../assets/upload.png')} />
                {/* <Text style={styles.addImg}>Add Stickers to Image</Text> */}
              </View>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => this._openImagePicker()}>
              <StickerPicker
                visible={this.state.pickerVisible}
                topContainer={
                  <View
                    style={{
                      paddingTop: 10,
                      textAlign: 'center',
                      alignItem: 'center',
                      justifyContent: 'center',
                    }}
                  />
                }
                bottomContainer={
                  // <Text style={styles.actionTitle}>Save Picture</Text>
                  <Icon
                    name="ios-cloud-download"
                    size={25}
                    type="ionicon"
                    color="#cf8cac"
                  />
                }
                bottomContainerStyle={styles.bottomContainerStyle}
                completedEditing={(imageUri, width, height) =>
                  this.setState({
                    pickerVisible: false,
                    finalImage: {imageUri, width, height},
                  })
                }
                includeDefaultStickers={true}
                imageStyle={null}
                previewImageSize={70}
                stickerSize={100}
                imageSource={this.state.userimage}
                stickers={null}
              />
            </TouchableOpacity>
          </View>
        )}
        {finalImage && (
          <View>
            <Image
              style={styles.finalAttachment}
              source={{uri: finalImage.imageUri}}
            />
            <TouchableOpacity
              onPress={() =>
                this.setState({pickerVisible: true, finalImage: false})
              }>
              <Text style={styles.actionTitle}>Upload Another Image</Text>
            </TouchableOpacity>
          </View>
        )}

        <View style={styles.footMain}>
          <View style={styles.footFlex}>
            <TouchableOpacity onPress={() => Actions.exploreRegion()}>
              <Icon name="ios-planet" size={35} type="ionicon" color="#fff" />
            </TouchableOpacity>
          </View>

          <View style={styles.footFlex}>
            <TouchableOpacity onPress={() => Actions.Profile()}>
              <Icon
                size={30}
                name="ios-person"
                type="ionicon"
                color="#fff"
              />
            </TouchableOpacity>
          </View>

          <View style={styles.footFlex}>
            <TouchableOpacity
              style={styles.centerMenu}
              onPress={this._onSubmit.bind(this)}>
              <Image
                style={{
                  marginTop: 20,
                  width: 40,
                  height: 40,
                  resizeMode: 'contain',
                }}
                source={require('../../assets/heartlock.png')}
              />
            </TouchableOpacity>
          </View>

          <View style={styles.footFlex}>
            <TouchableOpacity onPress={() => Actions.home()}>
              <Icon size={27} name="ios-home" type="ionicon" color="#fff" />
            </TouchableOpacity>
          </View>

          <View style={styles.footFlex}>
            <TouchableOpacity onPress={() => Actions.Profile()}>
              <Icon size={27} name="ios-search" type="ionicon" color="#fff" />
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }
}

let Window = Dimensions.get('window');
let CIRCLE_RADIUS = 45;
const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    justifyContent: 'flex-start',
    height: '100%',
    // paddingTop: 30,
    width: '100%',
  },
  bottomContainerStyle: {
    height: 80,
  },
  attachment: {
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf: 'stretch',
    height: 500,
    width: Window.width,
  },
  actionTitle: {
    color: 'blue',
    alignSelf: 'center',
    marginTop: 20,
    fontSize: 20,
  },
  stickerPickerContainer: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    width: Window.width,
    alignItems: 'center',
    justifyContent: 'center',
  },
  title: {
    width: Window.width,
    marginVertical: 5,
    fontSize: 20,
    textAlign: 'center',
  },
  appTitle: {
    width: Window.width,
    marginVertical: 25,
    fontSize: 25,
    textAlign: 'center',
  },
  finalAttachment: {
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf: 'stretch',
    height: 500,
    width: Window.width,
  },
  activeIndicator: {
    opacity: 1,
  },
  inactiveIndicator: {
    opacity: 0,
  },
  text: {
    fontSize: 15,
    alignSelf: 'center',
    marginLeft: 5,
    marginRight: 5,
    textAlign: 'center',
    color: '#fff',
  },
  actionTitle: {
    color: 'blue',
    alignSelf: 'center',
    marginTop: 20,
    fontSize: 20,
  },
  circle: {
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    width: CIRCLE_RADIUS * 2,
    height: CIRCLE_RADIUS * 2,
    padding: 5,
    margin: 2,
    borderRadius: CIRCLE_RADIUS,
  },
  logout: {
    position: 'absolute',
    right: 20,
    top: 20,
  },
  header: {
    position: 'relative',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    elevation: 1,
    backgroundColor: '#f0e0e2',
    width: '100%',
    paddingTop: 6,
    paddingBottom: 6,
  },
  hdr_img: {
    width: 200,
    height: 50,
    marginBottom: 5,
    resizeMode: 'stretch',
  },
  footMain: {
    backgroundColor: '#cf8cac',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    paddingTop: 10,
    paddingBottom: 10,
    maxHeight: 55,
  },
  footFlex: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  addImg: {
    height: 580,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
