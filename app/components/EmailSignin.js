/* eslint-disable react/no-string-refs */
/* eslint-disable react-native/no-inline-styles */
/* eslint-disable eqeqeq */
// eslint-disable-next-line no-unused-vars
import React, {Component} from 'react';
import {
  AsyncStorage,
  StyleSheet,
  Text,
  View,
  TextInput,
  Image,
  // eslint-disable-next-line no-unused-vars
  Alert,
  TouchableOpacity,
  TouchableHighlight,
  PermissionsAndroid,
  ScrollView,
  Switch,
} from 'react-native';
import {Icon} from 'react-native-elements';
import {Snackbar} from 'react-native-paper';
import Spinner from 'react-native-loading-spinner-overlay';
import {Actions} from 'react-native-router-flux';
import axios from 'axios';

class Register extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: '',
      error: '',
      spinner: false,
      loggedInUser: '',
      accessToken: null,
      visible: false,
      validation: '',
      color: 'black',
      locationStatus: null,
      rememberMe: false,
    };
    this.requestLocationPermission();
  }

  /*************CHECK FOR LOCATION PERMISSIONS*********** */
  async requestLocationPermission() {
    try {
      const granted = await PermissionsAndroid.check(
        PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
        {},
      );
      console.log('********Email Sign in page granted or not*******');
      console.log(granted);
      if (granted === true) {
        // Alert.alert('Location Permission Granted.');
        this.setState({locationStatus: 'Granted'});
      } else {
        // Alert.alert('Location Permission Not Granted');
        this.setState({locationStatus: 'Not Granted'});
      }
    } catch (err) {
      console.warn(err);
    }
  }

  /*************************LOGIN FORM SUBMISSION*********** */
  async _onSubmit() {
    var reg = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    if (
      (this.state.email == '' ||
        this.state.email == undefined ||
        this.state.email == null) &&
      (this.state.password == '' ||
        this.state.password == undefined ||
        this.state.password == null)
    ) {
      this.setState({
        visible: true,
        validation: '*Please enter email & password!',
      });
    } else if (
      this.state.email == '' ||
      this.state.email == undefined ||
      this.state.email == null
    ) {
      this.setState({visible: true, validation: '*Email Field is required!'});
    } else if (reg.test(this.state.email) == false) {
      this.setState({visible: true, validation: '*Email is not valid!'});
    } else if (
      this.state.password == '' ||
      this.state.password == undefined ||
      this.state.password == null
    ) {
      this.setState({
        visible: true,
        validation: '*Password field is required!',
      });
    } else {
      this.setState({spinner: true});

      const stremail =
        (await this.state.email.charAt(0).toLowerCase()) +
        this.state.email.slice(1);

      await this.setState({
        email: stremail,
      });

      await axios
        .post(
          'http://3.135.63.132:3000/api/v1/tplp/user_login',
          {
            email: this.state.email,
            password: this.state.password,
          },
          {
            headers: {
              'Content-Type': 'application/json',
              Accept: 'application/json',
            },
          },
        )
        .then(responseJson => {
          console.log('CHECK RESP');
          console.log(responseJson);

          if (JSON.stringify(responseJson.data.status) == 0) {
            if (this.state.rememberMe == true) {
              AsyncStorage.setItem(
                '@userRemember',
                JSON.stringify(responseJson.data.alldata._id),
              );
            }

            AsyncStorage.setItem(
              '@userLoggedInId',
              JSON.stringify(responseJson.data.alldata._id),
            );
            AsyncStorage.setItem(
              '@user_data',
              JSON.stringify(responseJson.data.alldata),
            );

            this.setState({
              visible: true,
              validation: 'LoggedIn Successfully!',
              color: '#228B22',
            });

            setTimeout(() => {
              this.setState({spinner: false});
              if (this.state.locationStatus == 'Not Granted') {
                Actions.enablelocation();
              } else if (this.state.locationStatus == 'Granted') {
                Actions.home();
              }
            }, 2000);
          } else if (JSON.stringify(responseJson.data.status) == 1) {
            this.setState({
              spinner: false,
              visible: true,
              validation: 'Your Email Or Password is Incorrect!',
              color: 'black',
            });
          } else if (JSON.stringify(responseJson.data.status) == 2) {
            this.setState({
              spinner: false,
              visible: true,
              validation: 'No User Exists with this email!',
              color: 'black',
            });
          }
        })
        .catch(error => {
          this.setState({spinner: false});
        });
    }
  }

  /************LOADER********* */

  renderLoader() {
    if (this.state.spinner) {
      return (
        <Spinner
          visible={this.state.spinner}
          size={'large'}
          textContent={'Loading...'}
          animation={'slide'}
          textStyle={styles.spinnerTextStyle}
        />
      );
    }
  }

  /********REMEMBER ME*********** */

  _toggleRememberMe = value => {
    this.setState({rememberMe: value});
  };

  /*****************RENDER VIEW***************** */
  render() {
    return (
      <ScrollView contentContainerStyle={styles.contentContainer}>
        {/*
                      <Toast
                      ref="toast"
                      style={{backgroundColor:'gray'}}
                      position='top'
                      positionValue={200}
                      fadeInDuration={750}
                      fadeOutDuration={1000}
                      opacity={0.8}
                      textStyle={{color:'white'}}
                      /> */}

        <View style={styles.container}>
          <Snackbar
            style={{backgroundColor: this.state.color}}
            visible={this.state.visible}
            onDismiss={() => this.setState({visible: false})}
            action={{
              // label: 'Undo',
              onPress: () => {
                // Do something
              },
            }}>
            {this.state.validation}
          </Snackbar>

          <View style={styles.header}>
            <Image
              style={styles.hdr_img}
              source={require('../../assets/LOGO_Blk_.png')}
            />
          </View>
          <Text style={{marginTop: 30}} />

          <View style={styles.SectionStyle}>
            <Icon color="#fff" name="email" />
            <TextInput
              ref="email"
              style={styles.input}
              onChangeText={email => this.setState({email})}
              value={this.state.email}
              placeholder="Enter Email"
              underlineColorAndroid="transparent"
              placeholderTextColor="#fff"
            />
          </View>

          <View style={styles.SectionStyle}>
            <Icon color="#fff" name="lock" />
            <TextInput
              secureTextEntry={true}
              ref="password"
              style={styles.input}
              onChangeText={password => this.setState({password})}
              value={this.state.password}
              placeholder="Enter Password"
              underlineColorAndroid="transparent"
              placeholderTextColor="#fff"
            />
          </View>

          <View style={styles.SectionStyleRemember}>
            <Switch
              trackColor={{false: '#767577', true: '#767577'}}
              thumbColor={this.state.rememberMe ? '#cf8cac' : '#f4f3f4'}
              value={this.state.rememberMe}
              onValueChange={value => this._toggleRememberMe(value)}
            />
            <Text style={{color: '#fff'}}>Remember Me</Text>
          </View>

          <View style={styles.Flexbox}>
            <TouchableOpacity onPress={this._onSubmit.bind(this)}>
              <Text style={styles.button}>LOGIN</Text>
            </TouchableOpacity>

            {this.renderLoader()}

            {/* <TouchableHighlight> */}
            <TouchableOpacity onPress={() => Actions.forgotpassword()}>
              <Text
                style={{
                  color: '#fff',
                  textDecorationLine: 'underline',
                  textAlign: 'right',
                  fontSize: 15,
                  marginTop: 5,
                }}>
                Forgot password?
              </Text>
            </TouchableOpacity>
            {/* </TouchableHighlight> */}
          </View>

          <View style={styles.boldLine} />

          <View>
            <TouchableHighlight>
              <TouchableOpacity onPress={() => Actions.Signin()}>
                <Text
                  style={{
                    color: '#fff',
                    textDecorationLine: 'underline',
                    textAlign: 'right',
                    fontSize: 15,
                    marginTop: 5,
                  }}>
                  Don't Have An Account? Signup Now
                </Text>
              </TouchableOpacity>
            </TouchableHighlight>
          </View>
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  contentContainer: {
    paddingBottom: 20,
    width: '100%',
    backgroundColor: '#cf8cac',
    height: '100%',
  },
  container: {
    backgroundColor: '#cf8cac',
    display: 'flex',
    height: '100%',
    width: '100%',
    paddingLeft: 20,
    paddingRight: 20,
    paddingTop: 110,
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  SectionStyle: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 1,
    borderColor: '#d3d3d3',
    height: 50,
    marginTop: 10,
    width: '100%',
    borderRadius: 50,
    paddingLeft: 25,
  },
  SectionStyleRemember: {
    flexDirection: 'row',
    alignItems: 'center',
    height: 50,
    marginTop: 10,
    width: '100%',
  },
  Flexbox: {
    marginTop: 30,
    width: '100%',
  },
  button: {
    backgroundColor: '#fff',
    height: 50,
    flexDirection: 'row',
    fontSize: 16,
    color: '#cf8cac',
    textAlign: 'center',
    textTransform: 'uppercase',
    width: '100%',
    borderRadius: 50,
    paddingTop: 14,
  },
  input: {
    color: '#d3d3d3',
    width: '100%',
    paddingLeft: 5,
    fontSize: 14,
  },
  CheckBoxRow: {
    flexDirection: 'row',
    width: '100%',
    height: 30,
    marginTop: 10,
  },
  boldLine: {
    flexDirection: 'row',
    height: 6,
    width: 150,
    backgroundColor: '#d3d3d3',
    marginTop: 40,
    borderRadius: 5,
  },
  header: {
    position: 'relative',
    display: 'flex',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
  hdr_img: {
    width: 320,
    height: 91,
    marginBottom: 20,
    resizeMode: 'contain',
  },
});
export default Register;
