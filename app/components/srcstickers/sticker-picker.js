import React, {Component} from 'react';
import ViewShot from 'react-native-view-shot';

//Native
import {
  Animated,
  Dimensions,
  Image,
  ImageBackground,
  Modal,
  PanResponder,
  ScrollView,
  StyleSheet,
  TouchableOpacity,
  TouchableHighlight,
  Text,
  View,
  AsyncStorage,
} from 'react-native';
import CameraRoll from '@react-native-community/cameraroll';
import {Actions} from 'react-native-router-flux';
import {Icon} from 'react-native-elements';
import ImagePicker from 'react-native-image-picker';
import {Col, Row, Grid} from 'react-native-easy-grid';
import axios from 'axios';

const ghost = require('../../../assets/sticker1.png');
const heart = require('../../../assets/sticker2.png');
const heartEyes = require('../../../assets/sticker3.png');
const kiss = require('../../../assets/sticker4.png');
const party = require('../../../assets/sticker5.png');
const robot = require('../../../assets/sticker6.png');
const smile = require('../../../assets/sticker7.png');
const sunglasses = require('../../../assets/sticker8.png');
// const thumbsup = require('../../../assets/sticker9.png');

type Props = {};
export default class StickerPicker extends Component<Props> {
  constructor(props) {
    super(props);
    this._getStickers();
    this.state = {
      userid: '',
      defaultStickers: [],
      pan: new Animated.ValueXY(),
      showSticker: false,
      imageSource:
        'https://img.favpng.com/8/21/25/upload-icon-multimedia-icon-arrows-icon-png-favpng-7sR65JmA1UYf3vbeUAiBALiRP_t.jpg',
    };

    this.panResponder = PanResponder.create({
      onStartShouldSetPanResponder: () => true,
      onPanResponderMove: Animated.event([
        null,
        {
          dx: this.state.pan.x,
          dy: this.state.pan.y,
        },
      ]),
      onPanResponderRelease: (e, gesture) => {
        this.state.pan.setOffset({
          x: this.currentPanValue.x,
          y: this.currentPanValue.y,
        });
        this.state.pan.setValue({
          x: 0,
          y: 0,
        });
      },
    });
  }

  /*********GET USER LOGGED IN ID************ */

  async componentDidMount() {
    this._isMounted = true;

    if (this._isMounted) {
      this.currentPanValue = {x: 0, y: 0};
      this.panListener = this.state.pan.addListener(
        value => (this.currentPanValue = value),
      );
    }
    console.log('component DID mount');
    await AsyncStorage.getItem('@userLoggedInId').then(value => {
      console.log(value);
      this.setState({
        userid: value,
      });
    });
  }

  _onSubmit = () => {
    AsyncStorage.clear();
    Actions.Signin();
  };

  _getStickers() {
    fetch('http://3.135.63.132:3000/api/v1/tplp/sticker/getStickersList')
      .then(response => response.json())
      .then(json => {
        console.log(json);
        for (var i = 0; i < json.list.length; i++) {
          console.log(json.list[i].sticker);
          json.list[i].sticker =
            'http://3.135.63.132:3000/images/' + json.list[i].sticker;
          var arr = [];
          arr = [
            this.previewStickerImage(json.list[i].sticker),
            json.list[i].sticker,
          ];
          this.state.defaultStickers.push(arr);
        }
      });
  }



  componentWillUnmount() {
    if (this._isMounted) {
      this.state.pan.removeListener(this.panListener);
    }

    this._isMounted = false;
  }

  takeViewShot() {
    if (this._isMounted) {
      this.viewShot.capture().then(uri => {
        Image.getSize(uri, (width, height) => {
          this.props.completedEditing(uri, width, height);
          this.setState({
            finalImage: {
              uri: uri,
              width: width,
              height: height,
            },
          });
          console.log(this.state);
          CameraRoll.saveToCameraRoll(uri);

          axios
            .post('http://3.135.63.132:3000/api/v1/tplp/addPhotos', {
              userid: this.state.userid,
              photos: uri,
            })
            .then(responseJson => {
              console.log(responseJson);
            })
            .catch(error => {});
        });
      });
    }
  }

  resetViewShot() {
    if (this._isMounted) {
      this.setState({
        finalImage: null,
        pan: new Animated.ValueXY(),
        showSticker: false,
      });
    }
  }

  previewStickerImage(imageUrl, size = 15) {
    if (this._isMounted) {
      return (
        <Image
          style={{
            width: this.props.previewImageSize,
            height: this.props.previewImageSize,
            margin: 5,
          }}
          source={{uri: imageUrl}}
        />
      );
    }
  }

  _openImagePicker() {
    // console.warn('123');
    const options = {
      title: 'Select Avatar',
      storageOptions: {
        skipBackup: true,
        path: 'images',
        mediaType: 'video',
      },
    };
    ImagePicker.showImagePicker(options, response => {
      console.log('------------camera response---------------');
      console.log(response);
      if (response.didCancel) {
        console.log('User cancelled image picker');
        this.setState({
          imageSource:
            'https://img.favpng.com/8/21/25/upload-icon-multimedia-icon-arrows-icon-png-favpng-7sR65JmA1UYf3vbeUAiBALiRP_t.jpg',
        });
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
        this.setState({
          imageSource:
            'https://img.favpng.com/8/21/25/upload-icon-multimedia-icon-arrows-icon-png-favpng-7sR65JmA1UYf3vbeUAiBALiRP_t.jpg',
        });
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      } else {
        this.setState({
          imageSource: response.uri,
        });
      }
    });
  }

  render() {
    const {
      bottomContainer,
      bottomContainerStyle,
      imageSource,
      includeDefaultStickers,
      stickers,
      style,
      topContainer,
      visible,
    } = this.props;
    const {finalImage, showSticker, sticker} = this.state;
    const finalImageUrl = finalImage ? finalImage.uri : null;
    // const defaultStickers = [
    //   [this.previewStickerImage(ghost), ghost],
    //   [this.previewStickerImage(heart), heart],
    //   [this.previewStickerImage(heartEyes), heartEyes],
    //   [this.previewStickerImage(kiss), kiss],
    //   [this.previewStickerImage(party), party],
    //   [this.previewStickerImage(robot), robot],
    //   [this.previewStickerImage(smile), smile],
    //   [this.previewStickerImage(sunglasses), sunglasses],
    //   // [this.previewStickerImage(thumbsup), thumbsup],
    // ];
    console.log(this.state.defaultStickers);

    let finalStickers;
    console.log(this.state.defaultStickers);
    if (!stickers) {
      finalStickers = this.state.defaultStickers;
    } else if (includeDefaultStickers) {
      finalStickers = stickers.concat(this.state.defaultStickers);
    } else {
      finalStickers = stickers;
    }

    if (!this._isMounted) {
      return <View />;
    }

    return (
      <Modal visible={visible}>
        <View style={styles.header}>
          <Image
            style={styles.hdr_img}
            source={require('../../../assets/pic.png')}
          />
          <View style={styles.logout}>
            <TouchableOpacity
              style={this.props.bottomContainerStyle}
              onPress={() => this.takeViewShot()}>
              {bottomContainer}
            </TouchableOpacity>
          </View>
        </View>
        <ScrollView>
          <View>{topContainer}</View>
          <View>
            {!finalImage && (
              <View>
                <ViewShot
                  style={{flex: null}}
                  ref={ref => {
                    this.viewShot = ref;
                  }}
                  options={{format: 'jpg', quality: 1.0}}>
                  <TouchableOpacity onPress={() => this._openImagePicker()}>
                    <View>
                      <ImageBackground
                        ref={image => (this.imageComponent = image)}
                        source={{uri: this.state.imageSource}}
                        style={[styles.attachment, this.props.imageStyle]}>
                        {showSticker && this._isMounted && (
                          <Animated.View
                            {...this.panResponder.panHandlers}
                            style={[this.state.pan.getLayout()]}>
                            <Image
                              style={{
                                width: this.props.stickerSize,
                                height: this.props.stickerSize,
                              }}
                              source={{uri: sticker}}
                            />
                            {/*
                            <Image
                              style={{
                                width: this.props.stickerSize,
                                height: this.props.stickerSize,
                              }}
                              source={sticker}
                            /> */}
                          </Animated.View>
                        )}
                      </ImageBackground>
                    </View>
                  </TouchableOpacity>
                </ViewShot>
                {/* <Text style={styles.title}>Select a sticker</Text> */}

                <View
                  style={{
                    position: 'relative',
                    backgroundColor: '#e7c7d6',
                    paddingBottom: 10,
                    // paddingLeft: 20,
                  }}>
                  <View
                    style={{
                      width: '100%',
                      flexDirection: 'row',
                      alignItems: 'center',
                      paddingRight: 20,
                      justifyContent: 'center',
                      marginBottom: 20,
                    }}>
                    <View>
                      <Text style={styles.temTitle2}>Stickers</Text>
                    </View>
                    <View style={{marginLeft: 'auto'}}>
                      <Icon
                        name="angle-down"
                        size={25}
                        type="font-awesome"
                        color="#cf8cac"
                      />
                    </View>
                  </View>
                  <ScrollView
                    overScrollMode={'always'}
                    horizontal={true}
                    contentContainerStyle={[
                      styles.stickerPickerContainer,
                      this.props.bottomContainerStyle,
                    ]}>
                    {finalStickers.map((sticker, index) => {
                      console.log(sticker);
                      return (
                        <ScrollView>
                          <TouchableOpacity
                            key={index}
                            onPress={() =>
                              this._isMounted &&
                              this.setState({
                                showSticker: true,
                                sticker: sticker[1],
                              })
                            }>
                            {sticker[0]}
                          </TouchableOpacity>
                        </ScrollView>
                      );
                    })}
                  </ScrollView>
                </View>
              </View>
            )}
          </View>
        </ScrollView>
        <View style={styles.footMain}>
          <View style={styles.footFlex}>
            <TouchableOpacity onPress={() => Actions.exploreHome()}>
              <Icon name="ios-planet" size={35} type="ionicon" color="#fff" />
            </TouchableOpacity>
          </View>

          <View style={styles.footFlex}>
            <TouchableOpacity onPress={() => Actions.Profile()}>
              <Icon size={30} name="ios-person" type="ionicon" color="#fff" />
            </TouchableOpacity>
          </View>

          <View style={styles.footFlex}>
            <TouchableOpacity
              style={styles.centerMenu}
              onPress={this._onSubmit.bind(this)}>
              <Image
                style={{
                  marginTop: 20,
                  width: 40,
                  height: 40,
                  resizeMode: 'contain',
                }}
                source={require('../../../assets/heartlock.png')}
              />
            </TouchableOpacity>
          </View>

          <View style={styles.footFlex}>
            <TouchableOpacity onPress={() => Actions.home()}>
              <Icon size={27} name="ios-home" type="ionicon" color="#fff" />
            </TouchableOpacity>
          </View>

          <View style={styles.footFlex}>
            <TouchableOpacity onPress={() => Actions.Profile()}>
              <Icon size={27} name="ios-search" type="ionicon" color="#fff" />
            </TouchableOpacity>
          </View>
        </View>
      </Modal>
    );
  }
}

let {height, width} = Dimensions.get('window');
const styles = StyleSheet.create({
  stickerPickerContainer: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    width: width,
    alignItems: 'center',
    justifyContent: 'center',
    minHeight: 150,
  },
  actionTitle: {
    color: 'blue',
    alignSelf: 'center',
    marginTop: 20,
    fontSize: 20,
    alignItems: 'center',
    justifyContent: 'center',
  },
  attachment: {
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf: 'stretch',
    height: 500,
    width: width,
  },
  finalAttachment: {
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf: 'stretch',
    height: 500,
    width: width,
  },
  text: {
    fontSize: 15,
    alignSelf: 'center',
    marginLeft: 5,
    marginRight: 5,
    textAlign: 'center',
    color: '#fff',
  },
  title: {
    width: width,
    marginVertical: 5,
    fontSize: 20,
    textAlign: 'center',
  },
  gridImg: {
    height: 50,
    resizeMode: 'contain',
  },
  gridCol: {
    maxWidth: '25%',
    minWidth: '25%',
    alignItems: 'center',
    justifyContent: 'center',
    marginVertical: 5,
  },
  flexGrid: {
    alignItems: 'center',
    justifyContent: 'center',
    width: '100%',
  },
  footMain: {
    backgroundColor: '#cf8cac',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    paddingTop: 10,
    paddingBottom: 10,
    maxHeight: 55,
  },
  footFlex: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  logout: {
    position: 'absolute',
    right: 20,
    top: 20,
  },
  header: {
    position: 'relative',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    elevation: 1,
    backgroundColor: '#f0e0e2',
    width: '100%',
    paddingTop: 6,
    paddingBottom: 6,
  },
  hdr_img: {
    width: 200,
    height: 50,
    marginBottom: 5,
    resizeMode: 'stretch',
  },
  temTitle2: {
    fontSize: 16,
    textAlign: 'center',
    justifyContent: 'center',
    alignItems: 'center',
    color: '#cf8cac',
    fontFamily: 'GeorgiaPro-Bold',
  },
  temTitle: {
    fontSize: 30,
    textAlign: 'center',
    color: '#cf8cac',
    paddingVertical: 5,
    backgroundColor: '#f0e0e2',
    marginVertical: 10,
    width: '100%',
    fontFamily: 'BAHNSCHRIFT',
    textTransform: 'uppercase',
  },
});
