/* eslint-disable no-unused-vars */
/* eslint-disable react-native/no-inline-styles */
'use strict';
import React from 'react';
import {
  StyleSheet,
  Text,
  Modal,
  View,
  Image,
  Alert,
  TouchableOpacity,
  TouchableHighlight,
  ScrollView,
  Linking,
  Platform,
  AsyncStorage,
  Dimensions,
  ImageBackground,
} from 'react-native';
import {Icon} from 'react-native-elements';
import {Actions} from 'react-native-router-flux';
// import ImagePicker from 'react-native-image-picker';
import Share from 'react-native-share';
import RNInstagramStoryShare from 'react-native-instagram-share';
import CameraRoll from '@react-native-community/cameraroll';
import RNFetchBlob from 'rn-fetch-blob';
import axios from 'axios';
import {Col, Row, Grid} from 'react-native-easy-grid';
import ViewShot from 'react-native-view-shot';
import ImageZoom from 'react-native-image-pan-zoom';
import {Snackbar} from 'react-native-paper';
import Video from 'react-native-video';
import ImagePicker from 'react-native-image-crop-picker';
import Swiper from 'react-native-swiper';
import {
  Collapse,
  CollapseHeader,
  CollapseBody,
  AccordionList,
} from 'accordion-collapse-react-native';

export default class templates extends React.Component {
  constructor(props) {
    super(props);
    console.log(this.props);

    this.state = {
      imageuri1: null,
      imageuri1type: '',
      imageuri2: null,
      imageuri2type: '',
      imageuri3: null,
      imageuri3type: '',
      imageuri4: null,
      imageuri4type: '',
      visible: false,
      type: '',
      imageCount: '',
      userid: '',
      modalVisible: false,
      modalVisible2: false,
      collapsed: true, // show the body by default
    };
  }

  async componentDidMount() {
    console.log('component DID mount');
    await AsyncStorage.getItem('@userLoggedInId').then(value => {
      console.log(value);
      this.setState({
        userid: value,
      });
    });
  }

  _onSubmit = () => {
    AsyncStorage.clear();
    Actions.Signin();
  };

  takeViewShot() {
    this.viewShot.capture().then(uri => {
      Image.getSize(uri, (width, height) => {
        console.log(this.state);
        CameraRoll.saveToCameraRoll(uri);
        this.setState({
          visible: true,
        });

        axios
          .post('http://3.135.63.132:3000/api/v1/tplp/addPhotos', {
            userid: this.state.userid,
            photos: uri,
          })
          .then(responseJson => {
            console.log(responseJson);
          })
          .catch(error => {});
      });
    });
  }

  chooseMethod(imageCount) {
    this.setState({
      imageCount: imageCount,
      modalVisible: true,
    });
    console.log(this.state);
  }

  takePicture() {
    this.setState({
      type: 'image',
      modalVisible: false,
    });
    try {
      ImagePicker.openPicker({
        width: 300,
        height: 400,
        cropping: true,
      }).then(image => {
        console.log(image);
        if (this.state.imageCount === 'img1') {
          this.setState({
            imageuri1: image.path,
            imageuri1type: this.state.type,
          });
          this._renderCameraImage1();
        } else if (this.state.imageCount === 'img2') {
          this.setState({
            imageuri2: image.path,
            imageuri2type: this.state.type,
          });
          this._renderCameraImage2();
        } else if (this.state.imageCount === 'img3') {
          this.setState({
            imageuri3: image.path,
            imageuri3type: this.state.type,
          });
          this._renderCameraImage3();
        } else if (this.state.imageCount === 'img4') {
          this.setState({
            imageuri4: image.path,
            imageuri4type: this.state.type,
          });
          this._renderCameraImage4();
        }
      });
    } catch (e) {
      Alert.alert('Server Error! Try Again!');
    }
  }

  takeVideo() {
    try {
      ImagePicker.openPicker({
        mediaType: 'video',
      }).then(video => {
        console.log(video);
        this.setState({
          type: 'video',
          modalVisible: false,
        });
        console.log(this.state);
        // console.log(this.state);

        if (this.state.imageCount === 'img1') {
          this.setState({
            imageuri1: video.path,
            imageuri1type: this.state.type,
          });
          this._renderCameraImage1();
        } else if (this.state.imageCount === 'img2') {
          this.setState({
            imageuri2: video.path,
            imageuri2type: this.state.type,
          });
          this._renderCameraImage2();
        } else if (this.state.imageCount === 'img3') {
          this.setState({
            imageuri3: video.path,
            imageuri3type: this.state.type,
          });
          this._renderCameraImage3();
        } else if (this.state.imageCount === 'img4') {
          this.setState({
            imageuri4: video.path,
            imageuri4type: this.state.type,
          });
          this._renderCameraImage4();
        }
      });
    } catch (e) {
      Alert.alert('Server Error! Try Again!');
    }
  }

  _getTemplateIndex(index) {
    if (index > 3) {
      this.setState({
        modalVisible2: true,
      });
    }
  }

  setModalVisible2() {
    this.setState({
      modalVisible2: false,
    });
  }

  _renderCameraImage1() {
    if (this.state.imageuri1 == null) {
      return (
        <TouchableOpacity onPress={() => this.chooseMethod('img1')}>
          <View style={styles.icon}>
            <Icon
              name="ios-add-circle"
              size={50}
              type="ionicon"
              color="#cf8cac"
            />
          </View>
        </TouchableOpacity>
      );
    } else {
      return (
        <View style={styles.containervid}>
          {this.state.imageuri1type === 'image' ? (
            <ImageZoom
              cropWidth={160}
              cropHeight={170}
              imageWidth={300}
              imageHeight={170}>
              {/* <Image style={styles.layimg} source={{uri: this.state.imageuri1}} /> */}
              <Image
                style={{width: '100%', height: 170, resizeMode: 'contain'}}
                source={{uri: this.state.imageuri1}}
              />
            </ImageZoom>
          ) : (
            <ImageZoom
              cropWidth={160}
              cropHeight={170}
              imageWidth={300}
              imageHeight={170}>
              <Video
                source={{
                  uri: this.state.imageuri1,
                }} // Can be a URL or a local file.
                ref={ref => {
                  this.player = ref;
                }} // Store reference
                onBuffer={() => {
                  // console.log('buffer', true);
                }}
                onEnd={() => {
                  // console.log('end', true);
                }}
                onError={() => {
                  // console.log('error', true);
                }}
                onLoad={() => {
                  // console.log('load', true);
                }}
                onProgress={() => {
                  // console.log('progress', true);
                }}
                resizeMode={'contain'}
                style={styles.backgroundVideo}
              />
            </ImageZoom>
          )}
        </View>
      );
    }
  }

  _renderCameraImage2() {
    if (this.state.imageuri2 == null) {
      return (
        <TouchableOpacity onPress={() => this.chooseMethod('img2')}>
          <View style={styles.icon}>
            <Icon
              name="ios-add-circle"
              size={50}
              type="ionicon"
              color="#cf8cac"
            />
          </View>
        </TouchableOpacity>
      );
    } else {
      return (
        <View style={styles.containervid}>
          {this.state.imageuri2type === 'image' ? (
            <ImageZoom
              cropWidth={160}
              cropHeight={170}
              imageWidth={300}
              imageHeight={170}>
              {/* <Image style={styles.layimg} source={{uri: this.state.imageuri1}} /> */}
              <Image
                style={{width: '100%', height: 170, resizeMode: 'contain'}}
                source={{uri: this.state.imageuri2}}
              />
            </ImageZoom>
          ) : (
            <ImageZoom
              cropWidth={160}
              cropHeight={170}
              imageWidth={300}
              imageHeight={170}>
              <Video
                source={{
                  uri: this.state.imageuri2,
                }} // Can be a URL or a local file.
                ref={ref => {
                  this.player = ref;
                }} // Store reference
                onBuffer={() => {
                  // console.log('buffer', true);
                }}
                onEnd={() => {
                  // console.log('end', true);
                }}
                onError={() => {
                  // console.log('error', true);
                }}
                onLoad={() => {
                  // console.log('load', true);
                }}
                onProgress={() => {
                  // console.log('progress', true);
                }}
                resizeMode={'contain'}
                style={styles.backgroundVideo}
              />
            </ImageZoom>
          )}
        </View>
      );
    }
  }

  _renderCameraImage3() {
    if (this.state.imageuri3 == null) {
      return (
        <TouchableOpacity onPress={() => this.chooseMethod('img3')}>
          <View style={styles.icon}>
            <Icon
              name="ios-add-circle"
              size={50}
              type="ionicon"
              color="#cf8cac"
            />
          </View>
        </TouchableOpacity>
      );
    } else {
      return (
        <View style={styles.containervid}>
          {this.state.imageuri3type === 'image' ? (
            <ImageZoom
              cropWidth={160}
              cropHeight={170}
              imageWidth={300}
              imageHeight={170}>
              {/* <Image style={styles.layimg} source={{uri: this.state.imageuri1}} /> */}
              <Image
                style={{width: '100%', height: 170, resizeMode: 'contain'}}
                source={{uri: this.state.imageuri3}}
              />
            </ImageZoom>
          ) : (
            <ImageZoom
              cropWidth={160}
              cropHeight={170}
              imageWidth={300}
              imageHeight={170}>
              <Video
                source={{
                  uri: this.state.imageuri3,
                }} // Can be a URL or a local file.
                ref={ref => {
                  this.player = ref;
                }} // Store reference
                onBuffer={() => {
                  // console.log('buffer', true);
                }}
                onEnd={() => {
                  // console.log('end', true);
                }}
                onError={() => {
                  // console.log('error', true);
                }}
                onLoad={() => {
                  // console.log('load', true);
                }}
                onProgress={() => {
                  // console.log('progress', true);
                }}
                resizeMode={'contain'}
                style={styles.backgroundVideo}
              />
            </ImageZoom>
          )}
        </View>
      );
    }
  }

  _renderCameraImage4() {
    if (this.state.imageuri4 == null) {
      return (
        <TouchableOpacity onPress={() => this.chooseMethod('img4')}>
          <View style={styles.icon}>
            <Icon
              name="ios-add-circle"
              size={50}
              type="ionicon"
              color="#cf8cac"
            />
          </View>
        </TouchableOpacity>
      );
    } else {
      return (
        <View style={styles.containervid}>
          {this.state.imageuri4type === 'image' ? (
            <ImageZoom
              cropWidth={160}
              cropHeight={170}
              imageWidth={300}
              imageHeight={170}>
              {/* <Image style={styles.layimg} source={{uri: this.state.imageuri1}} /> */}
              <Image
                style={{width: '100%', height: 170, resizeMode: 'contain'}}
                source={{uri: this.state.imageuri4}}
              />
            </ImageZoom>
          ) : (
            <ImageZoom
              cropWidth={160}
              cropHeight={170}
              imageWidth={300}
              imageHeight={170}>
              <Video
                source={{
                  uri: this.state.imageuri4,
                }} // Can be a URL or a local file.
                ref={ref => {
                  this.player = ref;
                }} // Store reference
                onBuffer={() => {
                  // console.log('buffer', true);
                }}
                onEnd={() => {
                  // console.log('end', true);
                }}
                onError={() => {
                  // console.log('error', true);
                }}
                onLoad={() => {
                  // console.log('load', true);
                }}
                onProgress={() => {
                  // console.log('progress', true);
                }}
                resizeMode={'contain'}
                style={styles.backgroundVideo}
              />
            </ImageZoom>
          )}
        </View>
      );
    }
  }

  render() {
    return (
      <View style={styles.container}>
        <Snackbar
          style={{backgroundColor: 'black'}}
          visible={this.state.visible}
          onDismiss={() => this.setState({visible: false})}
          action={{
            onPress: () => {},
          }}>
          Saved to Camera Roll!
        </Snackbar>
        <View style={styles.header}>
          <Image
            style={styles.hdr_img}
            source={require('../../assets/LOGO_Blk_.png')}
          />
          <View style={styles.logout}>
            <TouchableOpacity onPress={() => this.takeViewShot()}>
              <Icon
                name="ios-cloud-download"
                size={25}
                type="ionicon"
                color="#cf8cac"
              />
            </TouchableOpacity>
          </View>
        </View>
        <ScrollView style={{width: '100%'}}>
          <View style={styles.contentArea}>
            <View style={styles.bannerArea}>
              {/* <Text onPress={() => Actions.exploreHome()} style={styles.temTitle}>
            TEMPLATE
          </Text>
          <TouchableOpacity onPress={this.takePicture.bind(this)}>
            <Icon
              name="ios-add-circle"
              size={80}
              type="ionicon"
              color="#cf8cac"
            />
          </TouchableOpacity> */}

              <ViewShot
                style={{flex: null, backgroundColor: '#fff'}}
                ref={ref => {
                  this.viewShot = ref;
                }}
                options={{format: 'jpg', quality: 1.0}}>
                <Grid style={styles.grid}>
                  <Col>
                    <Row style={styles.row1}>{this._renderCameraImage1()}</Row>
                    <Row style={styles.row2}>{this._renderCameraImage2()}</Row>
                  </Col>
                  <Col>
                    <Row style={styles.row3}>{this._renderCameraImage3()}</Row>
                    <Row style={styles.row4}>{this._renderCameraImage4()}</Row>
                  </Col>
                </Grid>
              </ViewShot>
            </View>
            {/* <View style={styles.bannerArea}>
              <Text
                onPress={() => Actions.exploreHome()}
                style={styles.temTitle}>
                TEMPLATE
              </Text>
              <TouchableOpacity onPress={this.takePicture.bind(this)}>
                <Icon
                  name="ios-add-circle"
                  size={80}
                  type="ionicon"
                  color="#cf8cac"
                />
              </TouchableOpacity>
            </View> */}
          </View>

          <View
            style={[
              this.state.collapsed
                ? styles.templatesArea
                : styles.templatesAreamargin,
            ]}>
            <Collapse
              isCollapsed={this.state.collapsed}
              onToggle={isCollapsed => this.setState({collapsed: isCollapsed})}>
              <CollapseHeader>
                <View
                  // eslint-disable-next-line prettier/prettier
                    style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    marginTop: 20,
                    paddingRight: 20,
                  }}>
                  <Text style={styles.temTitle2}>TEMPLATES</Text>
                  <View style={{marginLeft: 'auto'}}>
                    {this.state.collapsed ? (
                      <Icon
                        name="angle-up"
                        size={25}
                        type="font-awesome"
                        color="#cf8cac"
                      />
                    ) : (
                      <Icon
                        name="angle-down"
                        size={25}
                        type="font-awesome"
                        color="#cf8cac"
                      />
                    )}
                  </View>
                </View>
              </CollapseHeader>
              <CollapseBody>
                <ScrollView>
                  <View
                    style={{
                      position: 'relative',
                      flexDirection: 'row',
                      marginBottom: 10,
                      flex: 1,
                    }}>
                    <Swiper
                      height={180}
                      showsButtons={true}
                      nextButton=<View style={styles.borderbtn}>
                        <Text style={styles.buttonText}>›</Text>
                      </View>
                      prevButton=<View style={styles.borderbtn}>
                        <Text style={styles.buttonText}>‹</Text>
                      </View>
                      loop={false}
                      showsPagination={false}>
                      <View style={styles.slide1}>
                        <View style={styles.flexGrid}>
                          <Image
                            style={styles.gridImg}
                            source={require('../../assets/Picture5.jpg')}
                          />
                          <Text style={styles.gridTitle}>Template 1</Text>
                        </View>
                        <View style={styles.flexGrid}>
                          <Image
                            style={styles.gridImg}
                            source={require('../../assets/Picture4.jpg')}
                          />
                          <Text style={styles.gridTitle}>Template 2</Text>
                        </View>
                        <View style={styles.flexGrid}>
                          <Image
                            style={styles.gridImg}
                            source={require('../../assets/Picture6.jpg')}
                          />
                          <Text style={styles.gridTitle}>Template 3</Text>
                        </View>
                      </View>

                      <View style={styles.slide1}>
                        <TouchableOpacity
                          onPress={() => this._getTemplateIndex('4')}>
                          <View style={styles.flexGrid}>
                            <Image
                              style={styles.gridImg}
                              source={require('../../assets/Picture5.jpg')}
                            />

                            <Text style={styles.gridTitle}>Template 11</Text>
                          </View>
                        </TouchableOpacity>

                        <TouchableOpacity
                          onPress={() => this._getTemplateIndex('5')}>
                          <View style={styles.flexGrid}>
                            <Image
                              style={styles.gridImg}
                              source={require('../../assets/Picture4.jpg')}
                            />
                            <Text style={styles.gridTitle}>Template 21</Text>
                          </View>
                        </TouchableOpacity>

                        <TouchableOpacity
                          onPress={() => this._getTemplateIndex('6')}>
                          <View style={styles.flexGrid}>
                            <Image
                              style={styles.gridImg}
                              source={require('../../assets/Picture6.jpg')}
                            />
                            <Text style={styles.gridTitle}>Template 31</Text>
                          </View>
                        </TouchableOpacity>
                      </View>

                      <View style={styles.slide1}>
                        <TouchableOpacity
                          onPress={() => this._getTemplateIndex('7')}>
                          <View style={styles.flexGrid}>
                            <Image
                              style={styles.gridImg}
                              source={require('../../assets/Picture5.jpg')}
                            />
                            <Text style={styles.gridTitle}>Template 12</Text>
                          </View>
                        </TouchableOpacity>

                        <TouchableOpacity
                          onPress={() => this._getTemplateIndex('8')}>
                          <View style={styles.flexGrid}>
                            <Image
                              style={styles.gridImg}
                              source={require('../../assets/Picture4.jpg')}
                            />
                            <Text style={styles.gridTitle}>Template 22</Text>
                          </View>
                        </TouchableOpacity>

                        <TouchableOpacity
                          onPress={() => this._getTemplateIndex('9')}>
                          <View style={styles.flexGrid}>
                            <Image
                              style={styles.gridImg}
                              source={require('../../assets/Picture6.jpg')}
                            />
                            <Text style={styles.gridTitle}>Template 32</Text>
                          </View>
                        </TouchableOpacity>
                      </View>
                    </Swiper>
                  </View>
                </ScrollView>
              </CollapseBody>
            </Collapse>
          </View>

          {/* MODAL Popup */}
          <View style={{alignItems: 'center', justifyContent: 'center'}}>
            {/* Loading Popup */}

            <Modal
              style={{
                alignItems: 'center',
                justifyContent: 'center',
                height: '100%',
              }}
              animationType="fade"
              transparent={true}
              visible={this.state.modalVisible}
              onRequestClose={() => {
                Alert.alert('Modal has been closed.');
              }}>
              <ImageBackground
                source={require('../../assets/overlaybg.png')}
                style={{width: '100%', height: '100%'}}>
                <View
                  style={{
                    alignItems: 'center',
                    marginTop: 0,
                    height: 900,
                    paddingTop: 140,
                    width: '100%',
                  }}>
                  <TouchableOpacity
                    style={styles.btnClose}
                    onPress={() => {
                      this.setState({modalVisible: false});
                    }}>
                    <View>
                      <Icon
                        name="ios-close"
                        size={30}
                        type="ionicon"
                        color="#fff"
                      />
                    </View>
                  </TouchableOpacity>
                  <View style={styles.scrollPricing} />
                  <View
                    style={{
                      width: '100%',
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}>
                    <View style={styles.loadingmain}>
                      <Text style={styles.loadingTxt}>Upload your photo</Text>
                      {/* <Image
                        style={styles.loadingImg}
                        source={require('../../assets/Blooming-Rose-GIF.gif')}
                      /> */}

                      <TouchableOpacity
                        style={styles.btnTripDetail}
                        onPress={() => this.takePicture()}>
                        <Text
                          style={{
                            fontSize: 15,
                            color: '#fff',
                            fontFamily: 'GeorgiaPro-Bold',
                          }}>
                          CHOOSE PHOTO
                        </Text>
                      </TouchableOpacity>
                      {/* <Text
                        style={{
                          fontFamily: 'BAHNSCHRIFT',
                          color: '#cf8cac',
                          fontSize: 15,
                          textAlign: 'center',
                          marginTop: 20,
                          lineHeight: 30,
                        }}>
                        OR
                      </Text>
                      <TouchableOpacity
                        style={styles.btnTripDetail}
                        onPress={() => this.takeVideo()}>
                        <Text
                          style={{
                            fontSize: 15,
                            color: '#fff',
                            fontFamily: 'GeorgiaPro-Bold',
                          }}>
                          CHOOSE VIDEO
                        </Text>
                      </TouchableOpacity> */}
                    </View>
                  </View>
                </View>
              </ImageBackground>
            </Modal>

            <Modal
              style={{
                alignItems: 'center',
                justifyContent: 'center',
                height: '100%',
              }}
              animationType="fade"
              transparent={true}
              visible={this.state.modalVisible2}
              onRequestClose={() => {
                Alert.alert('Modal has been closed.');
              }}>
              <ImageBackground
                source={require('../../assets/overlaybg.png')}
                style={{width: '100%', height: '100%'}}>
                <View
                  style={{
                    alignItems: 'center',
                    marginTop: 0,
                    height: 900,
                    paddingTop: 180,
                    width: '100%',
                  }}>
                  <TouchableHighlight
                    style={styles.pribtnClose}
                    onPress={() => {
                      this.setModalVisible2();
                    }}>
                    <Icon
                      name="ios-close"
                      size={25}
                      type="ionicon"
                      color="#fff"
                    />
                  </TouchableHighlight>

                  <View style={styles.scrollPricing}>
                    <ScrollView horizontal={true}>
                      <View style={styles.priColumn}>
                        <Text style={styles.priceHdng}>Editing</Text>
                        <View style={{position: 'relative', marginBottom: 40}}>
                          <Text style={styles.priceTxt}>$4.99</Text>
                          <Text style={styles.dollarTxt}>per month</Text>
                        </View>
                        <Text style={styles.priFeatures}>
                          Unlimited access to editing features
                        </Text>
                        <Text style={styles.priFeatures}>StoryTemplates</Text>
                        <Text style={styles.priFeatures}>MoodBoards</Text>
                        <Text style={styles.priFeatures}>Stickers</Text>
                        <Image
                          style={styles.priflowerImg}
                          source={require('../../assets/pic2.png')}
                        />
                      </View>
                      {/* <View style={styles.priColumn}>
                        <Text style={styles.priceHdng}>Per City</Text>
                        <View style={{position: 'relative', marginBottom: 40}}>
                          <Text style={styles.priceTxt}>$89.99</Text>
                          <Text style={styles.dollarTxt}>pay per city</Text>
                        </View>
                        <Text style={styles.priFeatures}>
                          Yearlong access to a customized.
                        </Text>
                        <Text style={styles.priFeatures}>
                          Photo Itinerary in the city of your choosing
                        </Text>
                        <Image
                          style={styles.priflowerImg}
                          source={require('../../assets/pic2.png')}
                        />
                      </View> */}
                      {/* <View style={styles.priColumn}>
                        <Text style={styles.priceHdngBundle}>Bundle two</Text>
                        <Text style={styles.priceHdng}>... Or more cities</Text>
                        <View
                          style={{
                            position: 'relative',
                            marginBottom: 40,
                            marginTop: 20,
                          }}>
                          <Text style={styles.priceTxt}>15%</Text>
                          <Text style={styles.dollarTxt}>off</Text>
                        </View>
                        <Text style={styles.priFeatures}>
                          Purchase two or more cities to receive discount
                        </Text>
                        <Image
                          style={styles.priflowerImg}
                          source={require('../../assets/pic2.png')}
                        />
                      </View> */}
                    </ScrollView>
                  </View>
                </View>
              </ImageBackground>
            </Modal>
          </View>
        </ScrollView>

        <View style={styles.footMain}>
          <View style={styles.footFlex}>
            <TouchableOpacity onPress={() => Actions.exploreRegion()}>
              <Icon name="ios-planet" size={35} type="ionicon" color="#fff" />
            </TouchableOpacity>
          </View>

          <View style={styles.footFlex}>
            <TouchableOpacity onPress={() => Actions.Profile()}>
              <Icon size={30} name="ios-person" type="ionicon" color="#fff" />
            </TouchableOpacity>
          </View>

          <View style={styles.footFlex}>
            <TouchableOpacity
              style={styles.centerMenu}
              onPress={this._onSubmit.bind(this)}>
              <Image
                style={{
                  marginTop: 20,
                  width: 40,
                  height: 40,
                  resizeMode: 'contain',
                }}
                source={require('../../assets/heartlock.png')}
              />
            </TouchableOpacity>
          </View>

          <View style={styles.footFlex}>
            <TouchableOpacity onPress={() => Actions.home()}>
              <Icon size={27} name="ios-home" type="ionicon" color="#fff" />
            </TouchableOpacity>
          </View>

          <View style={styles.footFlex}>
            <TouchableOpacity onPress={() => Actions.Profile()}>
              <Icon size={27} name="ios-search" type="ionicon" color="#fff" />
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  templatesArea: {
    backgroundColor: '#e7c7d6',
    paddingLeft: 20,
    paddingBottom: 20,
    // marginTop: 60,
  },
  templatesAreamargin: {
    backgroundColor: '#e7c7d6',
    paddingLeft: 20,
    paddingBottom: 20,
    marginTop: 100,
  },
  temTitle2: {
    fontSize: 16,
    textAlign: 'center',
    color: '#cf8cac',
    fontFamily: 'GeorgiaPro-Bold',
  },
  temTitle: {
    fontSize: 50,
    textAlign: 'center',
    color: '#cf8cac',
    paddingVertical: 5,
    backgroundColor: '#f0e0e2',
    marginVertical: 10,
    width: '100%',
    fontFamily: 'BAHNSCHRIFT',
  },
  logout: {
    position: 'absolute',
    right: 20,
    top: 20,
  },
  gridTitle: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 30,
    margin: 'auto',
    fontSize: 14,
    textAlign: 'center',
    color: '#cf8cac',
    textTransform: 'uppercase',
    paddingVertical: 5,
    backgroundColor: 'rgba(240,224,226, .8)',
    fontFamily: 'BAHNSCHRIFT',
  },
  gridImg: {
    width: '100%',
    height: 160,
    resizeMode: 'stretch',
    borderRadius: 5,
    opacity: 0.33,
  },
  flexGrid: {
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 10,
    width: 105,
    marginRight: 10,
    borderRadius: 5,
    backgroundColor: '#fff',
  },
  bannerArea: {
    alignItems: 'center',
    justifyContent: 'center',
    position: 'relative',
    height: 360,
    width: '100%',
    backgroundColor: '#ffffff',
    borderRadius: 5,
    borderWidth: 1,
    borderColor: '#aaa',
    borderStyle: 'solid',
  },
  contentArea: {
    paddingHorizontal: 20,
    paddingBottom: 20,
    paddingTop: 10,
  },
  footMain: {
    backgroundColor: '#cf8cac',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    paddingTop: 10,
    paddingBottom: 10,
    maxHeight: 55,
  },
  centerMenu: {
    backgroundColor: '#cf8cac',
    position: 'relative',
    top: -15,
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    borderTopLeftRadius: 50,
    borderTopRightRadius: 50,
  },
  footFlex: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  container: {
    alignItems: 'center',
    justifyContent: 'flex-start',
    height: '100%',
    paddingTop: 30,
    width: '100%',
  },
  header: {
    position: 'relative',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    elevation: 1,
    backgroundColor: '#ffffff',
    width: '100%',
    paddingTop: 6,
    paddingBottom: 6,
  },
  hdr_img: {
    width: 200,
    height: 50,
    marginBottom: 5,
    resizeMode: 'stretch',
  },
  body: {
    flex: 1,
    height: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  ftr_hdngs: {
    textAlign: 'center',
    color: '#fff',
    fontSize: 25,
  },
  instagram: {
    backgroundColor: '#cf8cac',
    alignItems: 'stretch',
    paddingVertical: 13,
    textAlign: 'center',
    lineHeight: 20,
    borderRadius: 30,
    paddingBottom: 15,
    fontSize: 16,
    marginBottom: 15,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    flex: 1,
    marginRight: 5,
  },
  btnEmail: {
    paddingVertical: 5,
    backgroundColor: '#cf8cac',
    textAlign: 'center',
    borderRadius: 30,
    fontSize: 14,
    marginBottom: 15,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    flex: 1,
    marginLeft: 5,
  },
  bannerImg: {
    height: 320,
    width: '100%',
    resizeMode: 'cover',
  },
  layimg: {
    width: '100%',
    // height: 100,
    // // marginBottom: 5,
    // resizeMode: 'cover',
  },
  row1: {
    // backgroundColor: '#cf8cac',
    borderWidth: 1,
    borderColor: '#aaa',
    // borderStyle: 'solid',
  },
  row2: {
    // backgroundColor: '#cf8cac',
    borderWidth: 1,
    borderColor: '#aaa',
    // borderStyle: 'dashed',
  },
  row3: {
    // backgroundColor: 'green',
    borderWidth: 1,
    borderColor: '#aaa',
    // borderStyle: 'dashed',
  },
  row4: {
    // backgroundColor: 'pink',
    borderWidth: 1,
    borderColor: '#aaa',
    // borderStyle: 'dashed',
  },
  grid: {
    width: '100%',
  },
  icon: {
    marginLeft: 60,
    marginTop: 60,
  },
  /*********RN CAMERA*********** */
  preview: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  capture: {
    flex: 0,
    backgroundColor: '#fff',
    borderRadius: 5,
    padding: 15,
    paddingHorizontal: 20,
    alignSelf: 'center',
    margin: 20,
  },
  containervid: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    height: '100%',
    width: '100%',
  },
  backgroundVideo: {
    position: 'absolute',
    top: 0,
    left: 10,
    bottom: 0,
    right: 10,
  },
  loadingImg: {
    width: 250,
    height: 250,
    resizeMode: 'contain',
  },
  loadingTxt: {
    fontFamily: 'BAHNSCHRIFT',
    textTransform: 'uppercase',
    fontSize: 20,
    textAlign: 'center',
    color: '#cf8cac',
  },
  loadingmain: {
    backgroundColor: '#fff',
    justifyContent: 'center',
    alignItems: 'center',
    padding: 20,
    maxWidth: '90%',
    borderRadius: 10,
    top: 70,
    elevation: 1,
  },
  btnTripDetail: {
    paddingVertical: 15,
    backgroundColor: '#cf8cac',
    borderRadius: 30,
    marginTop: 25,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    // flexDirection: 'row',
    flex: 1,
    width: 200,
    maxHeight: 50,
    minHeight: 50,
  },
  btnClose: {
    borderRadius: 30,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    position: 'absolute',
    top: 30,
    right: 20,
    zIndex: 99,
    width: 30,
    height: 30,
    paddingBottom: 0,
    backgroundColor: '#cf8cac',
  },
  slide1: {
    // flex: 1,
    // justifyContent: 'center',
    // alignItems: 'center',
    flexDirection: 'row',
  },
  borderbtn: {
    top: 5,
    borderRadius: 50,
    borderWidth: 3,
    borderColor: '#cf8cac',
    width: 30,
    backgroundColor: '#f0e0e2',
    height: 30,
    alignItems: 'center',
    justifyContent: 'center',
  },
  buttonText: {
    fontSize: 40,
    color: '#cf8cac',
    borderRadius: 5,
    marginTop: -10,
  },

  /************PRICING MODAL*************** */
  scrollPricing: {
    width: '100%',
    flexDirection: 'row',
    paddingLeft: 20,
  },
  priColumn: {
    backgroundColor: '#cf8cac',
    paddingHorizontal: 25,
    paddingVertical: 30,
    width: 320,
    marginBottom: 10,
    marginRight: 10,
    borderRadius: 15,
  },
  priFeatures: {
    color: '#fff',
    fontSize: 16,
    textAlign: 'center',
    marginBottom: 20,
    textTransform: 'uppercase',
    fontFamily: 'BAHNSCHRIFT',
  },
  priceHdngBundle: {
    color: '#fff',
    fontSize: 25,
    textAlign: 'center',
    fontFamily: 'GeorgiaPro-Bold',
    textTransform: 'uppercase',
  },
  priceHdng: {
    color: '#fff',
    fontSize: 18,
    textAlign: 'center',
    marginBottom: 10,
    fontFamily: 'GeorgiaPro-Bold',
    textTransform: 'uppercase',
  },
  priceTxt: {
    color: '#fff',
    fontSize: 50,
    textAlign: 'center',
    fontFamily: 'GeorgiaPro-Bold',
  },
  dollarTxt: {
    fontSize: 40,
    fontFamily: 'Southampton',
    color: '#fff',
    position: 'absolute',
    bottom: -30,
    right: 10,
  },
  pribtnClose: {
    borderRadius: 30,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    position: 'absolute',
    top: 30,
    right: 20,
    zIndex: 99,
    width: 30,
    height: 30,
    backgroundColor: '#cf8cac',
  },
  priflowerImg: {
    width: 60,
    height: 60,
    resizeMode: 'contain',
    position: 'absolute',
    bottom: -10,
    transform: [{rotate: '50deg'}],
    left: '46%',
  },
});
