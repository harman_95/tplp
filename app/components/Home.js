/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  ScrollView,
  Linking,
  AsyncStorage,
} from 'react-native';
import {Icon} from 'react-native-elements';
import {Actions} from 'react-native-router-flux';
import Swiper from 'react-native-swiper';
export default class Home extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loggedInUser: 'fdghj',
      products: [],
      cityImage: null,
      cityTitle: null,
      cityType: null,
      city: null,
      promoImage: null,
      promoTitle: null,
    };
    this._getBanners();
  }

  /*******************ON LOAD**************** */
  // componentWillMount() {
  //   console.log('component mount');
  //   console.log('***********');
  // }

  async componentDidMount() {
    console.log('component DID mount');
    // await AsyncStorage.getItem('@userLoggedInId').then(value => {
    //   console.log(value);
    //   if (value == null || value === '' || value === undefined) {
    //     Actions.Signin();
    //   } else {
    //   }
    // });
  }

  _getBanners() {
    fetch('http://3.135.63.132:3000/api/v1/tplp/banner/get_banner')
      .then(response => response.json())
      .then(json => {
        console.log(json);
        console.log(json.data);
        for (var i = 0; i < json.data.length; i++) {
          console.log('*****************');
          console.log(json.data[i].type);

          if (json.data[i].type == 'Featured City') {
            this.setState({
              cityImage: json.data[i].image,
              cityType: json.data[i].type,
              city: json.data[i].country,
            });
          } else if (json.data[i].type == 'Promo') {
            this.setState({
              promoImage: json.data[i].image,
              promoTitle: json.data[i].title,
            });
          }
        }
      });
  }

  goToInsta() {
    let concatedFinalUrl = 'https://www.instagram.com/theprettylittlepost';
    Linking.canOpenURL(concatedFinalUrl)
      .then(supported => {
        if (!supported) {
          console.log("Can't handle url: " + concatedFinalUrl);
        } else {
          this.listener(concatedFinalUrl);
          return Linking.openURL(concatedFinalUrl);
        }
      })
      .catch(err => console.error('An error occurred', err));
  }

  listener(uri) {
    Linking.addEventListener(uri, this._handleOpenURL);
  }

  _onSubmit = () => {
    AsyncStorage.clear();
    Actions.Signin();
  };

  renderSecondBanner() {
    if (this.state.promoImage == null) {
      return <View />;
    } else {
      return (
        <View style={styles.bannerArea}>
          <Image
            source={{uri: this.state.promoImage}}
            style={styles.bannerImg}
          />
          <View style={styles.absoluTxt}>
            <Text
              style={{
                fontFamily: 'BAHNSCHRIFT',
                color: '#cf8cac',
                fontSize: 22,
                textAlign: 'center',
                textTransform: 'uppercase',
                fontWeight: 'bold',
              }}>
              {this.state.promoTitle}
            </Text>
          </View>
        </View>
      );
    }
  }

  renderFirstBanner() {
    if (this.state.promoImage == null) {
      return <View />;
    } else {
      return (
        <View style={styles.bannerArea}>
          <Image
            source={{uri: this.state.cityImage}}
            style={styles.bannerImg}
          />
          <View style={styles.absoluTxt}>
            <Text
              style={{
                fontFamily: 'BAHNSCHRIFT',
                color: '#cf8cac',
                fontSize: 22,
                textAlign: 'center',
                textTransform: 'uppercase',
                fontWeight: 'bold',
              }}>
              {this.state.city}
            </Text>
            <Text
              style={{
                fontFamily: 'BAHNSCHRIFT',
                textAlign: 'center',
                color: '#cf8cac',
                textTransform: 'uppercase',
              }}>
              {this.state.cityType}
            </Text>
          </View>
        </View>
      );
    }
  }

  renderThirdBanner() {
    return (
      <View style={styles.bannerArea}>
        <TouchableOpacity
          style={styles.nocss}
          onPress={() => Actions.Profile()}>
          <Image
            source={require('../../assets/Picture3.jpg')}
            style={styles.bannerImg}
          />
          <View style={styles.absoluTxt}>
            <Text
              style={{
                fontFamily: 'BAHNSCHRIFT',
                color: '#cf8cac',
                fontSize: 22,
                textAlign: 'center',
                textTransform: 'uppercase',
                fontWeight: 'bold',
              }}>
              READY TO MINGLE... US {'\n'}
              TOO!
            </Text>
            <Text
              style={{
                fontFamily: 'BAHNSCHRIFT',
                textAlign: 'center',
                color: '#cf8cac',
                textTransform: 'uppercase',
              }}>
              SUBMIT YOUR BUSINESS HERE TO BE FEATURED
            </Text>
          </View>
        </TouchableOpacity>
      </View>
    );
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.header}>
          <Image
            style={styles.hdr_img}
            source={require('../../assets/LOGO_Blk_.png')}
          />
        </View>

        <ScrollView style={{width: '100%', backgroundColor: '#fff'}}>
          {/* sliderArea */}
          <View style={styles.sliderArea}>
            <Swiper
              showsButtons={false}
              dotColor="rgba(255,255,255, 1)"
              activeDotColor="#cf8cac"
              autoplay={true}>
              {this.renderFirstBanner()}
              {this.renderSecondBanner()}
              {this.renderThirdBanner()}
            </Swiper>
          </View>
          {/* sliderArea end */}

          <View style={styles.contentArea}>
            <View
              style={{
                display: 'flex',
                flexDirection: 'row',
                marginTop: -25,
                paddingHorizontal: 20,
              }}>
              <TouchableOpacity
                style={styles.instagram}
                onPress={() => Actions.EditHome()}>
                <Image
                  style={styles.flowerImg}
                  source={require('../../assets/pic3.png')}
                />
                {/* <Icon
                  name="pencil"
                  size={15}
                  type="font-awesome"
                  color="#fff"
                /> */}
                <Text
                  style={{
                    color: '#cf8cac',
                    fontSize: 15,
                    marginLeft: 10,
                    fontFamily: 'BAHNSCHRIFT',
                    fontWeight: 'bold',
                  }}>
                  EDIT
                </Text>
              </TouchableOpacity>

              {/* <View style={styles.btnEmail}>  */}
              <TouchableOpacity
                style={styles.instagram}
                onPress={() => Actions.exploreRegion()}>
                <Image
                  style={styles.flowerImg2}
                  source={require('../../assets/pic3.png')}
                />
                {/* <Icon name="ios-eye" size={20} type="ionicon" color="#fff" /> */}
                <Text
                  style={{
                    fontFamily: 'BAHNSCHRIFT',
                    color: '#cf8cac',
                    fontSize: 15,
                    marginLeft: 10,
                    fontWeight: 'bold',
                  }}>
                  EXPLORE
                </Text>
              </TouchableOpacity>
              {/* </View> */}
            </View>

            <View
              style={{
                justifyContent: 'center',
                position: 'relative',
                paddingHorizontal: 20,
                paddingBottom: 30,
              }}>
              <TouchableOpacity onPress={() => this.goToInsta()}>
                <Text style={styles.weKnow}>
                  WE KNOW, WE KNOW… {'\n'}YOU’RE OUR BEST {'\n'} FRIEND TOO!{' '}
                </Text>
                <Text style={styles.weKnow2}>
                  WE KNOW, WE KNOW… {'\n'}YOU’RE OUR BEST FRIEND TOO!{' '}
                </Text>
              </TouchableOpacity>
              <Text style={styles.signature}>
                Xx{'\n'}
                Bay and Liv
              </Text>
            </View>
            <View>
              <Text style={styles.mainTitle}>FOUNDER’S FAVES</Text>
            </View>
            <View
              style={{
                flexDirection: 'row',
                marginTop: 5,
                paddingHorizontal: 20,
              }}>
              <View style={styles.flexGrid}>
                <Image
                  style={styles.gridImg}
                  source={require('../../assets/Picture5.jpg')}
                />
                <Text style={styles.gridTitle}>BALI</Text>
              </View>
              <View style={styles.flexGrid}>
                <Image
                  style={styles.gridImg}
                  source={require('../../assets/Picture4.jpg')}
                />
                <Text style={styles.gridTitle}>BARCELONA</Text>
              </View>
              <View style={styles.flexGrid}>
                <Image
                  style={styles.gridImg}
                  source={require('../../assets/Picture6.jpg')}
                />
                <Text style={styles.gridTitle}>CHARLESTON</Text>
              </View>
            </View>
            {/* <View style={{flexDirection:'row',marginTop:5,paddingHorizontal:20,}}>
				<View style={styles.flexGrid}>
					<Image style={styles.gridImg} source={require('../../assets/Picture6.jpg')}/>
					<Text style={styles.gridTitle}>BALI</Text>
				</View>
				<View style={styles.flexGrid}>
					<Image style={styles.gridImg} source={require('../../assets/Picture7.jpg')}/>
					<Text style={styles.gridTitle}>BARCELONA</Text>
				</View>
				<View style={styles.flexGrid}>
					<Image style={styles.gridImg} source={require('../../assets/Picture1.jpg')}/>
					<Text style={styles.gridTitle}>CHARLESTON</Text>
				</View>
			</View>  */}
          </View>
        </ScrollView>

        <View style={styles.footMain}>
          <View style={styles.footFlex}>
            <TouchableOpacity onPress={() => Actions.exploreRegion()}>
              <Icon name="ios-planet" size={35} type="ionicon" color="#fff" />
            </TouchableOpacity>
          </View>

          <View style={styles.footFlex}>
            <TouchableOpacity onPress={() => Actions.Profile()}>
              <Icon size={30} name="ios-person" type="ionicon" color="#fff" />
            </TouchableOpacity>
          </View>

          <View style={styles.footFlex}>
            <TouchableOpacity
              style={styles.centerMenu}
              onPress={this._onSubmit.bind(this)}>
              <Image
                style={{
                  marginTop: 20,
                  width: 40,
                  height: 40,
                  resizeMode: 'contain',
                }}
                source={require('../../assets/heartlock.png')}
              />
            </TouchableOpacity>
          </View>

          <View style={styles.footFlex}>
            <TouchableOpacity onPress={() => Actions.home()}>
              <Icon size={27} name="ios-home" type="ionicon" color="#fff" />
            </TouchableOpacity>
          </View>

          <View style={styles.footFlex}>
            <TouchableOpacity onPress={() => Actions.Profile()}>
              <Icon size={27} name="ios-search" type="ionicon" color="#fff" />
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  signature: {
    textAlign: 'center',
    color: '#cf8cac',
    fontFamily: 'Southampton',
    fontSize: 25,
    transform: [{rotate: '-18deg'}],
    position: 'absolute',
    right: 30,
    lineHeight: 15,
    top: 90,
  },
  mainTitle: {
    fontSize: 15,
    backgroundColor: '#cf8cac',
    paddingVertical: 7,
    textAlign: 'center',
    color: '#fff',
    fontFamily: 'GeorgiaPro-Bold',
  },
  flowerImg2: {
    width: 50,
    height: 50,
    position: 'absolute',
    resizeMode: 'contain',
    right: -20,
    bottom: -15,
  },
  flowerImg: {
    width: 50,
    height: 50,
    position: 'absolute',
    resizeMode: 'contain',
    left: -20,
    top: -15,
    transform: [{rotate: '180deg'}],
  },
  sliderArea: {
    backgroundColor: '#000',
    height: 300,
  },
  gridTitle: {
    position: 'absolute',
    left: 0,
    right: 0,
    textAlign: 'center',
    backgroundColor: '#f0e0e2',
    color: '#cf8cac',
    paddingVertical: 5,
    fontSize: 12,
    fontFamily: 'BAHNSCHRIFT',
  },
  gridImg: {
    width: '100%',
    height: 110,
    resizeMode: 'cover',
    opacity: 0.5,
  },
  flexGrid: {
    flex: 1,
    borderColor: '#fff',
    borderWidth: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  weKnow: {
    color: '#cf8cac',
    fontFamily: 'BAHNSCHRIFT',
    fontSize: 25,
    textAlign: 'center',
    lineHeight: 30,
    letterSpacing: -2,
    marginTop: 20,
    marginBottom: 20,
    paddingVertical: 10,
    paddingHorizontal: 0,
  },
  weKnow2: {
    color: '#cf8cac',
    fontFamily: 'GeorgiaPro-Bold',
    fontSize: 20,
    textAlign: 'center',
    lineHeight: 30,
    position: 'absolute',
    letterSpacing: -2,
    top: 30,
    opacity: 0.1,
    marginTop: 0,
    marginBottom: 0,
    paddingVertical: 10,
    paddingHorizontal: 0,
  },
  absoluTxt: {
    position: 'absolute',
    top: 60,
    bottom: 60,
    left: 20,
    right: 20,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: 15,
    borderRadius: 5,
    fontFamily: 'BAHNSCHRIFT',
    borderWidth: 1,
    borderColor: '#cf8cac',
  },
  bannerArea: {
    alignItems: 'center',
    justifyContent: 'center',
    position: 'relative',
    fontFamily: 'BAHNSCHRIFT',
    backgroundColor: '#fff',
  },
  bannerImg: {
    height: 300,
    width: '100%',
    resizeMode: 'cover',
    opacity: 0.33,
  },
  contentArea: {
    paddingBottom: 20,
  },
  footMain: {
    backgroundColor: '#cf8cac',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    paddingTop: 10,
    paddingBottom: 10,
    maxHeight: 55,
  },
  centerMenu: {
    backgroundColor: '#cf8cac',
    position: 'relative',
    top: -15,
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    borderTopLeftRadius: 50,
    borderTopRightRadius: 50,
  },
  footFlex: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-start',
    height: '100%',
    paddingTop: 0,
  },
  header: {
    position: 'relative',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    elevation: 1,
    // backgroundColor: '#f0e0e2',
    backgroundColor: '#ffffff',
    width: '100%',
    paddingTop: 6,
    paddingBottom: 6,
  },

  hdr_img: {
    width: 200,
    height: 50,
    resizeMode: 'stretch',
  },
  body: {
    flex: 1,
    height: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  body_img: {
    alignItems: 'center',
    width: 220,
    height: 186,
  },
  ftr_hdngs: {
    textAlign: 'center',
    color: '#fff',
    fontSize: 25,
  },
  instagram: {
    backgroundColor: '#f0e0e2',
    alignItems: 'stretch',
    paddingVertical: 13,
    textAlign: 'center',
    lineHeight: 20,
    borderRadius: 30,
    paddingBottom: 15,
    fontSize: 16,
    marginBottom: 15,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    flex: 1,
    marginRight: 5,
  },
  btnEmail: {
    paddingVertical: 5,
    backgroundColor: '#cf8cac',
    textAlign: 'center',
    borderRadius: 30,
    fontSize: 14,
    marginBottom: 15,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    flex: 1,
    marginLeft: 5,
  },
  nocss: {
    height: 300,
    width: '100%',
    resizeMode: 'cover',
  }
});
