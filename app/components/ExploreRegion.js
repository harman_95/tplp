import React from 'react';
import {
  Platform,
  AppRegistry,
  StyleSheet,
  Text,
  View,
  TextInput,
  Image,
  Button,
  TouchableHighlight,
  TouchableOpacity,
  CheckBox,
  ScrollView,
  FlatList,
  AsyncStorage,
} from 'react-native';
import {Icon} from 'react-native-elements';
import {Actions} from 'react-native-router-flux';
import {Col, Row, Grid} from 'react-native-easy-grid';
import ContentLoader from 'react-native-content-loader';
import {Circle, Rect} from 'react-native-svg';

const asia = require('../../assets/Continents Page_Asia.jpg');
const africa = require('../../assets/Continents Page_Africa.jpg');
const australia = require('../../assets/Continents Page_Australia.jpg');
const europe = require('../../assets/Continents Page_Europe1.jpg');
const n_america = require('../../assets/Continents Page_Americas.jpg');
const s_america = require('../../assets/Continents Page_South America.jpg');

export default class exploreRegion extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      dataSource: [
        {
          id: '1',
          name: 'Asia',
          image: 'http://3.135.63.132/images/Continents Page_Asia.jpg',
        },
        {
          id: '2',
          name: 'Africa',
          image: 'http://3.135.63.132/images/Continents Page_Africa.jpg',
        },
        {
          id: '3',
          name: 'Australia',
          image:
            'http://3.135.63.132/images/Continents Page_Australia.jpg',
        },
        {
          id: '4',
          name: 'Europe',
          image:
            'http://3.135.63.132/images/Continents Page_Europe1.jpg',
        },
        {
          id: '5',
          name: 'N.America',
          image:
            'http://3.135.63.132/images/Continents Page_Americas.jpg',
        },
        {
          id: '6',
          name: 'S.America',
          image:
            'http://3.135.63.132/images/Continents Page_South America.jpg',
        },
      ],
      loader: true,
    };
    // setTimeout(() => {
    //   this.setState({
    //     loader: false,
    //   });
    // }, 3000);
  }

  componentDidMount() {
    this._getCities();
  }

  _onSubmit = () => {
    AsyncStorage.clear();
    Actions.Signin();
  };

  /***********RENDER LOADER COLS********** */
  _renderCol() {
    return (
      <Col>
        <Row>
          <ContentLoader height={100} duration={1000}>
            <Rect x="0" y="5" rx="0" ry="0" width="100" height="100" />
          </ContentLoader>
        </Row>
        <Row>
          <ContentLoader height={100} duration={1000}>
            <Rect x="0" y="5" rx="0" ry="0" width="100" height="100" />
          </ContentLoader>
        </Row>
        <Row>
          <ContentLoader height={100} duration={1000}>
            <Rect x="0" y="5" rx="0" ry="0" width="100" height="100" />
          </ContentLoader>
        </Row>
        <Row>
          <ContentLoader height={100} duration={1000}>
            <Rect x="0" y="5" rx="0" ry="0" width="100" height="100" />
          </ContentLoader>
        </Row>
      </Col>
    );
  }

  _getCities() {
    // fetch('http://3.135.63.132:3000/api/v1/tplp/cities/getCitiesList')
    //   .then(response => response.json())
    //   .then(json => {

    setTimeout(() => {
      this.setState({
        loader: false,
      });
    }, 3000);

    //     if (json.list.length > 0) {
    //       for (var i = 0; i < json.list.length; i++) {
    //         json.list[i].cityimage =
    //           'http://3.135.63.132:3000/images/' + json.list[i].cityimage;
    //       }
    this.setState({
      dataSource: this.state.dataSource,
    });
    //       console.log(this.state.dataSource);
    //     } else {
    //     }
    //   });
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.header}>
          <Image
            style={styles.hdr_img}
            source={require('../../assets/LOGO_Blk_.png')}
          />
        </View>

        {/* {this._getCities()} */}

        <ScrollView style={{width: '100%', backgroundColor: '#fff'}}>
          <View style={styles.bannerArea}>
            <Image
              style={styles.bannerImg}
              source={require('../../assets/Picture7.jpg')}
            />
            <View style={styles.absoluTxt}>
              <Text style={styles.whereTxt}>Where are you off to...</Text>
              <Text style={styles.absoTxt1}>Let's</Text>
              <Text style={styles.absoTxt2}>Let's</Text>
              <Text style={styles.absoTxt3}>Explore</Text>
              <Text style={styles.absoTxt4}>Explore</Text>
            </View>
          </View>

          <View style={{marginTop: 10, marginBottom: 5}}>
            <Text style={styles.mainTitle}>Regions</Text>
          </View>

          <View style={styles.contentArea}>
            {this.state.loader == false ? (
              this.state.dataSource.length > 0 ? (
                <FlatList
                  data={this.state.dataSource}
                  renderItem={({item}) =>
                    item.id === '1' ? (
                      <View style={styles.flexGrid}>
                        <Image
                          style={styles.gridImg}
                          source={{uri: item.image}}
                        />

                        <Text
                          onPress={() => Actions.exploreHome()}
                          style={styles.gridTitle}>
                          {item.name}
                        </Text>
                      </View>
                    ) : (
                      <View style={styles.flexGrid}>
                        <Image
                          style={styles.gridImg}
                          source={{uri: item.image}}
                        />

                        <Text
                          onPress={() => Actions.exploreHome()}
                          style={styles.gridTitle}>
                          {item.name}
                        </Text>
                      </View>
                    )
                  }
                  //Setting the number of column
                  numColumns={3}
                  keyExtractor={(item, index) => index.toString()}
                />
              ) : (
                <View style={styles.fullWidth}>
                  <Text style={styles.notFound}>No Cities Found !</Text>
                </View>
              )
            ) : (
              <Grid>
                {this._renderCol()}
                {this._renderCol()}
                {this._renderCol()}
              </Grid>
            )}
          </View>
        </ScrollView>

        <View style={styles.footMain}>
          <View style={styles.footFlex}>
            <TouchableOpacity onPress={() => Actions.exploreRegion()}>
              <Icon name="ios-planet" size={35} type="ionicon" color="#fff" />
            </TouchableOpacity>
          </View>

          <View style={styles.footFlex}>
            <TouchableOpacity onPress={() => Actions.Profile()}>
              <Icon size={30} name="ios-person" type="ionicon" color="#fff" />
            </TouchableOpacity>
          </View>

          <View style={styles.footFlex}>
            <TouchableOpacity
              style={styles.centerMenu}
              onPress={this._onSubmit.bind(this)}>
              <Image
                style={{
                  marginTop: 20,
                  width: 40,
                  height: 40,
                  resizeMode: 'contain',
                }}
                source={require('../../assets/heartlock.png')}
              />
            </TouchableOpacity>
          </View>

          <View style={styles.footFlex}>
            <TouchableOpacity onPress={() => Actions.home()}>
              <Icon size={27} name="ios-home" type="ionicon" color="#fff" />
            </TouchableOpacity>
          </View>

          <View style={styles.footFlex}>
            <TouchableOpacity onPress={() => Actions.Profile()}>
              <Icon size={27} name="ios-search" type="ionicon" color="#fff" />
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  whereTxt: {
    fontFamily: 'Southampton',
    color: '#fff',
    fontSize: 52,
    textAlign: 'left',
    paddingHorizontal: 20,
    position: 'relative',
    zIndex: 1,
  },
  mainTitle: {
    fontSize: 15,
    backgroundColor: '#cf8cac',
    paddingVertical: 7,
    textAlign: 'center',
    color: '#fff',
    textTransform: 'uppercase',
    fontFamily: 'GeorgiaPro-Bold',
    letterSpacing: 4,
  },
  absoTxt3: {
    fontFamily: 'GeorgiaPro-Bold',
    color: '#cf8cac',
    fontSize: 35,
    opacity: 0.2,
    textAlign: 'center',
    paddingHorizontal: 20,
    position: 'absolute',
    zIndex: 0,
    right: 0,
    top: 60,
  },
  absoTxt4: {
    fontFamily: 'GeorgiaPro-Bold',
    color: '#cf8cac',
    fontSize: 40,
    opacity: 0.6,
    textAlign: 'center',
    paddingHorizontal: 20,
    position: 'absolute',
    zIndex: 0,
    right: 0,
    top: 90,
  },
  absoTxt1: {
    fontFamily: 'GeorgiaPro-Bold',
    color: '#cf8cac',
    fontSize: 30,
    opacity: 0.2,
    textAlign: 'center',
    paddingHorizontal: 20,
    position: 'absolute',
    zIndex: 0,
    right: 0,
    top: 10,
  },
  absoTxt2: {
    fontFamily: 'GeorgiaPro-Bold',
    color: '#cf8cac',
    fontSize: 40,
    opacity: 0.6,
    textAlign: 'center',
    paddingHorizontal: 20,
    position: 'absolute',
    zIndex: 0,
    right: 0,
    top: 30,
  },
  letsEdit: {
    position: 'absolute',
    right: 10,
    bottom: 0,
    top: 0,
    zIndex: 0,
    opacity: 0.5,
    width: 140,
    resizeMode: 'contain',
  },
  gridTitle: {
    position: 'absolute',
    left: 0,
    right: 0,
    margin: 'auto',
    fontSize: 12,
    textAlign: 'center',
    textTransform: 'uppercase',
    color: '#cf8cac',
    paddingVertical: 5,
    backgroundColor: 'rgba(240,224,226, .8)',
    elevation: 1,
    marginVertical: 10,
    fontFamily: 'metropolis-bold',
  },
  gridImg: {
    width: '100%',
    height: 120,
    resizeMode: 'cover',
    opacity: 0.5,
    borderWidth: 1,
    borderColor: '#e9f0f6',
  },
  flexGrid: {
    // alignItems: 'center',
    justifyContent: 'center',
    marginTop: 0,
    borderRadius: 0,
    borderColor: '#cf8cac',
    borderWidth: 2,
    backgroundColor: '#fff',
    width: '33.3%',
    marginLeft: 2,
    marginTop: 2,
    flex: 1,
    flexDirection: 'column',
  },
  weKnow: {
    color: '#cf8cac',
    fontSize: 16,
    textAlign: 'center',
    lineHeight: 22,
    marginTop: 20,
    marginBottom: 20,
    paddingVertical: 10,
    paddingHorizontal: 40,
    fontWeight: 'bold',
    backgroundColor: '#f0e0e2',
    borderRadius: 100,
  },
  absoluTxt: {
    position: 'absolute',
    bottom: 20,
    left: 20,
    top: 20,
    borderWidth: 1,
    borderColor: '#cf8cac',
    right: 20,
    display: 'flex',
    justifyContent: 'flex-end',
    paddingBottom: 10,
    borderRadius: 5,
  },
  bannerArea: {
    alignItems: 'center',
    justifyContent: 'center',
    position: 'relative',
  },
  bannerImg: {
    height: 250,
    width: '100%',
    resizeMode: 'cover',
    opacity: 0.5,
  },
  contentArea: {
    paddingHorizontal: 20,
    paddingBottom: 20,
  },
  footMain: {
    backgroundColor: '#cf8cac',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    paddingTop: 10,
    paddingBottom: 10,
    maxHeight: 55,
  },
  centerMenu: {
    backgroundColor: '#cf8cac',
    position: 'relative',
    top: -15,
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    borderTopLeftRadius: 50,
    borderTopRightRadius: 50,
  },
  footFlex: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-start',
    height: '100%',
    paddingTop: 0,
  },
  header: {
    position: 'relative',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    elevation: 1,
    backgroundColor: '#ffffff',
    width: '100%',
    paddingTop: 6,
    paddingBottom: 6,
  },
  hdr_img: {
    width: 200,
    height: 50,
    marginBottom: 5,
    resizeMode: 'stretch',
  },
  body: {
    flex: 1,
    height: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  body_img: {
    alignItems: 'center',
    width: 220,
    height: 186,
  },
  ftr_hdngs: {
    textAlign: 'center',
    color: '#fff',
    fontSize: 25,
  },
  instagram: {
    backgroundColor: '#cf8cac',
    alignItems: 'stretch',
    paddingVertical: 13,
    textAlign: 'center',
    lineHeight: 20,
    borderRadius: 30,
    paddingBottom: 15,
    fontSize: 16,
    marginBottom: 15,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    flex: 1,
    marginRight: 5,
  },
  btnEmail: {
    paddingVertical: 5,
    backgroundColor: '#cf8cac',
    textAlign: 'center',
    borderRadius: 30,
    fontSize: 14,
    marginBottom: 15,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    flex: 1,
    marginLeft: 5,
  },
  notFound: {
    fontSize: 16,
    textAlign: 'center',
    alignItems: 'center',
    backgroundColor: '#fcfcfc',
    paddingHorizontal: 60,
    paddingVertical: 10,
    marginTop: 15,
    borderColor: '#ddd',
    borderWidth: 1,
    borderRadius: 5,
    width: '100%',
  },
  fullWidth: {
    width: '100%',
    display: 'flex',
    flexDirection: 'column',
  },
});
