import React, {Component} from 'react';
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  Image,
  TouchableOpacity,
  TouchableHighlight,
  ScrollView,
} from 'react-native';
import {Icon} from 'react-native-elements';
import {Snackbar} from 'react-native-paper';
import Spinner from 'react-native-loading-spinner-overlay';
import {Actions} from 'react-native-router-flux';
import axios from 'axios';

class Register extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      error: '',
      spinner: false,
      loggedInUser: '',
      accessToken: null,
      visible: false,
      validation: '',
      color: 'black',
    };
  }

  /*************************REGISTER FORM SUBMISSION*********** */
  _onSubmit = () => {
    var reg = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    if (
      this.state.email == '' ||
      this.state.email == undefined ||
      this.state.email == null
    ) {
      this.setState({visible: true, validation: '*Email Field is required!'});
    } else if (reg.test(this.state.email) == false) {
      this.setState({visible: true, validation: '*Email is not valid!'});
    } else {
      this.setState({spinner: true});
      axios
        .post(
          'http://3.135.63.132:3000/api/v1/resetPassword',
          {
            email: this.state.email,
          },
          {
            headers: {
              'Content-Type': 'application/json',
              Accept: 'application/json',
            },
          },
        )
        .then(responseJson => {
          console.log('***************RESET PASSWORD**************');
          console.log(responseJson);

          if (responseJson.data.status == 0) {
            setTimeout(() => {
              this.setState({
                spinner: false,
                visible: true,
                validation: responseJson.data.msg,
                color: '#228B22',
              });
            }, 2000);
          } else if (responseJson.data.status == 1) {
            setTimeout(() => {
              this.setState({
                spinner: false,
                visible: true,
                validation: responseJson.data.msg,
                color: 'black',
              });
            }, 2000);
          }

          // if(JSON.stringify(responseJson.data.status)==1){
          //   this.setState({spinner : false});
          // }else if(JSON.stringify(responseJson.data.status)==0){
          //   this.setState({spinner : false});
          // }else if(JSON.stringify(responseJson.data.status)==2){
          //   this.setState({spinner : false});
          // }
        })
        .catch(error => {
          this.setState({spinner: false});
        });
    }
  };

  /************LOADER********* */

  renderLoader() {
    if (this.state.spinner) {
      return (
        <Spinner
          visible={this.state.spinner}
          size={'large'}
          textContent={'Loading...'}
          animation={'slide'}
          textStyle={styles.spinnerTextStyle}
        />
      );
    }
  }

  /*****************RENDER VIEW***************** */
  render() {
    return (
      <ScrollView contentContainerStyle={styles.contentContainer}>
        <View style={styles.container}>
          <Snackbar
            style={{backgroundColor: this.state.color}}
            visible={this.state.visible}
            onDismiss={() => this.setState({visible: false})}
            action={{
              onPress: () => {
                // Do something
              },
            }}>
            {this.state.validation}
          </Snackbar>
          <View style={styles.header}>
            <Image
              style={styles.hdr_img}
              source={require('../../assets/LOGO_Blk_.png')}
            />
          </View>
          <Text style={{marginTop: 30}} />
          <View style={styles.SectionStyle}>
            <Icon color="#fff" name="email" />
            <TextInput
              ref="email"
              style={styles.input}
              onChangeText={email => this.setState({email})}
              value={this.state.email}
              placeholder="Enter Email"
              underlineColorAndroid="transparent"
              placeholderTextColor="#fff"
            />
          </View>
          <View style={styles.Flexbox}>
            <TouchableOpacity onPress={this._onSubmit.bind(this)}>
              <Text style={styles.button}>SUBMIT</Text>
            </TouchableOpacity>
            {this.renderLoader()}
          </View>
          <View style={styles.boldLine} />
          <View>
            <TouchableHighlight>
              <TouchableOpacity onPress={() => Actions['Signin']()}>
                <Text
                  style={{
                    color: '#fff',
                    textDecorationLine: 'underline',
                    textAlign: 'right',
                    fontSize: 15,
                    marginTop: 5,
                  }}>
                  Login Now
                </Text>
              </TouchableOpacity>
            </TouchableHighlight>
          </View>
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  contentContainer: {
    paddingBottom: 20,
    width: '100%',
    backgroundColor: '#cf8cac',
    height: '100%',
  },
  container: {
    backgroundColor: '#cf8cac',
    display: 'flex',
    height: '100%',
    width: '100%',
    paddingLeft: 20,
    paddingRight: 20,
    paddingTop: 110,
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  SectionStyle: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 1,
    borderColor: '#d3d3d3',
    height: 50,
    marginTop: 10,
    width: '100%',
    borderRadius: 50,
    paddingLeft: 25,
  },
  Flexbox: {
    marginTop: 30,
    width: '100%',
  },
  button: {
    backgroundColor: '#fff',
    height: 50,
    flexDirection: 'row',
    fontSize: 16,
    color: '#cf8cac',
    textAlign: 'center',
    textTransform: 'uppercase',
    width: '100%',
    borderRadius: 50,
    paddingTop: 14,
  },
  input: {
    color: '#d3d3d3',
    width: '100%',
    paddingLeft: 5,
    fontSize: 14,
  },
  CheckBoxRow: {
    flexDirection: 'row',
    width: '100%',
    height: 30,
    marginTop: 10,
  },
  boldLine: {
    flexDirection: 'row',
    height: 6,
    width: 150,
    backgroundColor: '#d3d3d3',
    marginTop: 40,
    borderRadius: 5,
  },
  header: {
    position: 'relative',
    display: 'flex',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
  hdr_img: {
    width: 320,
    height: 91,
    marginBottom: 20,
    resizeMode: 'contain',
  },
});
export default Register;
