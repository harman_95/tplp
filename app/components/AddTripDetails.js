import React, {Component, useState} from 'react';
import {
  ImageBackground,
  Platform,
  AppRegistry,
  StyleSheet,
  Text,
  View,
  TextInput,
  Image,
  Button,
  TouchableHighlight,
  TouchableOpacity,
  ScrollView,
  AsyncStorage,
  FlatList,
} from 'react-native';
import {Icon, CheckBox, Input} from 'react-native-elements';
import {Actions} from 'react-native-router-flux';
import DatePicker from 'react-native-datepicker';
import {GooglePlacesAutocomplete} from 'react-native-google-places-autocomplete';
import {SafeAreaView} from 'react-native-safe-area-context';
import axios from 'axios';
import Spinner from 'react-native-loading-spinner-overlay';
import {Snackbar} from 'react-native-paper';

export default class AddTripDetails extends React.Component {
  constructor(props) {
    super(props);
    console.log(this.props);
    this.state = {
      email: '',
      airline: '',
      hotels: [
        {
          hotelName: '',
          lat: '',
          lng: '',
          addressLine1: '',
          addressLine2: '',
          addressLine3: '',
          checkIn: new Date(),
          checkOut: new Date(),
        },
      ],
      userid: '',
      startdate: '',
      endDate: '',
      spinner: false,
      visible: false,
      color: 'light green',
    };
  }

  async _setVal(type, add, index) {
    switch (type) {
      case 'checkin':
        this.state.hotels[index].checkIn = add;
        this.setState({
          hotels: this.state.hotels,
        });
        console.log(this.state.hotels);
        break;
      case 'checkout':
        this.state.hotels[index].checkOut = add;
        this.setState({
          hotels: this.state.hotels,
        });
        console.log(this.state.hotels);
        break;
      case 'add1':
        this.state.hotels[index].addressLine1 = add;
        this.setState({
          hotels: this.state.hotels,
        });
        console.log(this.state.hotels);
        break;
      case 'add2':
        this.state.hotels[index].addressLine2 = add;
        this.setState({
          hotels: this.state.hotels,
        });
        console.log(this.state.hotels);
        break;
      case 'add3':
        this.state.hotels[index].addressLine3 = add;
        this.setState({
          hotels: this.state.hotels,
        });
        console.log(this.state.hotels);
        break;
      case 'hotelname':
        this.state.hotels[index].hotelName = add.formatted_address;

        const api_call = await fetch(
          'https://maps.googleapis.com/maps/api/geocode/json?address=' +
            add.formatted_address +
            '&key=AIzaSyAUG9ZACOjwqlMYmEqlkXPYMz1wZ14H8zs',
        );
        const locationres = await api_call.json();

        this.state.hotels[index].lat =
          locationres.results[0].geometry.location.lat;
        this.state.hotels[index].lng =
          locationres.results[0].geometry.location.lng;

        this.setState({
          hotels: this.state.hotels,
        });
        console.log(this.state.hotels);

        break;
      default:
      // code block
    }
  }

  /************LOADER********* */

  renderLoader() {
    if (this.state.spinner) {
      return (
        <Spinner
          visible={this.state.spinner}
          size={'large'}
          textContent={'Loading...'}
          animation={'slide'}
          textStyle={styles.spinnerTextStyle}
        />
      );
    }
  }

  _onSubmit = () => {
    AsyncStorage.clear();
    Actions.Signin();
  };

  onChangeText(text) {
    console.log(text);
  }

  async _Done() {
    console.log(this.state);
    await AsyncStorage.getItem('@userLoggedInId').then(value => {
      console.log(value);
      this.setState({
        userid: value,
      });
    });

    if (this.state.hotels.length > 1) {
      this.setState({
        startdate: this.state.hotels[0].checkIn,
        enddate: this.state.hotels[this.state.hotels.length - 1].checkOut,
      });
    } else {
      this.setState({
        startdate: this.state.hotels[0].checkIn,
        enddate: this.state.hotels[0].checkOut,
      });
    }

    this.setState({
      spinner: true,
    });

    await axios
      .post('http://3.135.63.132:3000/api/v1/tplp/hotels/add_hotel', {
        cityid: this.props.cityId,
        emailadd: this.state.email,
        airline: this.state.airline,
        hotels: this.state.hotels,
        userid: this.state.userid,
        startdate: this.state.startdate,
        enddate: this.state.enddate,
      })
      .then(responseJson => {
        console.log(responseJson);
        this.setState({
          email: '',
          airline: '',
          hotels: [
            {
              hotelName: '',
              lat: '',
              lng: '',
              addressLine1: '',
              addressLine2: '',
              addressLine3: '',
              checkIn: new Date(),
              checkOut: new Date(),
            },
          ],
          spinner: false,
          visible: true,
        });

        Actions.purchasedItenary({
          cityId: this.props.cityId,
          cityLocations: this.props.cityLocations,
          cityName: this.props.cityName,
          cityWeather: this.props.cityWeather,
          temperature: this.props.temperature,
          citylat: this.props.citylat,
          citylng: this.props.citylng,
          cityNewArr: this.props.cityNewArr,
        });
      })
      .catch(error => {
        this.setState({spinner: false});
      });
  }

  _addHotel = () => {
    this.state.hotels.push({
      addressLine1: '',
      addressLine2: '',
      addressLine3: '',
      checkIn: '',
      checkOut: '',
    });
    this.setState({
      hotels: this.state.hotels,
    });
  };

  _renderHotels() {
    return this.state.hotels.map((item, index) => {
      return (
        <View>
          <View style={styles.fieldRow}>
            <Text style={styles.inputLabel}>Hotel {index + 1}:</Text>
            {/* <TextInput
                          style={styles.textField}
                          onChangeText={text => this.onChangeText(text)}
                          value={this.state.value}
                          placeholder="Hotel Name"
                        /> */}
            {/* <GooglePlacesAutocomplete
                          placeholder="Search"
                          minLength={2}
                          autoFocus={false}
                          returnKeyType={'search'}
                          keyboardAppearance={'light'}
                          listViewDisplayed={this.state.listViewDisplayed}    // true/false/undefined
                          fetchDetails={true}
                          renderDescription={row => row.description}
                          onPress={(data, details = null) => {
                            // 'details' is provided when fetchDetails = true
                            console.log(data, details);
                          }}
                          query={{
                            key: 'AIzaSyAUG9ZACOjwqlMYmEqlkXPYMz1wZ14H8zs',
                            language: 'en',
                          }}
                        /> */}

            <GooglePlacesAutocomplete
              placeholder="Search"
              minLength={2}
              autoFocus={false}
              returnKeyType={'search'}
              keyboardAppearance={'light'}
              fetchDetails={true}
              listViewDisplayed={false}
              renderDescription={row => row.description}
              onPress={(data, details = null) => {
                console.log(data);
                console.log(details);
                this._setVal('hotelname', details, index);
                // this.setState({search_location: data.description});
                // this.setState({listViewDisplayed: 'false'});
              }}
              getDefaultValue={() => ''}
              query={{
                key: 'AIzaSyAUG9ZACOjwqlMYmEqlkXPYMz1wZ14H8zs',
                language: 'en',
                // types: '(cities)' // default: 'geocode'
              }}
              styles={{
                textInputContainer: {
                  width: '100%',
                  backgroundColor: '#fff',
                },
                description: {
                  fontWeight: 'bold',
                },
                predefinedPlacesDescription: {
                  color: '#1faadb',
                },
              }}
              currentLocation={false}
              currentLocationLabel="Current location"
              nearbyPlacesAPI="GooglePlacesSearch"
              GoogleReverseGeocodingQuery={{}}
              GooglePlacesSearchQuery={{
                rankby: 'distance',
                type: 'cafe',
              }}
              GooglePlacesDetailsQuery={{
                fields: 'formatted_address',
              }}
              filterReverseGeocodingByTypes={[
                'locality',
                'administrative_area_level_3',
              ]}
              // predefinedPlaces={[homePlace, workPlace]}
              debounce={200}
            />
          </View>

          <View style={styles.fieldRow}>
            <Text style={styles.inputLabel}>Hotel Address:</Text>
            <TextInput
              style={styles.textareaField2}
              onChangeText={text => this._setVal('add1', text, index)}
              placeholder="Address Line 1"
            />
            <TextInput
              style={styles.textareaField2}
              onChangeText={text => this._setVal('add2', text, index)}
              placeholder="Address Line 2"
            />
            <TextInput
              style={styles.textareaField}
              onChangeText={text => this._setVal('add3', text, index)}
              placeholder="Address Line  3"
            />
          </View>
          <View style={styles.fieldRowFlex}>
            <View style={{flex: 1, marginRight: 5}}>
              <Text style={styles.inputLabel}>Check In:</Text>
              <View style={{flexDirection: 'row'}}>
                <View style={{flex: 1, marginRight: 5}}>
                  {/* <TextInput
                          style={styles.textareaField}
                          onChangeText={text => this.onChangeText(text)}
                          value={this.state.value}
                          multiline
                          placeholder="MM/DD/YYYY"
                        /> */}

                  <DatePicker
                    style={{width: '100%'}}
                    date={item.checkIn}
                    mode="date"
                    androidMode="spinner"
                    placeholder="select date"
                    format="DD-MMMM-YYYY"
                    minDate="01-01-1900"
                    maxDate="06-01-3000"
                    confirmBtnText="Confirm"
                    cancelBtnText="Cancel"
                    customStyles={{
                      dateIcon: {
                        position: 'absolute',
                        right: 0,
                        top: 4,
                        marginRight: 0,
                      },
                      dateInput: {
                        marginRight: 36,
                      },
                      // ... You can check the source to find the other keys.
                    }}
                    onDateChange={date => {
                      this._setVal('checkin', date, index);
                    }}
                  />
                </View>
              </View>
            </View>
          </View>
          <View style={styles.fieldRowFlex}>
            <View style={{flex: 1, marginRight: 5}}>
              <Text style={styles.inputLabel}>Check Out:</Text>
              <View style={{flexDirection: 'row'}}>
                <View style={{flex: 1, marginRight: 5}}>
                  <DatePicker
                    style={{width: '100%'}}
                    date={item.checkOut}
                    mode="date"
                    androidMode="spinner"
                    placeholder="select date"
                    format="DD-MMMM-YYYY"
                    minDate="01-01-1900"
                    maxDate="06-01-3000"
                    confirmBtnText="Confirm"
                    cancelBtnText="Cancel"
                    customStyles={{
                      dateIcon: {
                        position: 'absolute',
                        right: 0,
                        top: 4,
                        marginRight: 0,
                      },
                      dateInput: {
                        marginRight: 36,
                      },
                      // ... You can check the source to find the other keys.
                    }}
                    onDateChange={date => {
                      this._setVal('checkout', date, index);
                    }}
                  />
                </View>
              </View>
            </View>
          </View>
        </View>
      );
    });
  }

  render() {
    const {
      cityId,
      cityLocations,
      cityName,
      cityWeather,
      temperature,
      citylat,
      citylng,
      cityNewArr,
    } = this.props;
    return (
      <View style={styles.container}>
        {this.renderLoader()}

        <Snackbar
          style={{backgroundColor: this.state.color}}
          visible={this.state.visible}
          onDismiss={() => this.setState({visible: false})}
          action={{
            // label: 'Undo',
            onPress: () => {
              // Do something
            },
          }}>
          Hotel Added Successfully!
        </Snackbar>
        <View style={styles.header}>
          <Image
            style={styles.hdr_img}
            source={require('../../assets/LOGO_Blk_.png')}
          />
        </View>

        <ScrollView
          keyboardShouldPersistTaps="handled"
          style={{width: '100%', backgroundColor: '#fff'}}>
          <ImageBackground
            source={require('../../assets/overlaybg.png')}
            style={{width: '100%', height: '100%'}}>
            <View
              style={{
                justifyContent: 'center',
                position: 'relative',
                flexDirection: 'row',
                backgroundColor: '#cf8cac',
                paddingVertical: 5,
                paddingHorizontal: 20,
                alignItems: 'center',
              }}>
              <Text
                onPress={() => Actions['app1']()}
                style={{
                  fontFamily: 'GeorgiaPro-Bold',
                  color: '#fff',
                  fontSize: 15,
                  marginLeft: 70,
                }}>
                {this.props.cityName}
              </Text>
              <View
                style={{
                  marginLeft: 'auto',
                  flexDirection: 'row',
                  alignItems: 'center',
                }}>
                <View style={{alignItems: 'center', flexDirection: 'row'}}>
                  <Image
                    style={{width: 35, height: 35, resizeMode: 'contain'}}
                    source={{uri: this.props.cityWeather}}
                  />
                  <Text
                    style={{
                      fontFamily: 'BAHNSCHRIFT',
                      color: '#fff',
                      marginLeft: 5,
                    }}>
                    {this.props.temperature}&deg;C
                  </Text>
                </View>
                <View
                  style={{
                    alignItems: 'center',
                    flexDirection: 'row',
                    marginLeft: 15,
                  }}>
                  <Image
                    style={{width: 14, resizeMode: 'contain'}}
                    source={require('../../assets/dollar.png')}
                  />
                  <Text style={{fontFamily: 'BAHNSCHRIFT', color: '#fff'}}>
                    50
                  </Text>
                </View>
              </View>
            </View>
            <View style={styles.bannerArea}>
              <View style={styles.createPic}>
                <Image
                  style={styles.createPicImg}
                  source={require('../../assets/profile3.jpg')}
                />
                <Image
                  style={styles.flowerImg}
                  source={require('../../assets/pic3.png')}
                />
              </View>
              <View style={styles.formFieldArea}>
                <View style={styles.fieldRow}>
                  <Text style={styles.inputLabel}>Email Address:</Text>
                  <TextInput
                    ref="email"
                    style={styles.textField}
                    onChangeText={email => this.setState({email})}
                    value={this.state.email}
                    placeholder="Email Address"
                  />
                </View>
                <View style={styles.fieldRow}>
                  <Text style={styles.inputLabel}>Airline:</Text>
                  <TextInput
                    style={styles.textField}
                    ref="airline"
                    onChangeText={airline => this.setState({airline})}
                    value={this.state.airline}
                    placeholder="Airline"
                  />
                </View>

                {this._renderHotels()}

                {this.state.hotels.length < 4 ? (
                  <View style={styles.fieldRowFlex}>
                    <TouchableOpacity
                      style={styles.btnAddHotel}
                      onPress={() => this._addHotel()}>
                      <Icon
                        name="ios-add"
                        size={25}
                        type="ionicon"
                        color="#fff"
                      />
                      <Text
                        style={{
                          marginLeft: 5,
                          color: '#fff',
                          fontFamily: 'BAHNSCHRIFT',
                          fontSize: 15,
                        }}>
                        Add Hotel
                      </Text>
                    </TouchableOpacity>
                  </View>
                ) : null}

                <View style={styles.fieldRowFlex}>
                  <View style={{flex: 1, marginRight: 5}}>
                    <TouchableOpacity style={styles.btnOtline}>
                      <Icon
                        name="ios-alarm"
                        size={18}
                        type="ionicon"
                        color="#cf8cac"
                      />
                      <Text onPress={()=>{
                        AsyncStorage.setItem('@remindMe', 'true');
                      }}
                        style={{
                          marginLeft: 5,
                          color: '#cf8cac',
                          fontFamily: 'BAHNSCHRIFT',
                          fontSize: 13,
                        }}>
                        Remind Me Later
                      </Text>
                    </TouchableOpacity>
                  </View>
                  <View style={{flex: 1, marginLeft: 5}}>
                    <TouchableOpacity
                      style={styles.btnOtline}
                      onPress={() => this._Done()}>
                      <Icon
                        name="ios-checkmark"
                        size={30}
                        type="ionicon"
                        color="#cf8cac"
                      />
                      <Text
                        style={{
                          marginLeft: 5,
                          color: '#cf8cac',
                          fontFamily: 'BAHNSCHRIFT',
                          fontSize: 13,
                        }}>
                        Done
                      </Text>
                    </TouchableOpacity>
                  </View>
                </View>
              </View>
            </View>
          </ImageBackground>
        </ScrollView>

        <View style={styles.footMain}>
          <View style={styles.footFlex}>
            <TouchableOpacity onPress={() => Actions.exploreRegion()}>
              <Icon name="ios-planet" size={35} type="ionicon" color="#fff" />
            </TouchableOpacity>
          </View>

          <View style={styles.footFlex}>
            <TouchableOpacity onPress={() => Actions.Profile()}>
              <Icon size={30} name="ios-person" type="ionicon" color="#fff" />
            </TouchableOpacity>
          </View>

          <View style={styles.footFlex}>
            <TouchableOpacity style={styles.centerMenu}>
              <Image
                style={{
                  marginTop: 20,
                  width: 40,
                  height: 40,
                  resizeMode: 'contain',
                }}
                source={require('../../assets/heartlock.png')}
              />
            </TouchableOpacity>
          </View>

          <View style={styles.footFlex}>
            <TouchableOpacity onPress={() => Actions.home()}>
              <Icon size={27} name="ios-home" type="ionicon" color="#fff" />
            </TouchableOpacity>
          </View>

          <View style={styles.footFlex}>
            <TouchableOpacity onPress={() => Actions.Profile()}>
              <Icon size={27} name="ios-search" type="ionicon" color="#fff" />
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  fieldRowFlex: {
    flexDirection: 'row',
    marginBottom: 15,
  },
  fieldRow: {
    marginBottom: 15,
  },
  inputLabel: {
    fontFamily: 'BAHNSCHRIFT',
    marginBottom: 3,
    color: '#cf8cac',
  },
  textareaField2: {
    borderColor: '#ddd',
    borderWidth: 1,
    borderRadius: 3,
    paddingHorizontal: 10,
    minHeight: 40,
    // width: '100%',
    marginBottom: 10,
    backgroundColor: '#fff',
    fontFamily: 'BAHNSCHRIFT',
  },
  textareaField: {
    borderColor: '#ddd',
    borderWidth: 1,
    borderRadius: 3,
    paddingHorizontal: 10,
    minHeight: 40,
    width: '100%',
    backgroundColor: '#fff',
    fontFamily: 'BAHNSCHRIFT',
  },
  textField: {
    borderColor: '#ddd',
    borderWidth: 1,
    borderRadius: 3,
    paddingHorizontal: 10,
    height: 40,
    width: '100%',
    backgroundColor: '#fff',
    fontFamily: 'BAHNSCHRIFT',
  },
  formFieldArea: {
    minHeight: 400,
    width: '100%',
    padding: 20,
    paddingTop: 30,
  },
  flowerImg: {
    width: 40,
    height: 40,
    position: 'absolute',
    resizeMode: 'contain',
    right: -15,
    bottom: -15,
  },
  eyeImg: {
    width: 20,
    resizeMode: 'contain',
  },
  fullWidth: {
    width: '100%',
    position: 'relative',
  },
  createPic: {
    position: 'absolute',
    top: -48,
    left: 0,
    zIndex: 99,
    backgroundColor: '#cf8cac',
    alignItems: 'center',
    justifyContent: 'center',
    borderTopRightRadius: 100,
    borderBottomLeftRadius: 100,
    borderBottomRightRadius: 100,
    width: 60,
    height: 60,
    borderWidth: 1,
    borderColor: '#fff',
    shadowColor: '#000',
    shadowOffset: {width: 10, height: 2},
    shadowOpacity: 0.8,
    shadowRadius: 2,
  },
  createPicImg: {
    width: 50,
    height: 50,
    resizeMode: 'cover',
    borderTopRightRadius: 100,
    borderBottomRightRadius: 100,
    borderBottomLeftRadius: 100,
  },
  logout: {
    position: 'absolute',
    right: 20,
    top: 15,
  },
  favorite: {
    position: 'absolute',
    right: 5,
    bottom: 5,
  },
  letsEdit: {
    position: 'absolute',
    right: 10,
    bottom: 0,
    top: 0,
    zIndex: 0,
    opacity: 0.5,
    width: 140,
    resizeMode: 'contain',
  },
  gridTitle: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 35,
    margin: 'auto',
    fontSize: 12,
    textAlign: 'center',
    color: '#cf8cac',
    paddingVertical: 5,
    backgroundColor: 'rgba(255,255,255, .5)',
    elevation: 1,
    marginVertical: 10,
    fontFamily: 'metropolis-bold',
  },
  gridImg: {
    width: '100%',
    height: 120,
    resizeMode: 'cover',
    opacity: 0.8,
    borderRadius: 5,
    borderWidth: 1,
    borderColor: '#e9f0f6',
  },
  flexGrid: {
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 0,
    width: '33.3%',
    borderRadius: 5,
    backgroundColor: '#000',
  },
  weKnow: {
    color: '#cf8cac',
    fontSize: 16,
    textAlign: 'center',
    lineHeight: 22,
    marginTop: 20,
    marginBottom: 20,
    paddingVertical: 10,
    paddingHorizontal: 40,
    fontWeight: 'bold',
    backgroundColor: '#f0e0e2',
    borderRadius: 100,
  },
  absoluTxt: {
    position: 'absolute',
    bottom: 20,
    left: 20,
    top: 20,
    backgroundColor: 'rgba(255,255,255, .7)',
    right: 20,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    paddingBottom: 10,
    borderRadius: 5,
    elevation: 1,
  },
  bannerArea: {
    alignItems: 'center',
    justifyContent: 'center',
    position: 'relative',
  },
  bannerImg: {
    height: 400,
    width: '100%',
    resizeMode: 'cover',
  },
  contentArea: {
    paddingHorizontal: 20,
    paddingBottom: 20,
  },
  footMain: {
    backgroundColor: '#cf8cac',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    paddingTop: 10,
    paddingBottom: 10,
    maxHeight: 55,
  },
  centerMenu: {
    backgroundColor: '#cf8cac',
    position: 'relative',
    top: -15,
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    borderTopLeftRadius: 50,
    borderTopRightRadius: 50,
  },
  footFlex: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-start',
    height: '100%',
    paddingTop: 30,
  },
  header: {
    position: 'relative',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    elevation: 1,
    // backgroundColor: '#f0e0e2',
    width: '100%',
    paddingTop: 6,
    paddingBottom: 6,
  },
  hdr_img: {
    width: 200,
    height: 50,
    marginBottom: 5,
    resizeMode: 'stretch',
  },
  btnAddHotel: {
    paddingVertical: 12,
    backgroundColor: '#cf8cac',
    textAlign: 'center',
    borderRadius: 30,
    fontSize: 14,
    marginBottom: 15,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    flex: 1,
    marginTop: 20,
  },
  btnAddHotel: {
    paddingVertical: 10,
    backgroundColor: '#cf8cac',
    textAlign: 'center',
    borderRadius: 30,
    fontSize: 14,
    marginBottom: 15,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    flex: 1,
    marginTop: 20,
  },
  btnOtline: {
    paddingVertical: 8,
    textAlign: 'center',
    borderRadius: 30,
    fontSize: 14,
    marginBottom: 15,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    flex: 1,
    marginTop: 0,
    borderColor: '#cf8cac',
    borderWidth: 1,
  },
});
