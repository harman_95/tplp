import React,{Component} from 'react';
import { StyleSheet, Text,View,AsyncStorage} from 'react-native';
import {TextInput,  Button} from 'react-native-paper';
import axios from 'axios';
//import AsyncStorage from '@react-native-community/async-storage';


export default class SignIn extends React.Component{
    constructor(props){
        super(props);
        this.state = {            
            email:'',           
            password:'',
            userEmail:'',
            data:[]
        };
         AsyncStorage.setItem('useremail', 'test');
         AsyncStorage.getItem('useremail').then(value => {
             console.log(value);
         })
    }
    

    async handleFormSubmit(){       
        
        try{
             await axios.post('http://10.0.2.2/earn/Api/login.php', {
                email: this.state.email,
                password: this.state.password
            })
            .then((response) => {
                //console.log(response.data.body);
                this.setState({
                    data:response.data.body
                })
            })             
            }catch(error){
            console.error(error);
        } ;  
        this.setState({
            userEmail:JSON.stringify(this.state.data.userEmail)
        })
        console.log(JSON.parse(this.state.userEmail));
        const mail = JSON.parse(this.state.userEmail);
        this.storeData(mail);
        this._displayData;
       
    }

     storeData = async (value) => {
         console.log('call');
        try {
          await AsyncStorage.setItem('useremail', value)
        } catch (e) {
            console.error(error);
        }
      }
    

        _displayData = async ()=>{  
        try{  
            //AsyncStorage.getItem('useremail').then((value) => console.log(value ));

            const useremail = await AsyncStorage.getItem('useremail');   
            if (useremail !== null) {
                // We have data!!
                console.log(' We have data!!');
                console.log(useremail);
             }          
        }  
        catch(error){  
            console.log(error)  
        }  
      }  

    render(){
        return(
            <View style={styles.container}>               

                <TextInput
                    style={styles.inputtxt}
                    theme={theme}
                    mode='outlined'
                    label="Email"
                    value={this.state.email}
                    onChangeText={text => this.setState({email:text})}
                />              

                <TextInput
                    style={styles.inputtxt}
                    theme={theme}
                    mode='outlined'
                    label="Password"
                    value={this.state.password}
                    onChangeText={text => this.setState({password:text})}
                    secureTextEntry={true}
                    password={true}
                />

                <Button
                    style={{margin:20}}
                    mode="contained" onPress={this.handleFormSubmit.bind(this)}
                    style={{backgroundColor:'#f86262', margin:3, marginTop:25, color:'#fff'}}                   
                    >
                        <Text style={{fontWeight:'bold',fontSize:25, color:'#fff', textTransform:'none',fontFamily: 'sans-serif-condensed'}}>Sign In</Text>
                </Button>

                

                  

                     
            </View>
        )
    }
}

const theme = {
    dark:true,
    fonts:{
        medium: 'sans-serif-condensed' ,
    },
    colors: {    
      primary: '#f86262',
      accent: '#47cf73',
      placeholder:'#fff',
      text:'#fff',
    },
}

const styles = StyleSheet.create({
    container:{
        flex:1,
        flexDirection:'column',
        fontFamily: 'sans-serif-condensed',
        justifyContent: 'center',
        padding:10,
         backgroundColor:'#000',
        
    },
    hrLine:{
        height: 5,
        borderRadius: 3,
        backgroundColor: '#444857',
    },
    inputtxt:{
        backgroundColor:'#1f2229',
        margin:4,
        fontSize:18,
    }
    
});

