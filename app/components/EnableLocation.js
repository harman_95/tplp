import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  Alert,
  TouchableOpacity,
  PermissionsAndroid,
} from 'react-native';
import {Actions} from 'react-native-router-flux';

export default class EnableLocation extends React.Component {
  constructor(props) {
    super(props);
    this.state = {locationStatus: null};
    this.alertIfRemoteNotificationsDisabledAsync();
  }

  /*********CHECK FOR PERMISSION ******** */
  async alertIfRemoteNotificationsDisabledAsync() {
    try {
      const granted = await PermissionsAndroid.check(
        PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
        {},
      );
      if (granted === true) {
        this.setState({locationStatus: 'Granted'});
      } else {
        this.setState({locationStatus: 'Not Granted'});
      }
    } catch (err) {
      console.warn(err);
    }
  }

  /***********ENABLE LOCATION ************* */
  _enableLocation = async () => {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
        {},
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        // Alert.alert('Location Permission Granted.');
        Actions.home();
      } else {
        // Alert.alert('Location Permission Not Granted');
      }
    } catch (err) {
      console.warn(err);
    }
  };

  /******************RENDER VIEW***************** */
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.header}>
          <Image
            style={styles.hdr_img}
            source={require('../../assets/LOGO_Blk_.png')}
          />
        </View>
        <View style={styles.body}>
          {/* <Image
            style={styles.body_img}
            source={require('../../assets/pic1.png')}
          /> */}
           <Image
            style={styles.body_img}
            source={require('../../assets/tag-line.gif')}
          />
        </View>

        <View style={styles.footer}>
          <View style={styles.ftr_inr}>
            <View style={styles.footer_hdr}>
              <Text style={styles.ftr_hdngs}>
                Enable Location {'\n'}
                Services
              </Text>
              <Text style={styles.ftr_des}>for better user experience</Text>
            </View>

            <View style={{display: 'flex', flexDirection: 'row'}}>
              <Image
                style={styles.flowerpic3}
                source={require('../../assets/pic2.png')}
              />
              <TouchableOpacity
                style={styles.instagram}
                onPress={this._enableLocation.bind(this)}>
                <View style={styles.inst_box}>
                  <Text
                    style={{
                      fontFamily: 'BAHNSCHRIFT',
                      color: '#fff',
                      fontSize: 15,
                      marginLeft: 10,
                    }}>
                    Enable Location Services
                  </Text>
                </View>
              </TouchableOpacity>
            </View>
          </View>
        </View>

        <View
          style={{
            flex: 1,
            justifyContent: 'flex-end',
            alignItems: 'flex-end',
            position: 'relative',
          }}>
          <Image
            style={styles.flowerpic4}
            source={require('../../assets/pic4.png')}
          />
          <TouchableOpacity
            style={styles.btnEmail}
            onPress={() => Actions.home()}>
            <View style={styles.inst_box}>
              <Text
                style={{
                  fontFamily: 'BAHNSCHRIFT',
                  color: '#cf8cac',
                  fontSize: 14,
                }}>
                Nah Get Lost
              </Text>
            </View>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  flowerpic3: {
    position: 'absolute',
    left: -5,
    top: -2,
    width: 70,
    height: 70,
    resizeMode: 'contain',
    zIndex: 999,
  },
  flowerpic4: {
    position: 'absolute',
    right: 10,
    bottom: 0,
    width: 50,
    height: 50,
    resizeMode: 'contain',
    zIndex: 999,
  },
  container: {
    flex: 1,
    justifyContent: 'flex-start',
    height: '100%',
    paddingTop: 50,
  },
  header: {
    position: 'relative',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  hdr_img: {
    width: 320,
    height: 91,
    marginBottom: 20,
    resizeMode: 'contain',
  },
  body: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  body_img: {
    alignItems: 'center',
    width: '90%',
    height: 160,
    resizeMode: 'contain',
  },
  footer: {
    width: '100%',
    marginTop: 10,
    paddingHorizontal: 20,
  },
  ftr_inr: {
    paddingHorizontal: 10,
    paddingVertical: 20,
    width: '100%',
    borderWidth: 1,
    borderColor: '#cf8cac',
    borderRadius: 10,
    backgroundColor: '#fcfcfc',
  },
  footer_hdr: {
    width: '100%',
    textAlign: 'center',
    paddingHorizontal: 7,
    paddingVertical: 8,
    marginBottom: 10,
    color: '#111',
    fontSize: 16,
  },
  ftr_hdngs: {
    textAlign: 'center',
    color: '#111',
    fontSize: 18,
    fontWeight: 'bold',
    marginTop: 15,
    // fontFamily: 'GeorgiaPro-Bold',
    fontFamily: 'BAHNSCHRIFT',
    textTransform: 'uppercase',
  },
  ftr_des: {
    textAlign: 'center',
    color: '#111',
    fontSize: 15,
    marginTop: 5,
    fontFamily: 'BAHNSCHRIFT',
    // fontFamily: 'GeorgiaPro-Bold',
  },
  inst_box: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
  },
  instagram: {
    backgroundColor: '#cf8cac',
    paddingVertical: 13,
    borderRadius: 30,
    borderWidth: 10,
    borderColor: '#f0e0e2',
    paddingBottom: 15,
    marginHorizontal: 20,
    marginBottom: 15,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    flex: 1,
    marginTop: 2,
  },
  btnEmail: {
    borderColor: '#e7c7d6',
    borderWidth: 1,
    paddingVertical: 13,
    borderRadius: 30,
    paddingBottom: 15,
    marginHorizontal: 20,
    paddingHorizontal: 20,
    marginBottom: 15,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    maxHeight: 40,
  },
  closeModal: {
    position: 'absolute',
    top: -20,
    right: 15,
    borderRadius: 50,
    borderWidth: 3,
    borderColor: '#fff',
    width: 40,
    backgroundColor: '#cf8cac',
    height: 40,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
