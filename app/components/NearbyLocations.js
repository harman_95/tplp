/* eslint-disable react-native/no-inline-styles */
/* eslint-disable prettier/prettier */
import React, {Component, useState} from 'react';
import {
  ImageBackground,
  Modal,
  Alert,
  Platform,
  AppRegistry,
  StyleSheet,
  Text,
  View,
  TextInput,
  Image,
  Button,
  AsyncStorage,
  TouchableHighlight,
  TouchableOpacity,
  ScrollView,
  FlatList,
} from 'react-native';
import {Icon, CheckBox} from 'react-native-elements';
import {Actions} from 'react-native-router-flux';
import axios from 'axios';
import * as geolib from 'geolib';
import MapView, {AnimatedRegion, Animated} from 'react-native-maps';
import Geolocation from '@react-native-community/geolocation';
import StarRating from 'react-native-star-rating';
import {Callout} from 'react-native-maps';
import {Marker} from 'react-native-maps';
export default class NearbyLocations extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      modalVisible: false,
      modalVisible2: false,
      modalVisible3: false,
      tempCounter: 0,
      _selectedLocations: '',
      _citiesWithinMiles: [],
      num: 0,
      initialPosition: {},
      markers: [],
      showLocationPopup: '',
      user_reviews: [],
      users_ratings: null,
      location_name: '',
      location_image: '',
      _showReviews: false,
      showCallout: false,
      popwindowname: '',
    };
  }
  /********ON VIEW LOAD****** */
  componentDidMount() {
    Geolocation.getCurrentPosition(
      position => {
        console.log('position');
        console.log(position);
        var coords = {
          latitude: position.coords.latitude,
          longitude: position.coords.longitude,
        };
        this.setState({
          initialPosition: coords,
        });
      },
      error => console.log('Error', JSON.stringify(error)),
      //   {enableHighAccuracy: true, timeout: 20000, maximumAge: 1000},
    );
    this._getHeartedPhotoLocations();
  }
 
  /*********LOGOUT*********** */
  _onSubmit = () => {
    AsyncStorage.clear();
    Actions.Signin();
  };

  /*************GET USER SELECTED LOCATIONS*********** */
  async _getHeartedPhotoLocations() {
    try {
      var arr = [];
      var markersArray = [];
      await this.props.cityLocations.forEach(loc => {
        if (loc.hearted == 'true') {
          arr.push(loc);
        }
      });
      var interval = await setInterval(() => {
        if (arr.length != 0) {
          arr.forEach(async (nearlocations, index) => {
            const arrindex = index;
            console.log(arr.length - 1);
            const api_call = await fetch(
              'https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=' +
                '25.2048' +
                ',' +
                '55.2708' +
                '+&radius=3000&type=point_of_interest&keyword=' +
                nearlocations.address +
                '&key=AIzaSyAUG9ZACOjwqlMYmEqlkXPYMz1wZ14H8zs',
            );
            const locationres = await api_call.json();
            if (locationres.status == 'OK') {
              console.log('OK');
              if (this.state.markers.length >= 5) {
                console.log('Greater and eq');
                console.log(this.state.markers);
              } else {
                markersArray.push(nearlocations);
                this.setState({
                  markers: markersArray,
                });
              }
            }
            if (
              arrindex === arr.length - 1 &&
              this.state.markers.length === 0
            ) {
              console.log(' 2nd ok ');
              this.setState({
                modalVisible2: true,
              });
            }
          });
          clearInterval(interval);
        }
      });
    } catch {
      Alert.alert('Server Error');
    }
  }

  /*************TRIP DETAIL*************** */
  _showTripDetailPage() {
    this.setState({modalVisible2: true});
    setTimeout(() => {
      this.setState({modalVisible2: false});
      Actions.AddTripDetails({
        cityId: this.props.cityId,
        cityLocations: this.props.cityLocations,
        cityName: this.props.cityName,
        cityWeather: this.props.cityIcon,
        temperature: this.props.temperature,
        citylat: this.props.citylat,
        citylng: this.props.citylng,
        cityNewArr: this.props.cityNewArr,
      });
    }, 3000);
  }

  /***********RENDER RATING REVIEWS******** */
  _renderReviews() {
    return this.state.user_reviews.map(comment => {
      return (
        <View style={styles.comntList}>
          <View style={styles.comntCols}>
            <Text style={styles.UserName}>{comment.author_name}</Text>
            <Text style={styles.ComntTxt}>{comment.text}</Text>
          </View>
          <View style={styles.comntCols2}>
            <View style={styles.ratingUser}>
              <StarRating
                disabled={false}
                maxStars={5}
                starSize={16}
                fullStarColor="#cf8cac"
                rating={comment.rating}
              />
            </View>
          </View>
        </View>
      );
    });
  }
  render() {
    const {
      cityId,
      cityLocations,
      cityIcon,
      cityName,
      cityWeather,
      temperature,
      citylat,
      citylng,
      cityNewArr,
      initialPosition,
    } = this.props;
    return (
      <View style={styles.container}>
        <View style={styles.header}>
          <Image
            style={styles.hdr_img}
            source={require('../../assets/LOGO_Blk_.png')}
          />
        </View>
        <ScrollView style={{width: '100%', backgroundColor: '#fff'}}>
          <View
            style={{
              justifyContent: 'center',
              position: 'relative',
              flexDirection: 'row',
              backgroundColor: '#cf8cac',
              paddingVertical: 5,
              paddingHorizontal: 20,
              alignItems: 'center',
            }}>
            <Text
              onPress={() => Actions.individualCity({
                cityTitle: this.props.cityName, 
                cityId: this.props.cityId, 
                cityLocations: this.props.cityLocations, 
                citylat: this.props.citylat, 
                citylng: this.props.citylng,
              })}
              style={{
                fontFamily: 'GeorgiaPro-Bold',
                color: '#fff',
                fontSize: 15,
                marginLeft: 80,
              }}>
              {this.props.cityName}
            </Text>
            <View
              style={{
                marginLeft: 'auto',
                flexDirection: 'row',
                alignItems: 'center',
              }}>
              <View style={{alignItems: 'center', flexDirection: 'row'}}>
                <Image
                  style={{width: 35, height: 35, resizeMode: 'contain'}}
                  source={{uri: this.props.cityIcon}}
                />
                <Text
                  style={{
                    fontFamily: 'BAHNSCHRIFT',
                    color: '#fff',
                    marginLeft: 5,
                  }}>
                  {this.props.temperature}&deg;C
                </Text>
              </View>
              <View
                style={{
                  alignItems: 'center',
                  flexDirection: 'row',
                  marginLeft: 15,
                }}>
                <Image
                  style={{width: 14, resizeMode: 'contain'}}
                  source={require('../../assets/dollar.png')}
                />
                <Text style={{fontFamily: 'BAHNSCHRIFT', color: '#fff'}}>
                  50
                </Text>
              </View>
            </View>
          </View>
          <View style={styles.bannerArea}>
            <MapView
              style={styles.bannerImg}
              zoomEnabled={true}
              scrollEnabled={true}
              showsScale={true}
              initialRegion={{
                latitude: 25.2048,
                longitude: 55.2708,
                latitudeDelta: 0.00922 * 2.5, //                        latitudeDelta:  0.00922 * 2.5,
                longitudeDelta: 0.00922 * 2.5,
              }}>
              <Marker
                coordinate={{
                  latitude: 25.2048,
                  longitude: 55.2708,
                }}>
                <View>
                  <Image
                    source={require('../../assets/profile3.jpg')}
                    style={{
                      height: 30,
                      width: 30,
                      borderTopLeftRadius: 50,
                      borderTopRightRadius: 50,
                      backgroundColor: '#FFBF00',
                      borderBottomLeftRadius: 50,
                      borderBottomRightRadius: 0,
                      borderWidth: 5,
                      borderColor: '#000000',
                    }}
                  />
                </View>
              </Marker>
              {this.state.markers.length > 0
                ? this.state.markers.map((marker, index) => (
                    <Marker
                      key={index}
                      coordinate={{
                        latitude: marker.lat,
                        longitude: marker.long,
                      }}
                      title={marker.name}
                      image={require('../../assets/rose.png')}>
                      <Callout style={{backgroundColor: '#f0e0e2'}}>
                        <View
                          style={{
                            backgroundColor: '#f0e0e2',
                            height: 200,
                            width: '100%',
                          }}>
                          {/* <View
                            style={{
                              borderRadius: 10,
                              alignItems: 'center',
                            }}> */}
                            <Text>
                              <Image
                                style={{
                                  width: 100,
                                  height: 140,
                                  resizeMode: 'contain',
                                  // marginTop: -60,
                                }}
                                source={{
                                  uri:
                                    'http://3.135.63.132:3000/images/' +
                                    marker.image,
                                }}
                              />
                            </Text>
                            <View
                              style={{
                                flexDirection: 'row',
                                alignItems: 'center',
                                width: '100%',
                              }}>
                              <View style={styles.ratingMainpop}>
                                <StarRating
                                  disabled={false}
                                  maxStars={5}
                                  starSize={15}
                                  fullStarColor="#cf8cac"
                                  rating={marker.rating}
                                />
                              </View>
                            </View>
                            <View style={styles.btnsecretpop}>
                              <Text
                                style={{
                                  fontSize: 8,
                                  alignItems: 'center',
                                  justifyContent: 'center',
                                  color: '#cf8cac',
                                  fontFamily: 'BAHNSCHRIFT',
                                }}>
                                {marker.name}
                              </Text>
                              {/* <Image
                                    style={styles.flowerImgpopup}
                                    source={require('../../assets/pic3.png')}
                                  /> */}
                            </View>
                          {/* </View> */}
                          <View
                            style={{
                              padding: 15,
                              backgroundColor: '#f0e0e2',
                              borderWidth: 1,
                              borderColor: '#e7c7d6',
                              borderRadius: 10,
                              flex: 1,
                              justifyContent: 'flex-end',
                              alignItems: 'flex-end',
                            }}>
                            <TouchableOpacity
                              style={styles.touchcss}
                              onPress={() => {
                                this.setState({_showReviews: true});
                              }}>
                              <View
                                style={{
                                  width: '100%',
                                  flexDirection: 'row',
                                  alignItems: 'center',
                                  minHeight: 10
                                }}>
                                {/* <View> */}
                                  <Text
                                    style={{
                                      fontSize: 10,
                                      color: '#cf8cac',
                                      fontFamily: 'GeorgiaPro-Bold',
                                      textTransform: 'uppercase',
                                    }}>
                                    Comments
                                  </Text>
                                {/* </View> */}
                                <View style={{marginLeft: 'auto'}}>
                                  <Icon
                                    name="angle-down"
                                    size={15}
                                    type="font-awesome"
                                    color="#cf8cac"
                                  />
                                </View>
                              </View>
                              {/* {this.state._showReviews == true
                                ? this._renderReviews()
                                : null} */}
                            </TouchableOpacity>
                          </View>
                        </View>
                      </Callout>
                    </Marker>
                  ))
                : null}
            </MapView>
            <View style={styles.createPic}>
              <Image
                style={styles.createPicImg}
                source={require('../../assets/profile3.jpg')}
              />
              <Image
                style={styles.flowerImg}
                source={require('../../assets/pic3.png')}
              />
            </View>
            <View style={styles.dateTravel}>
              <Icon name="ios-calendar" size={17} type="ionicon" color="#111" />
              <Text
                style={{
                  marginLeft: 5,
                  fontFamily: 'BAHNSCHRIFT',
                  color: '#111',
                  fontSize: 12,
                }}>
                Jan 5 - Jan 11
              </Text>
            </View>
          </View>
          <View style={styles.btnGroup}>
            <View style={styles.btnFlex}>
              <TouchableOpacity style={styles.btnEmail}>
                <View style={{flexDirection: 'row'}}>
                  <Icon
                    name="map-marker"
                    size={15}
                    type="font-awesome"
                    color="#cf8cac"
                  />
                  <Text
                    style={{
                      color: '#cf8cac',
                      fontFamily: 'BAHNSCHRIFT',
                      marginLeft: 5,
                    }}>
                    Nearby
                  </Text>
                </View>
              </TouchableOpacity>
            </View>
          </View>
          {this.state.markers.length > 0 ? (
            <FlatList
              data={this.state.markers}
              renderItem={({item}) => (
                <View style={styles.flexGrid}>
                  <TouchableOpacity
                    style={styles.touchcss}
                    onPress={() => {
                      this.setState({
                        showLocationPopup: true,
                        user_reviews: item.reviews,
                        users_ratings: Number(item.rating),
                        location_name: item.name,
                        location_image:
                          'http://3.135.63.132:3000/images/' + item.image,
                      });
                    }}>
                    <Image
                      style={styles.gridImg}
                      source={{
                        uri: 'http://3.135.63.132:3000/images/' + item.image,
                      }}
                    />
                    <Text style={styles.gridTitle}>{item.name}</Text>
                  </TouchableOpacity>
                </View>
              )}
              //Setting the number of column
              numColumns={3}
              keyExtractor={(item, index) => index.toString()}
            />
          ) : (
            <View style={styles.fullWidth}>
              <Text style={styles.notFound}>Aww shucks we can’t find anything near you!</Text>
            </View>
          )}
          {/* Expand Popup 2 */}
          <View style={{alignItems: 'center', justifyContent: 'center'}}>
            <Modal
              style={{
                alignItems: 'center',
                justifyContent: 'center',
                height: '100%',
              }}
              animationType="fade"
              transparent={true}
              visible={this.state.modalVisible2}
              onRequestClose={() => {
              }}>
              <ImageBackground
                source={require('../../assets/overlaybg.png')}
                style={{width: '100%', height: '100%'}}>
                <View
                  style={{
                    backgroundColor: 'rgba(255,255,255,.4)',
                    marginTop: 0,
                    height: 900,
                    paddingHorizontal: 20,
                    paddingVertical: 20,
                    marginTop: 0,
                    width: '100%',
                  }}>
                  {/* <TouchableOpacity
                    style={styles.btnClose}
                    onPress={() => {
                      this.setState({modalVisible2: false});
                    }}>
                    <View>
                      <Icon
                        name="ios-close"
                        size={30}
                        type="ionicon"
                        color="#fff"
                      />
                    </View>
                  </TouchableOpacity> */}
                  <View
                    style={{
                      backgroundColor: '#f0e0e2',
                      borderRadius: 10,
                      marginTop: 160,
                      paddingHorizontal: 20,
                      paddingTop: 40,
                      paddingBottom: 30,
                      borderWidth: 1,
                      borderColor: '#cf8cac',
                    }}>
                    <Image
                      style={styles.flowerImg}
                      source={require('../../assets/pic3.png')}
                    />
                    <Image
                      style={styles.flowerImg2}
                      source={require('../../assets/pic3.png')}
                    />
                    <View style={styles.fullWidth}>
                      <Text
                        style={{
                          fontFamily: 'BAHNSCHRIFT',
                          color: '#cf8cac',
                          fontSize: 22,
                          textAlign: 'center',
                          marginBottom: 10,
                          lineHeight: 30,
                        }}>
                        aww shucks we can’t find anything near you.
                      </Text>
                    </View>
                  </View>
                  <TouchableOpacity
                    style={styles.btnRemindme}
                    onPress={() => {
                      this.setState({
                        modalVisible2: false,
                      });
                    }}>
                    <Text
                      style={{
                        fontSize: 15,
                        color: '#cf8cac',
                        fontFamily: 'BAHNSCHRIFT',
                      }}>
                      Close
                    </Text>
                  </TouchableOpacity>
                </View>
              </ImageBackground>
            </Modal>
            <Modal
              style={{
                alignItems: 'center',
                justifyContent: 'center',
                height: '100%',
              }}
              animationType="fade"
              transparent={true}
              visible={this.state.showLocationPopup}
              onRequestClose={() => {
                Alert.alert('Modal has been closed.');
              }}>
              <ImageBackground style={{width: '100%', height: '100%'}}>
                <View
                  style={
                      this.state._showReviews === true
                        ? {
                            backgroundColor: '#e6c4d4',
                            height: 600,
                            paddingLeft: 20,
                            paddingVertical: 20,
                            marginTop: 90,
                            alignItems: 'center',
                            justifyContent: 'center',
                            width: '100%',
                          }
                        : {
                            backgroundColor: '#e6c4d4',
                            height: 420,
                            paddingLeft: 20,
                            paddingVertical: 20,
                            marginTop: 90,
                            alignItems: 'center',
                            justifyContent: 'center',
                            width: '100%',
                          }
                    }>
                  <TouchableOpacity
                    style={styles.btnClose}
                    onPress={() => {
                      this.setState({showLocationPopup: false});
                    }}>
                    <View>
                      <Icon
                        name="ios-close"
                        size={30}
                        type="ionicon"
                        color="#fff"
                      />
                    </View>
                  </TouchableOpacity>
                  <ScrollView style={{maxHeight: 680, paddingRight: 20}}>
                    <View
                      style={{
                        borderRadius: 10,
                        marginTop: 25,
                        alignItems: 'center',
                        paddingBottom: 25,
                      }}>
                      <Image
                        style={styles.locationImg}
                        source={{uri: this.state.location_image}}
                      />
                      <View
                        style={{
                          flexDirection: 'row',
                          alignItems: 'center',
                          width: '100%',
                        }}>
                        <View style={styles.ratingMain}>
                          <StarRating
                            disabled={false}
                            maxStars={5}
                            starSize={20}
                            fullStarColor="#cf8cac"
                            rating={this.state.users_ratings}
                          />
                        </View>
                      </View>
                      <View style={styles.btnsecret}>
                        <Text
                          style={{
                            fontSize: 15,
                            color: '#fff',
                            fontFamily: 'BAHNSCHRIFT',
                          }}>
                          {this.state.location_name}
                        </Text>
                        <Image
                          style={styles.flowerImgpop}
                          source={require('../../assets/pic3.png')}
                        />
                      </View>
                    </View>
                    <View
                      style={{
                        padding: 15,
                        backgroundColor: '#f0e0e2',
                        borderWidth: 1,
                        borderColor: '#e7c7d6',
                        borderRadius: 10,
                        flex: 1,
                        justifyContent: 'flex-end',
                        alignItems: 'flex-end',
                      }}>
                      <TouchableOpacity
                        style={styles.touchcss}
                        onPress={() => {
                          this.setState({_showReviews: true});
                        }}>
                        <View
                          style={{
                            width: '100%',
                            height: 10,
                            marginBottom: 10,
                            flexDirection: 'row',
                            alignItems: 'center',
                          }}>
                          <View>
                                                  <Image
                          style={styles.flowerImgpop2}
                          source={require('../../assets/pic2.png')}
                        />
                            <Text
                              style={{
                                fontSize: 10,
                                color: '#cf8cac',
                                fontFamily: 'GeorgiaPro-Bold',
                                textTransform: 'uppercase',
                              }}>
                              Comments
                            </Text>
                          </View>
                          <View style={{marginLeft: 'auto'}}>
                            <Icon
                              name="angle-down"
                              size={25}
                              type="font-awesome"
                              color="#cf8cac"
                            />
                          </View>
                        </View>
                        {this.state._showReviews == true
                          ? this._renderReviews()
                          : null}
                      </TouchableOpacity>
                    </View>
                  </ScrollView>
                </View>
              </ImageBackground>
            </Modal>
          </View>
        </ScrollView>
        <View style={styles.footMain}>
          <View style={styles.footFlex}>
            <TouchableOpacity onPress={() => Actions.exploreRegion()}>
              <Icon name="ios-planet" size={35} type="ionicon" color="#fff" />
            </TouchableOpacity>
          </View>
          <View style={styles.footFlex}>
            <TouchableOpacity onPress={() => Actions.Profile()}>
              <Icon
                size={30}
                name="ios-person"
                type="ionicon"
                color="#fff"
              />
            </TouchableOpacity>
          </View>
          <View style={styles.footFlex}>
            <TouchableOpacity
              style={styles.centerMenu}
              onPress={this._onSubmit.bind(this)}>
              <Image
                style={{
                  marginTop: 20,
                  width: 40,
                  height: 40,
                  resizeMode: 'contain',
                }}
                source={require('../../assets/heartlock.png')}
              />
            </TouchableOpacity>
          </View>
          <View style={styles.footFlex}>
            <TouchableOpacity onPress={() => Actions.home()}>
              <Icon size={27} name="ios-home" type="ionicon" color="#fff" />
            </TouchableOpacity>
          </View>
          <View style={styles.footFlex}>
            <TouchableOpacity onPress={() => Actions.Profile()}>
              <Icon size={27} name="ios-search" type="ionicon" color="#fff" />
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  favNoti: {
    position: 'absolute',
    top: -5,
    right: -10,
    fontFamily: 'BAHNSCHRIFT',
    width: 15,
    fontSize: 11,
    height: 15,
    borderRadius: 50,
    backgroundColor: '#cf8cac',
    color: '#fff',
    textAlign: 'center',
    alignItems: 'center',
    justifyContent: 'center',
  },
  comntList: {
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: 'rgba(255,255,255, 1)',
    paddingHorizontal: 15,
    paddingVertical: 10,
    borderRadius: 5,
    marginBottom: 5,
  },
  UserName: {
    fontFamily: 'GeorgiaPro-Bold',
    fontSize: 13,
    marginBottom: 3,
  },
  ComntTxt: {
    fontFamily: 'BAHNSCHRIFT',
    fontSize: 12,
    lineHeight: 15,
  },
  comntCols: {
    flex: 1,
  },
  comntCols2: {
    flex: 1,
    marginRight: 'auto',
    alignItems: 'flex-end',
    maxWidth: 80,
  },
  ratingUser: {
    flexDirection: 'row',
  },
  ratingMain: {
    flexDirection: 'row',
    top: -260,
  },
  goRight: {
    width: 20,
    height: 20,
    resizeMode: 'contain',
    right: 10,
    top: 16,
  },
  locationImg: {
    width: '100%',
    height: 250,
    resizeMode: 'cover',
    borderRadius: 10,
  },
  flowerImg2: {
    width: 60,
    height: 60,
    zIndex: 11,
    position: 'absolute',
    resizeMode: 'contain',
    left: -15,
    top: -15,
  },
  flowerImg5: {
    width: 50,
    zIndex: 11,
    height: 50,
    position: 'absolute',
    resizeMode: 'contain',
    left: 0,
    transform: [{rotate: '-160deg'}],
    top: -20,
  },
  flowerImgpop: {
    width: 40,
    zIndex: 11,
    height: 40,
    position: 'absolute',
    resizeMode: 'contain',
    right: -15,
    bottom: -5,
  },
    flowerImgpop2: {
    width: 50,
    zIndex: 11,
    height: 50,
    position: 'absolute',
    resizeMode: 'contain',
    left: -25,
    bottom: -5,
  },
  flowerImg: {
    width: 60,
    zIndex: 11,
    height: 60,
    position: 'absolute',
    resizeMode: 'contain',
    right: -15,
    bottom: -15,
  },
  eyeImg: {
    width: 20,
    height: 20,
    resizeMode: 'contain',
  },
  checkboxTxt: {
    flex: 1,
    fontFamily: 'BAHNSCHRIFT',
    color: '#fff',
    fontSize: 13,
    lineHeight: 12,
    marginTop: 10,
    padding: 0,
  },
  checkBox: {
    marginLeft: 'auto',
    maxWidth: 60,
    maxHeight: 45,
  },
  fullWidth: {
    width: '100%',
    position: 'relative',
  },
  checkboxRow: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  btnClose: {
    borderRadius: 30,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    position: 'absolute',
    top: 30,
    right: 20,
    zIndex: 99,
    width: 30,
    height: 30,
    paddingBottom: 0,
    backgroundColor: '#cf8cac',
  },
  expandBtn: {
    backgroundColor: '#cf8cac',
    textAlign: 'center',
    borderRadius: 30,
    fontSize: 14,
    marginBottom: 15,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    flex: 1,
    marginHorizontal: 5,
  },
  centerBtn: {
    minWidth: 60,
  },
  btnFlex: {
    flex: 1,
  },
  btnGroup: {
    flexDirection: 'row',
    // paddingHorizontal: 20,
    // paddingTop: 15,
  },
  dateTravel: {
    position: 'absolute',
    top: 10,
    right: 10,
    zIndex: 99,
    backgroundColor: '#e7c7d6',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 100,
    paddingHorizontal: 15,
    paddingTop: 3,
    paddingBottom: 5,
    flexDirection: 'row',
  },
  createPic: {
    position: 'absolute',
    top: -48,
    left: -1,
    zIndex: 99,
    backgroundColor: '#e7c7d6',
    alignItems: 'center',
    justifyContent: 'center',
    borderTopRightRadius: 100,
    borderBottomLeftRadius: 100,
    borderBottomRightRadius: 100,
    width: 90,
    height: 90,
    borderWidth: 1,
    borderColor: '#fff',
  },
  createPicImg: {
    width: 80,
    height: 80,
    resizeMode: 'cover',
    borderTopRightRadius: 100,
    borderBottomRightRadius: 100,
    borderBottomLeftRadius: 100,
  },
  logout: {
    position: 'absolute',
    right: 20,
    top: 15,
  },
  favorite: {
    position: 'absolute',
    right: 5,
    bottom: 5,
    zIndex: 999999,
  },
  letsEdit: {
    position: 'absolute',
    right: 10,
    bottom: 0,
    top: 0,
    zIndex: 0,
    opacity: 0.5,
    width: 140,
    resizeMode: 'contain',
  },
  gridTitle: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: '30%',
    margin: 'auto',
    fontSize: 12,
    textAlign: 'center',
    color: '#cf8cac',
    paddingVertical: 5,
    backgroundColor: 'rgba(240,224,226, .8)',
    elevation: 1,
    marginVertical: 10,
    fontFamily: 'GeorgiaPro-Bold',
  },
  TouchTitle: {
    position: 'absolute',
    left: 0,
    right: 0,
    zIndex: 999,
  },
  gridImg: {
    width: '100%',
    height: 120,
    resizeMode: 'cover',
    opacity: 0.5,
    borderRadius: 5,
    borderWidth: 1,
    borderColor: '#e9f0f6',
  },
  flexGrid: {
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 0,
    marginLeft: 2,
    marginRight: 2,
    height: 120,
    width: '33.3%',
    borderRadius: 5,
    backgroundColor: '#fff',
  },
  weKnow: {
    color: '#cf8cac',
    fontSize: 16,
    textAlign: 'center',
    lineHeight: 22,
    marginTop: 20,
    marginBottom: 20,
    paddingVertical: 10,
    paddingHorizontal: 40,
    fontWeight: 'bold',
    backgroundColor: '#f0e0e2',
    borderRadius: 100,
  },
  absoluTxt: {
    position: 'absolute',
    bottom: 20,
    left: 20,
    top: 20,
    backgroundColor: 'rgba(255,255,255, .7)',
    right: 20,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    paddingBottom: 10,
    borderRadius: 5,
    elevation: 1,
  },
  bannerArea: {
    alignItems: 'center',
    justifyContent: 'center',
    position: 'relative',
    backgroundColor: '#fff',
    borderWidth: 1,
    borderColor: '#ddd',
  },
  bannerImg: {
    height: 400,
    width: '100%',
    // resizeMode: 'cover',
  },
  contentArea: {
    paddingHorizontal: 20,
    paddingBottom: 20,
  },
  footMain: {
    backgroundColor: '#cf8cac',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    paddingTop: 10,
    paddingBottom: 10,
    maxHeight: 55,
  },
  centerMenu: {
    backgroundColor: '#cf8cac',
    position: 'relative',
    top: -15,
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    borderTopLeftRadius: 50,
    borderTopRightRadius: 50,
  },
  centerMenu: {
    backgroundColor: '#cf8cac',
    position: 'relative',
    top: -15,
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    borderTopLeftRadius: 50,
    borderTopRightRadius: 50,
  },
  footFlex: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-start',
    height: '100%',
    paddingTop: 30,
  },
  header: {
    position: 'relative',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    elevation: 1,
    // backgroundColor: '#f0e0e2',
    width: '100%',
    paddingTop: 6,
    paddingBottom: 6,
  },
  hdr_img: {
    width: 200,
    height: 50,
    marginBottom: 5,
    resizeMode: 'stretch',
  },
  body: {
    flex: 1,
    height: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  body_img: {
    alignItems: 'center',
    width: 220,
    height: 186,
  },
  ftr_hdngs: {
    textAlign: 'center',
    color: '#fff',
    fontSize: 25,
  },
  instagram: {
    backgroundColor: '#cf8cac',
    alignItems: 'stretch',
    paddingVertical: 13,
    textAlign: 'center',
    lineHeight: 20,
    borderRadius: 30,
    paddingBottom: 15,
    fontSize: 16,
    marginBottom: 15,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    flex: 1,
    marginRight: 5,
  },
  btnEmail: {
    paddingVertical: 15,
    backgroundColor: '#f0e0e2',
    textAlign: 'center',
    borderRadius: 0,
    borderWidth: 2,
    borderColor: '#cf8cac',
    fontSize: 14,
    marginBottom: 15,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    flex: 1,
  },
  btnTripDetail: {
    paddingVertical: 15,
    backgroundColor: '#cf8cac',
    borderRadius: 30,
    marginTop: 25,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    flex: 1,
    maxHeight: 50,
    minHeight: 50,
    marginHorizontal: 50,
  },
  btnsecret: {
    paddingVertical: 15,
    backgroundColor: '#cf8cac',
    borderRadius: 30,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    flex: 1,
    maxHeight: 50,
    borderWidth: 1,
    borderColor: '#fff',
    minHeight: 40,
    paddingHorizontal: 20,
    position: 'absolute',
    top: 0,
    zIndex: 11,
  },
  btnRemindme: {
    paddingVertical: 15,
    borderRadius: 30,
    marginTop: 25,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    flex: 1,
    borderWidth: 1,
    borderColor: '#cf8cac',
    maxHeight: 40,
    minHeight: 40,
    marginHorizontal: 70,
  },
  touchcss: {
    // position: 'relative',
    width: '100%',
    justifyContent: 'center',
  },
  notFound: {
    fontSize: 16,
    color: '#cf8cac',
    textAlign: 'center',
    alignItems: 'center',
    backgroundColor: '#fcfcfc',
    paddingHorizontal: 60,
    paddingVertical: 10,
    marginTop: 15,
    borderColor: '#ddd',
    borderWidth: 1,
    borderRadius: 5,
    width: '100%',
  },
  /************** */
  locationImgpop: {
    width: '50%',
    height: 120,
    resizeMode: 'cover',
    borderRadius: 10,
  },
  ratingMainpop: {
    top: -130,
  },
  btnsecretpop: {
    // paddingVertical: 20,
    backgroundColor: '#f0e0e2',
    borderRadius: 30,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    flex: 1,
    borderWidth: 1,
    borderColor: '#cf8cac',
    minHeight: 30,
    paddingHorizontal: 20,
    position: 'absolute',
    top: 120,
    zIndex: 11,
  },
  flowerImgpopup: {
    width: '100%',
    height: 120,
    resizeMode: 'contain',
    right: -15,
    bottom: -5,
  },
});
