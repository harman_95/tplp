import React from 'react';
import {
  Platform,
  AppRegistry,
  StyleSheet,
  Text,
  View,
  TextInput,
  Image,
  Button,
  TouchableHighlight,
  TouchableOpacity,
  CheckBox,
  ScrollView,
  AsyncStorage,
} from 'react-native';
import {Icon} from 'react-native-elements';
import {Actions} from 'react-native-router-flux';

export default class EditHome extends React.Component {
  _onSubmit = () => {
    AsyncStorage.clear();
    Actions.Signin();
  };

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.header}>
          <Image
            style={styles.hdr_img}
            source={require('../../assets/LOGO_Blk_.png')}
          />
        </View>

        <ScrollView style={{width: '100%', backgroundColor: '#fff'}}>
          <View style={styles.bannerArea}>
            <Image
              style={styles.bannerImg}
              source={require('../../assets/Picture7.jpg')}
            />
            <View style={styles.absoluTxt}>
              <Text style={styles.slideTxt}>
                Take your stories to the next level{'\n'} with TPLP’s templates
                and stickers
              </Text>
              <Text style={styles.absoTxt1}>Let's</Text>
              <Text style={styles.absoTxt2}>let's</Text>
              <Text style={styles.absoTxt3}>Edit</Text>
              <Text style={styles.absoTxt4}>edit</Text>
            </View>
          </View>

          <View style={styles.contentArea}>
            <View
              style={{
                justifyContent: 'center',
                position: 'relative',
                flexDirection: 'column',
              }}>
              <TouchableOpacity onPress={() => Actions.templates()}>
                <View style={styles.flexGrid}>
                  <Image
                    style={styles.gridImg}
                    source={require('../../assets/Picture5.jpg')}
                  />
                  <Text style={styles.gridTitle}>Story Templates</Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity onPress={() => Actions.moodBroad()}>
                <View style={styles.flexGrid}>
                  <Image
                    style={styles.gridImg}
                    source={require('../../assets/Picture4.jpg')}
                  />
                  <Text style={styles.gridTitle}>Mood Boards</Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity onPress={() => Actions.AddStickers()}>
                <View style={styles.flexGrid}>
                  <Image
                    style={styles.gridImg}
                    source={require('../../assets/Picture6.jpg')}
                  />
                  <Text style={styles.gridTitle}>Stickers</Text>
                </View>
              </TouchableOpacity>
            </View>
          </View>
        </ScrollView>

        <View style={styles.footMain}>
          <View style={styles.footFlex}>
            <TouchableOpacity onPress={() => Actions.exploreRegion()}>
              <Icon name="ios-planet" size={35} type="ionicon" color="#fff" />
            </TouchableOpacity>
          </View>

          <View style={styles.footFlex}>
            <TouchableOpacity onPress={() => Actions.Profile()}>
              <Icon
                size={30}
                name="ios-person"
                type="ionicon"
                color="#fff"
              />
            </TouchableOpacity>
          </View>

          <View style={styles.footFlex}>
            <TouchableOpacity
              style={styles.centerMenu}
              onPress={this._onSubmit.bind(this)}>
              <Image
                style={{
                  marginTop: 20,
                  width: 40,
                  height: 40,
                  resizeMode: 'contain',
                }}
                source={require('../../assets/heartlock.png')}
              />
            </TouchableOpacity>
          </View>

          <View style={styles.footFlex}>
            <TouchableOpacity onPress={() => Actions.home()}>
              <Icon size={27} name="ios-home" type="ionicon" color="#fff" />
            </TouchableOpacity>
          </View>

          <View style={styles.footFlex}>
            <TouchableOpacity onPress={() => Actions.Profile()}>
              <Icon size={27} name="ios-search" type="ionicon" color="#fff" />
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  slideTxt: {
    fontFamily: 'BAHNSCHRIFT',
    color: '#cf8cac',
    fontSize: 14,
    textAlign: 'center',
    paddingHorizontal: 20,
    position: 'relative',
    zIndex: 1,
    letterSpacing: -0.3,
    lineHeight: 18,
    textTransform: 'uppercase',
  },
  absoTxt3: {
    fontFamily: 'GeorgiaPro-Bold',
    color: '#cf8cac',
    fontSize: 45,
    opacity: 0.2,
    textTransform: 'uppercase',
    textAlign: 'center',
    paddingHorizontal: 20,
    position: 'absolute',
    zIndex: 0,
    right: 0,
    top: 80,
  },
  absoTxt4: {
    fontFamily: 'GeorgiaPro-Bold',
    color: '#cf8cac',
    fontSize: 35,
    textTransform: 'lowercase',
    opacity: 0.6,
    textAlign: 'center',
    paddingHorizontal: 20,
    position: 'absolute',
    zIndex: 0,
    right: 0,
    top: 120,
  },
  absoTxt1: {
    fontFamily: 'GeorgiaPro-Bold',
    color: '#cf8cac',
    fontSize: 45,
    opacity: 0.2,
    textTransform: 'uppercase',
    textAlign: 'center',
    paddingHorizontal: 20,
    position: 'absolute',
    zIndex: 0,
    right: 0,
    top: 10,
  },
  absoTxt2: {
    fontFamily: 'GeorgiaPro-Bold',
    color: '#cf8cac',
    fontSize: 35,
    opacity: 0.6,
    textAlign: 'center',
    paddingHorizontal: 20,
    position: 'absolute',
    zIndex: 0,
    right: 0,
    top: 50,
  },
  letsEdit: {
    position: 'absolute',
    right: 10,
    bottom: 0,
    top: 0,
    zIndex: 0,
    opacity: 0.5,
    width: 140,
    resizeMode: 'contain',
  },
  gridTitle: {
    position: 'absolute',
    left: 0,
    right: 0,
    margin: 'auto',
    fontSize: 45,
    textAlign: 'center',
    color: '#cf8cac',
    paddingVertical: 5,
    backgroundColor: 'rgba(255,255,255, .7)',
    elevation: 1,
    marginVertical: 10,
    fontFamily: 'Southampton',
    height: 55,
  },
  gridImg: {
    width: '100%',
    height: 120,
    resizeMode: 'stretch',
    opacity: 0.5,
    borderRadius: 5,
  },
  flexGrid: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 20,
  },
  weKnow: {
    color: '#cf8cac',
    fontSize: 16,
    textAlign: 'center',
    lineHeight: 22,
    marginTop: 20,
    marginBottom: 20,
    paddingVertical: 10,
    paddingHorizontal: 40,
    fontWeight: 'bold',
    backgroundColor: '#f0e0e2',
    borderRadius: 100,
  },
  absoluTxt: {
    position: 'absolute',
    bottom: 20,
    left: 20,
    top: 20,
    right: 20,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    paddingBottom: 10,
    borderRadius: 5,
    borderWidth: 1,
    borderColor: '#cf8cac',
  },
  bannerArea: {
    alignItems: 'center',
    justifyContent: 'center',
    position: 'relative',
  },
  bannerImg: {
    height: 250,
    width: '100%',
    resizeMode: 'cover',
    opacity: 0.5,
  },
  contentArea: {
    paddingHorizontal: 20,
    paddingBottom: 20,
  },
  footMain: {
    backgroundColor: '#cf8cac',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    paddingTop: 10,
    paddingBottom: 10,
    maxHeight: 55,
  },
  centerMenu: {
    backgroundColor: '#cf8cac',
    position: 'relative',
    top: -15,
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    borderTopLeftRadius: 50,
    borderTopRightRadius: 50,
  },
  footFlex: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-start',
    height: '100%',
    paddingTop: 0,
  },
  header: {
    position: 'relative',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    elevation: 1,
    backgroundColor: '#ffffff',
    width: '100%',
    paddingTop: 6,
    paddingBottom: 6,
  },
  hdr_img: {
    width: 200,
    height: 50,
    resizeMode: 'stretch',
  },
  body: {
    flex: 1,
    height: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  body_img: {
    alignItems: 'center',
    width: 220,
    height: 186,
  },
  ftr_hdngs: {
    textAlign: 'center',
    color: '#fff',
    fontSize: 25,
  },
  instagram: {
    backgroundColor: '#cf8cac',
    alignItems: 'stretch',
    paddingVertical: 13,
    textAlign: 'center',
    lineHeight: 20,
    borderRadius: 30,
    paddingBottom: 15,
    fontSize: 16,
    marginBottom: 15,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    flex: 1,
    marginRight: 5,
  },
  btnEmail: {
    paddingVertical: 5,
    backgroundColor: '#cf8cac',
    textAlign: 'center',
    borderRadius: 30,
    fontSize: 14,
    marginBottom: 15,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    flex: 1,
    marginLeft: 5,
  },
});
