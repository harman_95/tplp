/* eslint-disable handle-callback-err */
/* eslint-disable no-unused-vars */
/* eslint-disable react-native/no-inline-styles */
import React, {Component, useState} from 'react';
import {
  ImageBackground,
  Modal,
  Alert,
  Platform,
  AppRegistry,
  StyleSheet,
  Text,
  View,
  TextInput,
  Image,
  Button,
  TouchableHighlight,
  TouchableOpacity,
  CheckBox,
  ScrollView,
  FlatList,
  AsyncStorage,
} from 'react-native';
import {Icon} from 'react-native-elements';
import {Actions} from 'react-native-router-flux';
import ImageZoom from 'react-native-image-pan-zoom';
import ViewShot from 'react-native-view-shot';
import CameraRoll from '@react-native-community/cameraroll';
import {Snackbar} from 'react-native-paper';
import axios from 'axios';
import ContentLoader from 'react-native-content-loader';
import {Circle, Rect} from 'react-native-svg';
import {Col, Row, Grid} from 'react-native-easy-grid';
import ImagePicker from 'react-native-image-crop-picker';
import Video from 'react-native-video';
import DraggableFlatList from 'react-native-draggable-flatlist';
import {DynamicCollage, StaticCollage} from 'react-native-images-collage';
const moodboardimages = [];

const photos = [
  'https://assets.website-files.com/5e1c6cdadb4d52399503394c/5e1c6cdadb4d5203d70339d3_size4-mobile%20(1).png',
  'https://assets.website-files.com/5e1c6cdadb4d52399503394c/5e1c6cdadb4d5203d70339d3_size4-mobile%20(1).png',
  'https://assets.website-files.com/5e1c6cdadb4d52399503394c/5e1c6cdadb4d5203d70339d3_size4-mobile%20(1).png',
  'https://assets.website-files.com/5e1c6cdadb4d52399503394c/5e1c6cdadb4d5203d70339d3_size4-mobile%20(1).png',
  'https://assets.website-files.com/5e1c6cdadb4d52399503394c/5e1c6cdadb4d5203d70339d3_size4-mobile%20(1).png',
  'https://assets.website-files.com/5e1c6cdadb4d52399503394c/5e1c6cdadb4d5203d70339d3_size4-mobile%20(1).png',
  'https://assets.website-files.com/5e1c6cdadb4d52399503394c/5e1c6cdadb4d5203d70339d3_size4-mobile%20(1).png',
  'https://assets.website-files.com/5e1c6cdadb4d52399503394c/5e1c6cdadb4d5203d70339d3_size4-mobile%20(1).png',
  'https://assets.website-files.com/5e1c6cdadb4d52399503394c/5e1c6cdadb4d5203d70339d3_size4-mobile%20(1).png',
  // 'https://assets.website-files.com/5e1c6cdadb4d52399503394c/5e1c6cdadb4d5203d70339d3_size4-mobile%20(1).png',
  //   'https://assets.website-files.com/5e1c6cdadb4d52399503394c/5e1c6cdadb4d5203d70339d3_size4-mobile%20(1).png',
  //     'https://assets.website-files.com/5e1c6cdadb4d52399503394c/5e1c6cdadb4d5203d70339d3_size4-mobile%20(1).png',
];

export default class moodBroadBlank extends React.Component {
  constructor() {
    super();
    this.state = {
      dummymoodboardsimages: moodboardimages,
      moodboardsimages: '',
      visible: false,
      color: 'black',
      userid: '',
      loader: true,
      modalVisible: false,
      showCross: true,
      snackbarMsg: 'Please Upload at least 9 Images first!',
      showLogo: true,
      imageModal: true,
      imageCountIndex: '',
      photos: photos,
      matrixArr: [3, 3, 3],
    };

    setTimeout(() => {
      this.setState({
        loader: false,
      });
      this._renderLoader();
    }, 2000);
  }

  _onSubmit = () => {
    AsyncStorage.clear();
    Actions.Signin();
  };

  _onClickImage() {
    console.log('djasdjajsdjasjd');
  }

  /*************IF USER IS SUBSCRIBED POPUP WILL NOT SHOW********** */
  setModalVisible() {
    if (this.state.modalVisible == false) {
      this.setState({
        modalVisible: true,
      });
      setTimeout(() => {
        this.setState({
          modalVisible: false,
          showLogo: false,
        });
      }, 4000);
    } else {
      this.setState({
        modalVisible: false,
      });
    }
    console.log(this.state);
  }

  /*********GET USER LOGGED IN ID************ */

  async componentDidMount() {
    console.log('component DID mount');
    await AsyncStorage.getItem('@userLoggedInId').then(value => {
      console.log(value);
      this.setState({
        userid: value,
      });
    });

    await this._getMoodBoardImages();
  }

  /******************GET MOOD BOARDS IMAGES ***************** */

  async _getMoodBoardImages() {
    console.log('getMOOD Boards');
    await axios
      .post(
        'http://3.135.63.132:3000/api/v1/tplp/moodboards/getMoodBoardsImagesByUserIdMoodBoardId',
        {
          userid: this.state.userid,
          moodboardid: this.props.moodboardid,
        },
      )
      .then(responseJson => {
        if (
          responseJson.data.images == null ||
          responseJson.data.images == undefined ||
          responseJson.data.images == ''
        ) {
          this.setState({
            moodboardsimages: this.state.dummymoodboardsimages,
          });
        } else {
          this.setState({
            moodboardsimages: responseJson.data.images.moodboardimages,
          });
        }
      })
      .catch(error => {});
  }

  /**********CHECK IF THE IMAGES HAVE BEEN UPLOADED IN FIRST NINE BOXES******** */

  checkImagesFull2() {
    for (var i = 0; i < 8; i++) {
      if (this.state.moodboardsimages[i].imgsrc == null) {
        return true;
      }
    }
  }

  /*************CHECK IF ANY IMAGE IS NOT NULL************* */

  checkImageNotNull() {
    for (var i = 0; i < 8; i++) {
      if (this.state.moodboardsimages[i].imgsrc != null) {
        return true;
      }
    }
  }

  /************SAVE COLLAGE OF FIRST NINE TO GALLERY*********** */

  takeViewShot() {
    if (this.checkImagesFull2() !== true) {
      console.log(this.state.moodboardsimages.length);
      if (this.state.moodboardsimages.length > 9) {
        var p1 = this.state.moodboardsimages.slice(0, 9);
        var p2 = this.state.moodboardsimages.slice(9);
        console.log(p1);
        console.log(p2);
        this.setState({
          moodboardsimages: p1,
          showCross: false,
        });
        this.viewShot.capture().then(uri => {
          Image.getSize(uri, (width, height) => {
            console.log(this.state);
            ImagePicker.openCropper({
              path: uri,
              width: 480,
              height: 980,
            }).then(
              image => {
                console.log(image.path);
                CameraRoll.saveToCameraRoll(image.path);
                this.setState({
                  visible: true,
                  snackbarMsg: 'Saved to Gallery!',
                });

                axios
                  .post('http://3.135.63.132:3000/api/v1/tplp/addPhotos', {
                    userid: this.state.userid,
                    photos: image.path,
                  })
                  .then(responseJson => {
                    console.log(responseJson);
                  })
                  .catch(error => {});
              },
              err =>
                this.setState({
                  visible: true,
                  snackbarMsg: 'Error! Try Again!',
                }),
            );
            p1 = p1.concat(p2);
            this.setState({
              moodboardsimages: p1,
              showCross: true,
            });
          });
        });
      }
    } else {
      this.setState({
        visible: true,
        snackbarMsg: 'Please Upload at least 9 Images first!',
      });
    }
  }

  /*************RENDER ITEMS IN A FLATLIST********************** */

  _renderItem = ({item, index, drag, isActive}) => {
    if (item.imgsrc == null) {
      return (
        <TouchableOpacity style={styles.flexGridTouch}>
          <View style={styles.flexGrid}>
            <View
              style={{
                justifyContent: 'center',
                alignItems: 'center',
                height: '100%',
              }}>
              <TouchableOpacity
                style={styles.addImg}
                onPress={() => this._uploadImageToMoodBroad(index)}>
                <Icon name="ios-add" size={30} type="ionicon" color="#cf8cac" />
              </TouchableOpacity>
            </View>
          </View>
        </TouchableOpacity>
      );
    } else {
      if (
        item.imgType === 'img' ||
        item.imgType === undefined ||
        item.imgType === null
      ) {
        return (
          <TouchableOpacity style={styles.flexGridTouch}>
            <View style={styles.flexGrid}>
              <View
                style={{
                  justifyContent: 'center',
                  alignItems: 'center',
                  height: '100%',
                  width: '100%',
                }}>
                <TouchableOpacity
                  style={styles.uploadedImg}
                  onPress={() => this._uploadImageToMoodBroad(index)}>
                  <Image
                    style={{width: '100%', height: 177}}
                    source={{uri: item.imgsrc}}
                  />
                </TouchableOpacity>
              </View>
            </View>
          </TouchableOpacity>
        );
      } else {
        return (
          <TouchableOpacity style={styles.flexGridTouch}>
            <View style={styles.containervid}>
              <View style={styles.flexGrid}>
                <View
                  style={{
                    justifyContent: 'center',
                    alignItems: 'center',
                    height: '100%',
                    width: '100%',
                  }}>
                  <TouchableOpacity
                    style={styles.uploadedImg}
                    onPress={() => this._uploadImageToMoodBroad(index)}>
                    <ImageZoom
                      cropWidth={160}
                      cropHeight={170}
                      imageWidth={300}
                      imageHeight={170}>
                      <Video
                        source={{
                          uri: item.imgsrc,
                        }} // Can be a URL or a local file.
                        ref={ref => {
                          this.player = ref;
                        }} // Store reference
                        onBuffer={() => {
                          // console.log('buffer', true);
                        }}
                        onEnd={() => {
                          // console.log('end', true);
                        }}
                        onError={() => {
                          // console.log('error', true);
                        }}
                        onLoad={() => {
                          // console.log('load', true);
                        }}
                        onProgress={() => {
                          // console.log('progress', true);
                        }}
                        resizeMode={'contain'}
                        style={styles.backgroundVideo}
                      />
                    </ImageZoom>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </TouchableOpacity>
        );
      }
    }
  };

  /*******************SAVE IMAGES BADSED ON MOOD BOARD ************* */

  async _saveImagesByID() {
    await axios
      .post(
        'http://3.135.63.132:3000/api/v1/tplp/moodboards/saveMoodBoardsByUserId',
        {
          userid: this.state.userid,
          moodboardid: this.props.moodboardid,
          moodboardimages: this.state.moodboardsimages,
        },
      )
      .then(responseJson => {
        console.log(responseJson);
      })
      .catch(error => {});
  }

  /**********UPLOAD IMAGE FROM GALLERY OR CAMERA************** */

  _takePicture() {
    ImagePicker.openPicker({
      width: 300,
      height: 400,
      cropping: true,
      multiple: true,
    }).then(response => {
      this.setState({
        imageModal: false,
      });
      console.log(response);

      if (response.length > 0) {
        for (var i = 0; i < response.length; i++) {
          if (i !== response.length - 1) {
            this.state.photos[i] = response[i].path;
            // this.setState({
            //   photos: this.state.photos,
            // });
          } else if (i === response.length - 1) {
            this.state.photos[i] = response[i].path;
            // this.setState({photos: this.state.photos}, () => {
              const val = this.state.photos.length / 3;
              console.log(val);
              // Alert.alert('Please select at least 9 photos!');
              // this._takePicture();
            // });
          }
        }

        // for(var j=0;j<this.state.photos.length;j++){

        // }
      }
    });
  }

  /***********RENDER LOADER COLS********** */
  _renderCol() {
    return (
      <Col>
        <Row>
          <ContentLoader height={200} duration={1000}>
            <Rect x="0" y="5" rx="0" ry="0" width="100" height="200" />
          </ContentLoader>
        </Row>
        <Row>
          <ContentLoader height={200} duration={1000}>
            <Rect x="0" y="5" rx="0" ry="0" width="100" height="200" />
          </ContentLoader>
        </Row>
        <Row>
          <ContentLoader height={200} duration={1000}>
            <Rect x="0" y="5" rx="0" ry="0" width="100" height="200" />
          </ContentLoader>
        </Row>
      </Col>
    );
  }

  /******************SHOW LOADER************** */

  _renderLoader() {
    if (this.state.loader == true) {
      return (
        <View style={styles.contentArea}>
          <Text style={styles.mainTitle}>{this.props.moodboardtitle}</Text>
          <Grid>
            {this._renderCol()}
            {this._renderCol()}
            {this._renderCol()}
          </Grid>
        </View>
      );
    } else {
      return (
        <View style={styles.contentArea}>
          <Text style={styles.mainTitle}>{this.props.moodboardtitle}</Text>

          <View style={{paddingHorizontal: 20}}>
            <View style={{position: 'relative', flexDirection: 'row', flex: 1}}>
              <ViewShot
                style={{flex: null, backgroundColor: '#fff'}}
                ref={ref => {
                  this.viewShot = ref;
                }}
                options={{format: 'jpg', quality: 1.0}}>
                {this.state.showLogo === true ? (
                  <View>
                    <Image
                      style={styles.absoLogo}
                      source={require('../../assets/pic.png')}
                    />

                    {this.state.showCross === true ? (
                      <TouchableOpacity
                        style={styles.logoBtnClose}
                        onPress={() => this.setModalVisible()}>
                        <View
                          style={{alignItems: 'center', flexDirection: 'row'}}>
                          <Icon
                            name="ios-close"
                            size={20}
                            type="ionicon"
                            color="#cf8cac"
                          />
                        </View>
                      </TouchableOpacity>
                    ) : null}
                  </View>
                ) : null}

                {/* <FlatList
                  data={this.state.moodboardsimages}
                  renderItem={this._renderItem}
                  numColumns={3}
                  keyExtractor={(item, index) => index.toString()}
                /> */}

                <DynamicCollage
                  width={320}
                  height={650}
                  images={this.state.photos}
                  matrix={this.state.matrixArr}
                  imageStyle={{
                    borderWidth: 2,
                    borderColor: '#cf8cac',
                  }}
                />
              </ViewShot>
            </View>
          </View>
        </View>
      );
    }
  }

  /**********RENDER VIEW********* */

  render() {
    const {moodboardtitle, moodboardid} = this.props;
    console.log(this.props);

    return (
      <View style={styles.container}>
        <Snackbar
          style={{backgroundColor: this.state.color}}
          visible={this.state.visible}
          onDismiss={() => this.setState({visible: false})}
          action={{
            onPress: () => {
              // Do something
            },
          }}>
          {this.state.snackbarMsg}
        </Snackbar>
        <View style={styles.header}>
          <Image
            style={styles.hdr_img}
            source={require('../../assets/LOGO_Blk_.png')}
          />
          <View style={styles.logout}>
            <TouchableOpacity onPress={() => this._takePicture()}>
              <Icon
                name="ios-images"
                size={25}
                type="ionicon"
                color="#cf8cac"
              />
            </TouchableOpacity>
            <TouchableOpacity onPress={() => this.takeViewShot()}>
              <Icon
                name="ios-download"
                size={25}
                type="ionicon"
                color="#cf8cac"
              />
            </TouchableOpacity>
          </View>
        </View>
        <ScrollView style={{width: '100%'}}>
          {this._renderLoader()}

          {/* <DynamicCollage
            width={360}
            height={650}
            images={this.state.photos}
            matrix={[3, 3, 3]}
            onPress={() => this._onClickImage()}
          /> */}

          <View style={{alignItems: 'center', justifyContent: 'center'}}>
            <Modal
              style={{
                alignItems: 'center',
                justifyContent: 'center',
                height: '100%',
              }}
              animationType="fade"
              transparent={true}
              visible={this.state.modalVisible}
              onRequestClose={() => {
                Alert.alert('Modal has been closed.');
              }}>
              <ImageBackground
                source={require('../../assets/overlaybg.png')}
                style={{width: '100%', height: '100%'}}>
                <View
                  style={{
                    alignItems: 'center',
                    marginTop: 0,
                    height: 900,
                    paddingTop: 180,
                    width: '100%',
                  }}>
                  <TouchableHighlight
                    style={styles.pribtnClose}
                    onPress={() => {
                      this.setModalVisible();
                    }}>
                    <Icon
                      name="ios-close"
                      size={25}
                      type="ionicon"
                      color="#fff"
                    />
                  </TouchableHighlight>

                  <View style={styles.scrollPricing}>
                    <ScrollView horizontal={true}>
                      <View style={styles.priColumn}>
                        <Text style={styles.priceHdng}>Editing</Text>
                        <View style={{position: 'relative', marginBottom: 40}}>
                          <Text style={styles.priceTxt}>$4.99</Text>
                          <Text style={styles.dollarTxt}>per month</Text>
                        </View>
                        <Text style={styles.priFeatures}>
                          Unlimited access to editing features
                        </Text>
                        <Text style={styles.priFeatures}>StoryTemplates</Text>
                        <Text style={styles.priFeatures}>MoodBoards</Text>
                        <Text style={styles.priFeatures}>Stickers</Text>
                        <Image
                          style={styles.priflowerImg}
                          source={require('../../assets/pic2.png')}
                        />
                      </View>
                      {/* <View style={styles.priColumn}>
                        <Text style={styles.priceHdng}>Per City</Text>
                        <View style={{position: 'relative', marginBottom: 40}}>
                          <Text style={styles.priceTxt}>$89.99</Text>
                          <Text style={styles.dollarTxt}>pay per city</Text>
                        </View>
                        <Text style={styles.priFeatures}>
                          Yearlong access to a customized.
                        </Text>
                        <Text style={styles.priFeatures}>
                          Photo Itinerary in the city of your choosing
                        </Text>
                        <Image
                          style={styles.priflowerImg}
                          source={require('../../assets/pic2.png')}
                        />
                      </View> */}
                      {/* <View style={styles.priColumn}>
                        <Text style={styles.priceHdngBundle}>Bundle two</Text>
                        <Text style={styles.priceHdng}>... Or more cities</Text>
                        <View
                          style={{
                            position: 'relative',
                            marginBottom: 40,
                            marginTop: 20,
                          }}>
                          <Text style={styles.priceTxt}>15%</Text>
                          <Text style={styles.dollarTxt}>off</Text>
                        </View>
                        <Text style={styles.priFeatures}>
                          Purchase two or more cities to receive discount
                        </Text>
                        <Image
                          style={styles.priflowerImg}
                          source={require('../../assets/pic2.png')}
                        />
                      </View> */}
                    </ScrollView>
                  </View>
                </View>
              </ImageBackground>
            </Modal>

            <Modal
              style={{
                alignItems: 'center',
                justifyContent: 'center',
                height: '100%',
              }}
              animationType="fade"
              transparent={true}
              visible={this.state.imageModal}
              onRequestClose={() => {
                Alert.alert('Modal has been closed.');
              }}>
              <ImageBackground
                source={require('../../assets/overlaybg.png')}
                style={{width: '100%', height: '100%'}}>
                <View
                  style={{
                    alignItems: 'center',
                    marginTop: 0,
                    height: 900,
                    paddingTop: 140,
                    width: '100%',
                  }}>
                  {/* <TouchableOpacity
                    style={styles.btnClose}
                    onPress={() => {
                      this.setState({imageModal: false});
                    }}>
                    <View>
                      <Icon
                        name="ios-close"
                        size={30}
                        type="ionicon"
                        color="#fff"
                      />
                    </View>
                  </TouchableOpacity> */}
                  <View style={styles.scrollPricing} />
                  <View
                    style={{
                      width: '100%',
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}>
                    <View style={styles.loadingmain}>
                      <Text style={styles.loadingTxt}>
                        Upload photos First - (You can select multiple photos
                        too)
                      </Text>
                      {/* <Image
                        style={styles.loadingImg}
                        source={require('../../assets/Blooming-Rose-GIF.gif')}
                      /> */}

                      <TouchableOpacity
                        style={styles.btnTripDetail}
                        onPress={() => this._takePicture()}>
                        <Text
                          style={{
                            fontSize: 15,
                            color: '#fff',
                            fontFamily: 'GeorgiaPro-Bold',
                          }}>
                          CHOOSE PHOTO
                        </Text>
                      </TouchableOpacity>
                      {/* <Text
                        style={{
                          fontFamily: 'BAHNSCHRIFT',
                          color: '#cf8cac',
                          fontSize: 15,
                          textAlign: 'center',
                          marginTop: 20,
                          lineHeight: 30,
                        }}>
                        OR
                      </Text> */}
                      {/* <TouchableOpacity
                        style={styles.btnTripDetail}
                        onPress={() => this._takeVideo()}>
                        <Text
                          style={{
                            fontSize: 15,
                            color: '#fff',
                            fontFamily: 'GeorgiaPro-Bold',
                          }}>
                          CHOOSE VIDEO
                        </Text>
                      </TouchableOpacity> */}
                    </View>
                  </View>
                </View>
              </ImageBackground>
            </Modal>
          </View>
        </ScrollView>

        <View style={styles.footMain}>
          <View style={styles.footFlex}>
            <TouchableOpacity onPress={() => Actions.exploreRegion()}>
              <Icon name="ios-planet" size={35} type="ionicon" color="#fff" />
            </TouchableOpacity>
          </View>

          <View style={styles.footFlex}>
            <TouchableOpacity onPress={() => Actions.Profile()}>
              <Icon size={30} name="ios-person" type="ionicon" color="#fff" />
            </TouchableOpacity>
          </View>

          <View style={styles.footFlex}>
            <TouchableOpacity
              style={styles.centerMenu}
              onPress={this._onSubmit.bind(this)}>
              <Image
                style={{
                  marginTop: 20,
                  width: 40,
                  height: 40,
                  resizeMode: 'contain',
                }}
                source={require('../../assets/heartlock.png')}
              />
            </TouchableOpacity>
          </View>

          <View style={styles.footFlex}>
            <TouchableOpacity onPress={() => Actions.home()}>
              <Icon size={27} name="ios-home" type="ionicon" color="#fff" />
            </TouchableOpacity>
          </View>

          <View style={styles.footFlex}>
            <TouchableOpacity onPress={() => Actions.Profile()}>
              <Icon size={27} name="ios-search" type="ionicon" color="#fff" />
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  mainTitle: {
    marginBottom: 10,
    fontSize: 15,
    backgroundColor: '#e7c6d5',
    paddingVertical: 7,
    textAlign: 'center',
    color: '#fff',
    fontFamily: 'GeorgiaPro-Bold',
  },
  absoLogo: {
    position: 'absolute',
    width: '95%',
    resizeMode: 'contain',
    zIndex: 99,
    top: 160,
    left: 15,
  },
  addImg: {
    position: 'absolute',
    width: 50,
    alignItems: 'center',
    justifyContent: 'center',
    height: 50,
    borderRadius: 50,
    borderWidth: 1,
    borderColor: '#cf8cac',
  },
  uploadedImg: {
    position: 'absolute',
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    height: 180,
    borderRadius: 5,
    borderWidth: 1,
    borderColor: '#cf8cac',
  },
  fullWidth: {
    width: '100%',
    position: 'relative',
  },
  flowerImg: {
    width: 60,
    height: 60,
    position: 'absolute',
    resizeMode: 'contain',
    right: -15,
    bottom: -15,
  },
  flowerImg2: {
    width: 60,
    height: 60,
    position: 'absolute',
    resizeMode: 'contain',
    left: -15,
    top: -15,
  },
  popupBox: {
    position: 'absolute',
    top: '20%',
    left: 25,
    right: 25,
    backgroundColor: 'rgba(255,255,255, .8)',
    borderRadius: 5,
  },
  logout: {
    position: 'absolute',
    right: 20,
    top: 10,
  },
  temTitle: {
    fontSize: 30,
    textAlign: 'center',
    color: '#cf8cac',
    paddingVertical: 5,
    backgroundColor: '#f0e0e2',
    marginVertical: 10,
    width: '100%',
    fontFamily: 'GeorgiaPro-Bold',
  },
  gridTitle: {
    position: 'absolute',
    left: 0,
    right: 0,
    margin: 'auto',
    fontSize: 14,
    textAlign: 'center',
    color: '#111',
    paddingVertical: 5,
    backgroundColor: 'rgba(255,255,255, .7)',
    elevation: 1,
    height: 30,
    fontFamily: 'GeorgiaPro-Bold',
  },
  gridImg: {
    width: '100%',
    height: 220,
    resizeMode: 'stretch',
    borderRadius: 5,
    resizeMode: 'cover',
  },
  flexGrid: {
    width: '100%',
  },
  flexGridTouch: {
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 1,
    height: 180,
    width: '33%',
    marginRight: 1,
    borderWidth: 1,
    borderColor: '#ccc',
    borderRadius: 5,
    borderStyle: 'solid',
    backgroundColor: 'rgba(255,255,255, .5)',
  },
  contentArea: {
    paddingHorizontal: 0,
    paddingBottom: 20,
  },
  footMain: {
    backgroundColor: '#cf8cac',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    paddingTop: 10,
    paddingBottom: 10,
    maxHeight: 55,
  },
  centerMenu: {
    backgroundColor: '#cf8cac',
    position: 'relative',
    top: -15,
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    borderTopLeftRadius: 50,
    borderTopRightRadius: 50,
  },
  footFlex: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  container: {
    alignItems: 'center',
    justifyContent: 'flex-start',
    height: '100%',
    paddingTop: 0,
    width: '100%',
  },
  header: {
    position: 'relative',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    elevation: 1,
    backgroundColor: '#ffffff',
    width: '100%',
    paddingTop: 6,
    paddingBottom: 6,
  },
  hdr_img: {
    width: 200,
    height: 50,
    resizeMode: 'stretch',
  },
  body: {
    flex: 1,
    height: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  ftr_hdngs: {
    textAlign: 'center',
    color: '#fff',
    fontSize: 25,
  },
  instagram: {
    backgroundColor: '#cf8cac',
    alignItems: 'stretch',
    paddingVertical: 13,
    textAlign: 'center',
    lineHeight: 20,
    borderRadius: 30,
    paddingBottom: 15,
    fontSize: 16,
    marginBottom: 15,
    display: 'flex',
    justifyContent: 'center',
    flexDirection: 'row',
    flex: 1,
    marginRight: 5,
  },
  logoBtnClose: {
    borderRadius: 20,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    position: 'absolute',
    top: 200,
    right: 40,
    zIndex: 99,
    width: 20,
    height: 20,
    paddingBottom: 0,
    borderWidth: 1,
    borderColor: '#cf8cac',
    borderStyle: 'dotted',
  },
  /************PRICING MODAL********** */

  priceHdngBundle: {
    color: '#fff',
    fontSize: 25,
    textAlign: 'center',
    fontFamily: 'GeorgiaPro-Bold',
    textTransform: 'uppercase',
  },
  priceHdng: {
    color: '#fff',
    fontSize: 18,
    textAlign: 'center',
    marginBottom: 10,
    fontFamily: 'GeorgiaPro-Bold',
    textTransform: 'uppercase',
  },
  priceTxt: {
    color: '#fff',
    fontSize: 50,
    textAlign: 'center',
    fontFamily: 'GeorgiaPro-Bold',
  },
  dollarTxt: {
    fontSize: 40,
    fontFamily: 'Southampton',
    color: '#fff',
    position: 'absolute',
    bottom: -30,
    right: 10,
  },
  scrollPricing: {
    width: '100%',
    flexDirection: 'row',
    paddingLeft: 20,
  },
  priColumn: {
    backgroundColor: '#cf8cac',
    paddingHorizontal: 25,
    paddingVertical: 30,
    width: 320,
    marginBottom: 10,
    marginRight: 10,
    borderRadius: 15,
  },
  priFeatures: {
    color: '#fff',
    fontSize: 16,
    textAlign: 'center',
    marginBottom: 20,
    textTransform: 'uppercase',
    fontFamily: 'BAHNSCHRIFT',
  },
  pribtnClose: {
    borderRadius: 30,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    position: 'absolute',
    top: 30,
    right: 20,
    zIndex: 99,
    width: 30,
    height: 30,
    backgroundColor: '#cf8cac',
  },
  priflowerImg: {
    width: 60,
    height: 60,
    resizeMode: 'contain',
    position: 'absolute',
    bottom: -10,
    transform: [{rotate: '50deg'}],
    left: '46%',
  },
  loadingImg: {
    width: 250,
    height: 250,
    resizeMode: 'contain',
  },
  loadingTxt: {
    fontFamily: 'BAHNSCHRIFT',
    textTransform: 'uppercase',
    fontSize: 20,
    textAlign: 'center',
    color: '#cf8cac',
  },
  loadingmain: {
    backgroundColor: '#fff',
    justifyContent: 'center',
    alignItems: 'center',
    padding: 20,
    maxWidth: '90%',
    borderRadius: 10,
    top: 70,
    elevation: 1,
  },
  btnTripDetail: {
    paddingVertical: 15,
    backgroundColor: '#cf8cac',
    borderRadius: 30,
    marginTop: 25,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    // flexDirection: 'row',
    flex: 1,
    width: 200,
    maxHeight: 50,
    minHeight: 50,
  },
  btnClose: {
    borderRadius: 30,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    position: 'absolute',
    top: 30,
    right: 20,
    zIndex: 99,
    width: 30,
    height: 30,
    paddingBottom: 0,
    backgroundColor: '#cf8cac',
  },
  containervid: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    height: '100%',
    width: '100%',
  },
  backgroundVideo: {
    position: 'absolute',
    top: 0,
    left: 10,
    bottom: 0,
    right: 10,
  },
});
