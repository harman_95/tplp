import React, {Component, useState} from 'react';
import {
  ImageBackground,
  Modal,
  Alert,
  Platform,
  AppRegistry,
  StyleSheet,
  Text,
  View,
  TextInput,
  Image,
  Button,
  AsyncStorage,
  TouchableHighlight,
  TouchableOpacity,
  CheckBox,
  ScrollView,
} from 'react-native';

import {Icon} from 'react-native-elements';
import {Actions} from 'react-native-router-flux';

export default class Pricing extends React.Component {
  _onSubmit = () => {
    AsyncStorage.clear();
    Actions.Signin();
  };

  render() {
    const [modalVisible, setModalVisible] = useState(true);
    return (
      <View style={styles.container}>
        <View style={styles.header}>
          <Image
            style={styles.hdr_img}
            source={require('../../assets/pic.png')}
          />
          <View style={styles.logout}>
            <Icon name="ios-heart" size={25} type="ionicon" color="#cf8cac" />
            <Text style={styles.favNoti}>3</Text>
          </View>
        </View>

        <ScrollView style={{width: '100%', backgroundColor: '#e9f0f6'}}>
          <View
            style={{
              justifyContent: 'center',
              position: 'relative',
              flexDirection: 'row',
              backgroundColor: '#cf8cac',
              paddingVertical: 5,
              paddingHorizontal: 20,
              alignItems: 'center',
            }}>
            <Text
              style={{
                fontFamily: 'GeorgiaPro-Bold',
                color: '#fff',
                fontSize: 15,
                marginLeft: 80,
              }}>
              LOS ANGELES
            </Text>
            <View
              style={{
                marginLeft: 'auto',
                flexDirection: 'row',
                alignItems: 'center',
              }}>
              <View style={{alignItems: 'center', flexDirection: 'row'}}>
                <Image
                  style={{width: 35, resizeMode: 'contain'}}
                  source={require('../../assets/temperature.png')}
                />
                <Text
                  style={{
                    color: '#fff',
                    marginLeft: 5,
                    fontFamily: 'BAHNSCHRIFT',
                  }}>
                  24°
                </Text>
              </View>
              <View
                style={{
                  alignItems: 'center',
                  flexDirection: 'row',
                  marginLeft: 15,
                }}>
                <Image
                  style={{width: 14, resizeMode: 'contain'}}
                  source={require('../../assets/dollar.png')}
                />
                <Text style={{color: '#fff', fontFamily: 'BAHNSCHRIFT'}}>
                  50
                </Text>
              </View>
            </View>
          </View>
          <View style={styles.bannerArea}>
            <Image
              style={styles.bannerImg}
              source={require('../../assets/cities.png')}
            />
            <View style={styles.createPic}>
              <Image
                style={styles.createPicImg}
                source={require('../../assets/clickcreate.png')}
              />
            </View>
            <View style={styles.createPic}>
              <Image
                style={styles.createPicImg}
                source={require('../../assets/clickcreate.png')}
              />
            </View>
            <View style={styles.createPic}>
              <Image
                style={styles.createPicImg}
                source={require('../../assets/clickcreate.png')}
              />
            </View>
          </View>

          <View style={styles.contentArea}>
            <View
              style={{
                justifyContent: 'center',
                position: 'relative',
                flexDirection: 'row',
              }}>
              <View style={styles.flexGrid}>
                <Image
                  style={styles.gridImg}
                  source={require('../../assets/Picture5.jpg')}
                />
                <Text style={styles.gridTitle}>BALI</Text>
                <View style={styles.favorite}>
                  <Icon
                    name="heart-o"
                    size={20}
                    type="font-awesome"
                    color="#ff7dbb"
                  />
                </View>
              </View>
              <View style={styles.flexGrid}>
                <Image
                  style={styles.gridImg}
                  source={require('../../assets/Picture4.jpg')}
                />
                <Text style={styles.gridTitle}>BARCELONA</Text>
                <View style={styles.favorite}>
                  <Icon
                    name="heart"
                    size={20}
                    type="font-awesome"
                    color="#cf8cac"
                  />
                </View>
              </View>
              <View style={styles.flexGrid}>
                <Image
                  style={styles.gridImg}
                  source={require('../../assets/Picture4.jpg')}
                />
                <Text style={styles.gridTitle}>CHARLESTON</Text>
                <View style={styles.favorite}>
                  <Icon
                    name="heart"
                    size={20}
                    type="font-awesome"
                    color="#cf8cac"
                  />
                </View>
              </View>
            </View>
            <View
              style={{
                justifyContent: 'center',
                position: 'relative',
                flexDirection: 'row',
              }}>
              <View style={styles.flexGrid}>
                <Image
                  style={styles.gridImg}
                  source={require('../../assets/Picture6.jpg')}
                />
                <Text style={styles.gridTitle}>FLORENCE</Text>
                <View style={styles.favorite}>
                  <Icon
                    name="heart"
                    size={20}
                    type="font-awesome"
                    color="#cf8cac"
                  />
                </View>
              </View>
              <View style={styles.flexGrid}>
                <Image
                  style={styles.gridImg}
                  source={require('../../assets/Picture4.jpg')}
                />
                <Text style={styles.gridTitle}>L.A</Text>
                <View style={styles.favorite}>
                  <Icon
                    name="heart-o"
                    size={20}
                    type="font-awesome"
                    color="#ff7dbb"
                  />
                </View>
              </View>
              <View style={styles.flexGrid}>
                <Image
                  style={styles.gridImg}
                  source={require('../../assets/Picture5.jpg')}
                />
                <Text style={styles.gridTitle}>L.A. WALLS</Text>
                <View style={styles.favorite}>
                  <Icon
                    name="heart"
                    size={20}
                    type="font-awesome"
                    color="#cf8cac"
                  />
                </View>
              </View>
            </View>
            <View
              style={{
                justifyContent: 'center',
                position: 'relative',
                flexDirection: 'row',
              }}>
              <View style={styles.flexGrid}>
                <Image
                  style={styles.gridImg}
                  source={require('../../assets/Picture6.jpg')}
                />
                <Text style={styles.gridTitle}>LA TRIPPIN</Text>
                <View style={styles.favorite}>
                  <Icon
                    name="heart"
                    size={20}
                    type="font-awesome"
                    color="#cf8cac"
                  />
                </View>
              </View>
              <View style={styles.flexGrid}>
                <Image
                  style={styles.gridImg}
                  source={require('../../assets/Picture5.jpg')}
                />
                <Text style={styles.gridTitle}>LONDON</Text>
                <View style={styles.favorite}>
                  <Icon
                    name="heart-o"
                    size={20}
                    type="font-awesome"
                    color="#ff7dbb"
                  />
                </View>
              </View>
              <View style={styles.flexGrid}>
                <Image
                  style={styles.gridImg}
                  source={require('../../assets/Picture6.jpg')}
                />
                <Text style={styles.gridTitle}>MOROCCO</Text>
                <View style={styles.favorite}>
                  <Icon
                    name="heart-o"
                    size={20}
                    type="font-awesome"
                    color="#cf8cac"
                  />
                </View>
              </View>
            </View>
            <View
              style={{
                justifyContent: 'center',
                position: 'relative',
                flexDirection: 'row',
              }}>
              <View style={styles.flexGrid}>
                <Image
                  style={styles.gridImg}
                  source={require('../../assets/Picture4.jpg')}
                />
                <Text style={styles.gridTitle}>MUNICH</Text>
                <View style={styles.favorite}>
                  <Icon
                    name="heart-o"
                    size={20}
                    type="font-awesome"
                    color="#ff7dbb"
                  />
                </View>
              </View>
              <View style={styles.flexGrid}>
                <Image
                  style={styles.gridImg}
                  source={require('../../assets/Picture2.jpg')}
                />
                <Text style={styles.gridTitle}>NYC</Text>
                <View style={styles.favorite}>
                  <Icon
                    name="heart-o"
                    size={20}
                    type="font-awesome"
                    color="#ff7dbb"
                  />
                </View>
              </View>
              <View style={styles.flexGrid}>
                <Image
                  style={styles.gridImg}
                  source={require('../../assets/Picture1.jpg')}
                />
                <Text style={styles.gridTitle}>PALM SPRINGS</Text>
                <View style={styles.favorite}>
                  <Icon
                    name="heart-o"
                    size={20}
                    type="font-awesome"
                    color="#ff7dbb"
                  />
                </View>
              </View>
            </View>
            <View
              style={{
                justifyContent: 'center',
                position: 'relative',
                flexDirection: 'row',
              }}>
              <View style={styles.flexGrid}>
                <Image
                  style={styles.gridImg}
                  source={require('../../assets/Picture5.jpg')}
                />
                <Text style={styles.gridTitle}>PARIS</Text>
                <View style={styles.favorite}>
                  <Icon
                    name="heart-o"
                    size={20}
                    type="font-awesome"
                    color="#ff7dbb"
                  />
                </View>
              </View>
              <View style={styles.flexGrid}>
                <Image
                  style={styles.gridImg}
                  source={require('../../assets/Picture6.jpg')}
                />
                <Text style={styles.gridTitle}>ROME</Text>
                <View style={styles.favorite}>
                  <Icon
                    name="heart-o"
                    size={20}
                    type="font-awesome"
                    color="#ff7dbb"
                  />
                </View>
              </View>
              <View style={styles.flexGrid}>
                <Image
                  style={styles.gridImg}
                  source={require('../../assets/Picture4.jpg')}
                />
                <Text style={styles.gridTitle}>SANTORINI</Text>
                <View style={styles.favorite}>
                  <Icon
                    name="heart-o"
                    size={20}
                    type="font-awesome"
                    color="#ff7dbb"
                  />
                </View>
              </View>
            </View>
            <View
              style={{
                justifyContent: 'center',
                position: 'relative',
                flexDirection: 'row',
              }}>
              <View style={styles.flexGrid}>
                <Image
                  style={styles.gridImg}
                  source={require('../../assets/Picture3.jpg')}
                />
                <Text style={styles.gridTitle}>SOUTH OF FRANCE</Text>
                <View style={styles.favorite}>
                  <Icon
                    name="heart-o"
                    size={20}
                    type="font-awesome"
                    color="#ff7dbb"
                  />
                </View>
              </View>
              <View style={styles.flexGrid}>
                <Image
                  style={styles.gridImg}
                  source={require('../../assets/Picture2.jpg')}
                />
                <Text style={styles.gridTitle}>TURKEY</Text>
                <View style={styles.favorite}>
                  <Icon
                    name="heart-o"
                    size={20}
                    type="font-awesome"
                    color="#ff7dbb"
                  />
                </View>
              </View>
              <View style={styles.flexGrid}>
                <Image
                  style={styles.gridImg}
                  source={require('../../assets/Picture1.jpg')}
                />
                <Text style={styles.gridTitle}>FIND ME</Text>
                <View style={styles.favorite}>
                  <Icon
                    name="heart-o"
                    size={20}
                    type="font-awesome"
                    color="#ff7dbb"
                  />
                </View>
              </View>
            </View>
          </View>

          <View style={{alignItems: 'center', justifyContent: 'center'}}>
            <Modal
              style={{
                alignItems: 'center',
                justifyContent: 'center',
                height: '100%',
              }}
              animationType="fade"
              transparent={true}
              visible={modalVisible}
              onRequestClose={() => {
                Alert.alert('Modal has been closed.');
              }}>
              <ImageBackground
                source={require('../../assets/overlaybg.png')}
                style={{width: '100%', height: '100%'}}>
                <View
                  style={{
                    alignItems: 'center',
                    marginTop: 0,
                    height: 900,
                    paddingTop: 180,
                    width: '100%',
                  }}>
                  <TouchableHighlight
                    style={styles.btnClose}
                    onPress={() => {
                      setModalVisible(!modalVisible);
                    }}>
                    <Icon
                      name="ios-close"
                      size={25}
                      type="ionicon"
                      color="#fff"
                    />
                  </TouchableHighlight>

                  <View style={styles.scrollPricing}>
                    <ScrollView horizontal={true}>
                      <View style={styles.priColumn}>
                        <Text style={styles.priceHdng}>Editing</Text>
                        <View style={{position: 'relative', marginBottom: 40}}>
                          <Text style={styles.priceTxt}>$4.99</Text>
                          <Text style={styles.dollarTxt}>per month</Text>
                        </View>
                        <Text style={styles.priFeatures}>
                          Unlimited access to editing features
                        </Text>
                        <Text style={styles.priFeatures}>StoryTemplates</Text>
                        <Text style={styles.priFeatures}>MoodBoards</Text>
                        <Text style={styles.priFeatures}>Stickers</Text>
                        <Image
                          style={styles.flowerImg}
                          source={require('../../assets/pic2.png')}
                        />
                      </View>
                      <View style={styles.priColumn}>
                        <Text style={styles.priceHdng}>Per City</Text>
                        <View style={{position: 'relative', marginBottom: 40}}>
                          <Text style={styles.priceTxt}>$89.99</Text>
                          <Text style={styles.dollarTxt}>per per city</Text>
                        </View>
                        <Text style={styles.priFeatures}>
                          Yearlong access to a customized.
                        </Text>
                        <Text style={styles.priFeatures}>
                          Photo Itinerary in the city of your choosing
                        </Text>
                        <Image
                          style={styles.flowerImg}
                          source={require('../../assets/pic2.png')}
                        />
                      </View>
                      <View style={styles.priColumn}>
                        <Text style={styles.priceHdngBundle}>Bundle two</Text>
                        <Text style={styles.priceHdng}>... Or more cities</Text>
                        <View
                          style={{
                            position: 'relative',
                            marginBottom: 40,
                            marginTop: 20,
                          }}>
                          <Text style={styles.priceTxt}>15%</Text>
                          <Text style={styles.dollarTxt}>off</Text>
                        </View>
                        <Text style={styles.priFeatures}>
                          Purchase two or more cities to receive discount
                        </Text>
                        <Image
                          style={styles.flowerImg}
                          source={require('../../assets/pic2.png')}
                        />
                      </View>
                    </ScrollView>
                  </View>
                </View>
              </ImageBackground>
            </Modal>
          </View>
        </ScrollView>

        <View style={styles.footMain}>
          <View style={styles.footFlex}>
            <TouchableOpacity onPress={() => Actions.exploreRegion()}>
              <Icon name="ios-planet" size={35} type="ionicon" color="#fff" />
            </TouchableOpacity>
          </View>

          <View style={styles.footFlex}>
            <TouchableOpacity onPress={() => Actions.Profile()}>
              <Icon
                size={30}
                name="ios-person"
                type="ionicon"
                color="#fff"
              />
            </TouchableOpacity>
          </View>

          <View style={styles.footFlex}>
            <TouchableOpacity
              style={styles.centerMenu}
              onPress={this._onSubmit.bind(this)}>
              <Image
                style={{
                  marginTop: 20,
                  width: 40,
                  height: 40,
                  resizeMode: 'contain',
                }}
                source={require('../../assets/heartlock.png')}
              />
            </TouchableOpacity>
          </View>

          <View style={styles.footFlex}>
            <TouchableOpacity onPress={() => Actions.home()}>
              <Icon size={27} name="ios-home" type="ionicon" color="#fff" />
            </TouchableOpacity>
          </View>

          <View style={styles.footFlex}>
            <TouchableOpacity onPress={() => Actions.Profile()}>
              <Icon size={27} name="ios-search" type="ionicon" color="#fff" />
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  favNoti: {
    position: 'absolute',
    top: -5,
    right: -10,
    fontFamily: 'BAHNSCHRIFT',
    width: 15,
    fontSize: 11,
    height: 15,
    borderRadius: 50,
    backgroundColor: '#cf8cac',
    color: '#fff',
    textAlign: 'center',
    alignItems: 'center',
    justifyContent: 'center',
  },
  flowerImg: {
    width: 60,
    height: 60,
    resizeMode: 'contain',
    position: 'absolute',
    bottom: -10,
    transform: [{rotate: '50deg'}],
    left: '46%',
  },
  priceHdngBundle: {
    color: '#fff',
    fontSize: 25,
    textAlign: 'center',
    fontFamily: 'GeorgiaPro-Bold',
    textTransform: 'uppercase',
  },
  priceHdng: {
    color: '#fff',
    fontSize: 18,
    textAlign: 'center',
    marginBottom: 10,
    fontFamily: 'GeorgiaPro-Bold',
    textTransform: 'uppercase',
  },
  priceTxt: {
    color: '#fff',
    fontSize: 50,
    textAlign: 'center',
    fontFamily: 'GeorgiaPro-Bold',
  },
  dollarTxt: {
    fontSize: 40,
    fontFamily: 'Southampton',
    color: '#fff',
    position: 'absolute',
    bottom: -30,
    right: 10,
  },
  scrollPricing: {
    width: '100%',
    flexDirection: 'row',
    paddingLeft: 20,
  },
  priColumn: {
    backgroundColor: '#cf8cac',
    paddingHorizontal: 25,
    paddingVertical: 30,
    width: 250,
    marginBottom: 10,
    marginRight: 10,
    borderRadius: 15,
  },
  priFeatures: {
    color: '#fff',
    fontSize: 16,
    textAlign: 'center',
    marginBottom: 20,
    textTransform: 'uppercase',
    fontFamily: 'BAHNSCHRIFT',
  },
  createPic: {
    position: 'absolute',
    top: -48,
    left: 0,
    zIndex: 99,
    backgroundColor: '#e7c7d6',
    alignItems: 'center',
    justifyContent: 'center',
    borderTopRightRadius: 100,
    borderBottomLeftRadius: 100,
    borderBottomRightRadius: 100,
    width: 90,
    height: 90,
    borderWidth: 1,
    borderColor: '#fff',
  },
  createPicImg: {
    width: 80,
    height: 80,
    resizeMode: 'contain',
    marginRight: -10,
    marginBottom: -6,
  },
  logout: {
    position: 'absolute',
    right: 20,
    top: 15,
  },
  favorite: {
    position: 'absolute',
    right: 5,
    top: 5,
  },
  letsEdit: {
    position: 'absolute',
    right: 10,
    bottom: 0,
    top: 0,
    zIndex: 0,
    opacity: 0.5,
    width: 140,
    resizeMode: 'contain',
  },
  gridTitle: {
    position: 'absolute',
    left: 0,
    right: 0,
    margin: 'auto',
    fontSize: 12,
    textAlign: 'center',
    color: '#cf8cac',
    paddingVertical: 5,
    backgroundColor: 'rgba(240,224,226, .8)',
    elevation: 1,
    marginVertical: 10,
    fontFamily: 'GeorgiaPro-Bold',
  },
  gridImg: {
    width: '100%',
    height: 120,
    resizeMode: 'cover',
    opacity: 0.5,
    borderRadius: 5,
    borderWidth: 1,
    borderColor: '#e9f0f6',
  },
  flexGrid: {
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 0,
    width: '33.3%',
    borderRadius: 5,
    backgroundColor: '#fff',
  },
  weKnow: {
    color: '#cf8cac',
    fontSize: 16,
    textAlign: 'center',
    lineHeight: 22,
    marginTop: 20,
    marginBottom: 20,
    paddingVertical: 10,
    paddingHorizontal: 40,
    fontWeight: 'bold',
    backgroundColor: '#f0e0e2',
    borderRadius: 100,
  },
  absoluTxt: {
    position: 'absolute',
    bottom: 20,
    left: 20,
    top: 20,
    backgroundColor: 'rgba(255,255,255, .7)',
    right: 20,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    paddingBottom: 10,
    borderRadius: 5,
    elevation: 1,
  },
  bannerArea: {
    alignItems: 'center',
    justifyContent: 'center',
    position: 'relative',
    backgroundColor: '#fff',
    marginBottom: 10,
  },
  bannerImg: {
    height: 400,
    width: '100%',
    resizeMode: 'cover',
  },
  contentArea: {
    paddingHorizontal: 20,
    paddingBottom: 20,
  },
  footMain: {
    backgroundColor: '#cf8cac',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    paddingTop: 10,
    paddingBottom: 10,
    maxHeight: 55,
  },
  centerMenu: {
    backgroundColor: '#cf8cac',
    position: 'relative',
    top: -15,
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    borderTopLeftRadius: 50,
    borderTopRightRadius: 50,
  },
  footFlex: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-start',
    height: '100%',
    paddingTop: 30,
  },
  header: {
    position: 'relative',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    elevation: 1,
    backgroundColor: '#f0e0e2',
    width: '100%',
    paddingTop: 6,
    paddingBottom: 6,
  },
  hdr_img: {
    width: 200,
    height: 50,
    marginBottom: 5,
    resizeMode: 'stretch',
  },
  body: {
    flex: 1,
    height: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  body_img: {
    alignItems: 'center',
    width: 220,
    height: 186,
  },
  ftr_hdngs: {
    textAlign: 'center',
    color: '#fff',
    fontSize: 25,
  },
  instagram: {
    backgroundColor: '#cf8cac',
    alignItems: 'stretch',
    paddingVertical: 13,
    textAlign: 'center',
    lineHeight: 20,
    borderRadius: 30,
    paddingBottom: 15,
    fontSize: 16,
    marginBottom: 15,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    flex: 1,
    marginRight: 5,
  },
  btnClose: {
    borderRadius: 30,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    position: 'absolute',
    top: 30,
    right: 20,
    zIndex: 99,
    width: 30,
    height: 30,
    backgroundColor: '#cf8cac',
  },
});
