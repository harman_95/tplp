import React from 'react';
import {
  AsyncStorage,
  StyleSheet,
  Text,
  View,
  Image,
  Alert,
  PermissionsAndroid,
  TouchableOpacity,
} from 'react-native';
import {Icon} from 'react-native-elements';
import {Actions} from 'react-native-router-flux';
import InstagramLogin from 'react-native-instagram-login';
import Spinner from 'react-native-loading-spinner-overlay';
import {Snackbar} from 'react-native-paper';
import axios from 'axios';

// export async function request_location_runtime_permission() {
//   try {
//     const granted = await PermissionsAndroid.request(
//       PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
//       {
//         // title: 'ThePrettyLittlePostApp',
//         // message: 'Needs access to your location ',
//         // title: 'Cool Photo App Camera Permission',
//         // message:
//         //   'Cool Photo App needs access to your camera ' +
//         //   'so you can take awesome pictures.',
//         // buttonNeutral: 'Ask Me Later',
//         // buttonNegative: 'Cancel',
//         // buttonPositive: 'OK',
//       },
//     );
//     if (granted === PermissionsAndroid.RESULTS.GRANTED) {
//       Alert.alert('Location Permission Granted.');
//       this.setState({locationStatus: 'Granted'});
//     } else {
//       Alert.alert('Location Permission Not Granted');
//       this.setState({locationStatus: 'Not Granted'});
//     }
//   } catch (err) {
//     console.warn(err);
//   }
// }

export default class app2 extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      spinner: false,
      visible: false,
      errorMessage: null,
      locationStatus: null,
    };
    this.requestLocationPermission();
  }

  // async componentDidMount() {
  //   await request_location_runtime_permission();
  // }

  /*************CHECK FOR LOCATION PERMISSIONS*********** */
  async requestLocationPermission() {
    try {
      const granted = await PermissionsAndroid.check(
        PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
        {},
      );
      console.log('********granted or not*******');
      console.log(granted);
      if (granted === true) {
        this.setState({locationStatus: 'Granted'});
      } else {
        this.setState({locationStatus: 'Not Granted'});
      }
    } catch (err) {
      console.warn(err);
    }
  }

  /************SET INSTAGRAM TOKEN ***************** */
  setIgToken = async data => {
    this.setState({spinner: true});
    fetch(
      'https://graph.instagram.com/' +
        data.user_id +
        '?fields=id,username&access_token=' +
        data.access_token,
    )
      .then(response => response.json())
      .then(json => {
        axios
          .post('http://3.135.63.132:3000/api/v1/tplp/add_user', {
            instaid: json.id,
            username: json.username,
          })
          .then(responseJson => {
            console.log(responseJson);
            this.setState({spinner: false, visible: true});
            console.log('RESPONSE JSON');
            console.log(json, '***response***', json.id);
            AsyncStorage.setItem('@userLoggedInId', json.id);
            setTimeout(() => {
              if (this.state.locationStatus == 'Not Granted') {
                Actions.enablelocation();
              } else if (this.state.locationStatus == 'Granted') {
                Actions.home();
              }
            }, 2000);
          })
          .catch(error => {
            this.setState({spinner: false});
          });
      })
      .catch(err => {
        console.log('ERRRORRRR');
        this.setState({spinner: false});
        console.log('ERROR GETTING DATA FROM Instagram');
      });
  };

  /***************RENDER LOADER************** */
  renderLoader() {
    if (this.state.spinner) {
      return (
        <Spinner
          visible={this.state.spinner}
          size={'large'}
          textContent={'Loading...'}
          animation={'slide'}
          textStyle={styles.spinnerTextStyle}
        />
      );
    }
  }

  /******************RENDER VIEW**************** */
  render() {
    return (
      <View style={styles.container}>
        {this.renderLoader()}

        <Snackbar
          visible={this.state.visible}
          onDismiss={() => this.setState({visible: false})}
          action={{
            // label: 'Undo',
            onPress: () => {
              // Do something
            },
          }}>
          Logged in Successfully{' '}
        </Snackbar>

        <View style={styles.header}>
          <Image
            style={styles.hdr_img}
            source={require('../../assets/LOGO_Blk_.png')}
          />
        </View>
        <View style={styles.body}>
          {/* <Image
            style={styles.body_img}
            source={require('../../assets/pic1.png')}
          /> */}
          <Image
            style={styles.body_img}
            source={require('../../assets/tag-line.gif')}
          />
        </View>
        <View style={styles.footer}>
          <View style={styles.ftr_inr}>
            <View style={styles.footer_hdr}>
              <Image
                style={styles.flowerTopLeft}
                source={require('../../assets/pic2.png')}
              />
              <Text style={styles.ftr_hdngs}>
                Wish you were here {'\n'}
                Oh wait you are...
              </Text>
              <Image
                style={styles.flowerBotmRit}
                source={require('../../assets/pic4.png')}
              />
            </View>
            <View>
              <Text
                onPress={() => Actions.register()}
                style={{
                  fontFamily: 'BAHNSCHRIFT',
                  color: '#cf8cac',
                  textAlign: 'center',
                  marginTop: 0,
                  marginBottom: 30,
                  fontSize: 17,
                }}>
                SIGN UP WITH EMAIL
              </Text>
            </View>
            <View style={{display: 'flex', flexDirection: 'row'}}>
              {/* <TouchableOpacity
                style={styles.btnEmail}
                onPress={() => Actions.register()}>
                <View style={styles.inst_box}>
                  <Icon name="ios-mail" type="ionicon" color="#fff" />
                  <Text
                    style={{
                      fontFamily: 'BAHNSCHRIFT',
                      color: '#fff',
                      fontSize: 15,
                      marginLeft: 10,
                    }}>
                    Email
                  </Text>
                </View>
              </TouchableOpacity> */}
            </View>
            <View style={styles.closeModal}>
              <Icon name="ios-close" type="ionicon" color="#fff" />
            </View>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  flowerBotmRit: {
    position: 'absolute',
    right: -5,
    bottom: -5,
    width: 50,
    height: 50,
    resizeMode: 'contain',
  },
  flowerTopLeft: {
    position: 'absolute',
    left: -5,
    top: -5,
    width: 50,
    height: 50,
    resizeMode: 'contain',
  },
  container: {
    flex: 1,
    justifyContent: 'flex-start',
    height: '100%',
    paddingTop: 50,
  },
  header: {
    position: 'relative',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  hdr_img: {
    width: 320,
    height: 91,
    marginBottom: 20,
    resizeMode: 'contain',
  },
  body: {
    flex: 1,
    height: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    width: '100%',
  },
  body_img: {
    alignItems: 'center',
    width: '90%',
    height: 186,
  },
  footer: {
    width: '100%',
    justifyContent: 'flex-end',
    flex: 1,
  },
  ftr_inr: {
    paddingVertical: 8,
    paddingHorizontal: 10,
    width: '100%',
    backgroundColor: '#e5c6d4',
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    minHeight: 200,
  },
  footer_hdr: {
    width: '100%',
    textAlign: 'center',
    paddingHorizontal: 7,
    paddingVertical: 8,
    marginBottom: 10,
    color: '#fff',
    fontSize: 16,
    marginTop: 10,
  },
  ftr_hdngs: {
    textAlign: 'center',
    color: '#fff',
    fontSize: 19,
    marginTop: 5,
    fontFamily: 'GeorgiaPro-Bold',
    textTransform: 'uppercase',
    // backgroundColor: 'rgba(231,199,214, .2)',
    backgroundColor: '#cf8cac',
    borderTopLeftRadius: 20,
    borderBottomRightRadius: 20,
    paddingVertical: 10,
  },
  inst_box: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
  },
  instagram: {
    backgroundColor: '#fff',
    alignItems: 'stretch',
    paddingVertical: 13,
    // textAlign:'center',
    fontFamily: 'Conv_Southampton',
    color: '#fff',
    lineHeight: 20,
    borderRadius: 30,
    paddingBottom: 15,
    fontSize: 16,
    marginBottom: 15,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    flex: 1,
    marginRight: 5,
  },
  btnEmail: {
    paddingVertical: 5,
    // textAlign:'center',
    color: '#fff',
    borderRadius: 30,
    fontSize: 14,
    marginBottom: 15,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    borderWidth: 1,
    borderColor: '#fff',
    flex: 1,
    marginLeft: 5,
  },
  closeModal: {
    position: 'absolute',
    top: -20,
    right: 15,
    borderRadius: 50,
    borderWidth: 3,
    borderColor: '#fff',
    width: 40,
    backgroundColor: '#cf8cac',
    height: 40,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
