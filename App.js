import React, {Component} from 'react';
import {AppRegistry, View, AsyncStorage} from 'react-native';
import Routes from './Routes.js';
import {Platform, StyleSheet, Text, Image} from 'react-native';

export default class App extends Component {
  constructor() {
    super();
  }

  render() {
    return <Routes />;
  }
}
AppRegistry.registerComponent('App', () => App);
